<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from department where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
    
    $viewquery = "Select a.*, b.menu_name from department_has_permissions a INNER JOIN menus as b ON b.id=a.id_menu where id_department='$id'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['menu_name'] = $row['menu_name'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }
}

if (isset($_POST['save']))
{

    $name = $_POST['name'];

   $sql1 = "SELECT * FROM department WHERE name = '$name' ";
      $result1  = $con->query($sql1);
      $resnum = mysqli_num_rows($result1);
      if($resnum < 1){

    $sql = "insert into department(name) values('$name') ";
    $con->query($sql) or die(mysqli_error($con));
      }
      else{
          echo "<script>alert('Department Already Exists')</script>";
      }

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

$tempsql = mysqli_query($con,"SELECT * FROM temp_department_has_permissions WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['id_menu'] = $row['id_menu'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $menu = $fetch[$i]['id_menu'];

   $insertbill = "INSERT INTO department_has_permissions(id_department, id_menu) VALUES ('$last_id', '$menu')";
    $result = mysqli_query($con,$insertbill);
    }

    header("location: departments.php");
}

if (isset($_POST['update']))
{
    $id  = $item['id'];

    $name = $_POST['name'];
    
    $tempsql = mysqli_query($con,"SELECT * FROM temp_department_has_permissions WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['id_menu'] = $row['id_menu'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $menu = $fetch[$i]['id_menu'];

   $insertbill = "INSERT INTO department_has_permissions(id_department, id_menu) VALUES ('$id','$menu')";
    $result = mysqli_query($con,$insertbill);
    }

  $updatequery = "update department set name = '$name' where id = $id";
  
    $res=$con->query($updatequery);
    if ($res==1)
    {
        // echo '<script>alert("Updated successfully")</script>';
        header("location: departments.php");
        
    }
    header("location: departments.php");
}

mysqli_query($con, "DELETE FROM temp_department_has_permissions");

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, menu_name FROM menus";
$result = $con->query($sql);
$menusList = array();
while ($row = $result->fetch_assoc()) {
    array_push($menusList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Department</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Department</h3>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Department Name</label>
                                <input type="text" class="form-control" name="name" value="<?php if (!empty($item['name'])) {echo $item['name'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label> </label> <br>
                                <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Permissions</button>
                            </div>

                        </div>
                           
                        </div>
                        <div id="previous"></div>
                            
                            <div style="<?php if($id==''){echo "display:none";}?>">
                                <table class='table'>
                                   <thead>
                            		<tr>
                                		<th>Menu Name</th>
                            		</tr>
                            		</thead>
                            		<tbody>
                            		    <?php for($i=0; $i<count($career); $i++){?>
                            		    <tr>
                            		        <td><?php echo $career[$i]['menu_name']; ?></td>
                            		    </tr>
                            		    <?php } ?>
                            		</tbody>
                        		</table>
                            </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="banks.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
            
             <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Menus</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Select Menus</label>
                    <select name="menu" id="menu" class="form-control selitemIcon" style="width: 350px;">
                        <option value="">SELECT</option>
                        <?php
                        for($i=0; $i<count($menusList); $i++){?>
                        <option value="<?php echo $menusList[$i]['id']; ?>"><?php echo strtoupper($menusList[$i]['menu_name']); ?></option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
  
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                name:"required",
                id_department:"required",
                item_name : "required",
                item_code : "required",
                hsn_code : "required"
            },
            messages:{
                id_category:"<span>*Enter Name</span>",
                id_subcategory:"<span>*Select Sub Category Name</span>",
                item_name:"<span>*Enter Item Name</span>",
                item_code:"<span>*Enter Item code</span>",
                hsn_code:"<span>*Enter HSN Code</span>"
            },
        })
    })
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var menu = $("#menu").val();
    
      $.ajax({

        url: 'add_menu_permission.php',
        data:{

          'sid': sid,
          'menu': menu,
        },
        success: function(result){
        $("#previous").html(result);
        $('#menu').val('');
      }
        });
      });
    </script>
    
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>