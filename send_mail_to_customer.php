<?php
include('connection.php');
// include('session_check.php');
error_reporting(0);

 
      $sid = $_GET['sid'];
      $cid = $_GET['cid'];

      $id = $_GET['cid'];
    $sql = "select * from visited_customer where id = $id";
    $result = $con->query($sql);
    $item = $result->fetch_assoc();

    $sql = "select a.id, b.employee_name from sales_engineer_report as a INNER JOIN employee as b ON a.id_employee=b.id where a.id = $sid";

    $result = $con->query($sql);
    $engineerList = $result->fetch_assoc();

    $viewquery = "SELECT a.* from contacts_has_visited_customer as a INNER JOIN visited_customer as c ON c.id=a.id_customer where a.id_sales_report='$sid' AND a.id_customer='$cid'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $cusdata = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $cusdata[$i]['contact_name'] = $row['contact_name'];
      $cusdata[$i]['contact_number'] = $row['contact_number'];
      $cusdata[$i]['contact_email'] = $row['contact_email'];
      $cusdata[$i]['attachment'] = $row['attachment'];
      $cusdata[$i]['remarks'] = $row['remarks'];
      $cusdata[$i]['id'] = $row['id'];
      $i++;
    }

    $viewquery = "SELECT DISTINCT(a.id), a.remarks, b.name, b.technical_attach from sales_report_has_items as a INNER JOIN item as b ON a.id_item=b.id INNER JOIN visited_customer as c ON a.id_item=b.id where a.id_sales_report='$sid' AND a.id_customer='$cid'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $itemdata = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $itemdata[$i]['name'] = $row['name'];
      $itemdata[$i]['technical_attach'] = $row['technical_attach'];
      $itemdata[$i]['remarks'] = $row['remarks'];
      $itemdata[$i]['id'] = $row['id'];
      $i++;
    }

//     if(isset($_POST['send'])){

// 	echo "<script>alert('Email Sent succesfully')</script>";
// echo "<script>parent.location='send_mail_to_customer.php?cid=$cid&sid=$sid'</script>";
// exit();
// }
    if($_POST){

    $arremail= implode(",", $_POST['check']);

      $recipients = array($arremail);

      $engineer = $engineerList['employee_name'];
    $companyName = strtoupper($engineerList['company_name']);

$to = implode(",", $recipients);

$subject = "THANKS FOR THE COURTESY SHOWN DURING MY VISIT TO YOUR OFFICE AND UOT GENERATED SOME ENQ FOR BELOW PRODUCTS";

$message = "
<table>
<tr><th style='text-align: left'>DEAR MR. </th></tr>
<tr><th style='text-align: left'>THANKS FOR THE COURTESY SHOWN DURING MY VISIT TO YOUR OFFICE AND <br> YOU GENERATED SOME ENQ FOR BELOW PRODUCTS,</th></tr>
</table>
<br>
<table>
<tr>
<th>Sl. No</th>
<th>PRODUCT</th>
<th>ATTACHMENT</th>
</tr>";

for ($i=0; $i<count($itemdata); $i++)
{
 $id = $i+1;
 $name = $itemdata[$i]['name'];
 $attach = $itemdata[$i]['technical_attach'];
 

$message .="
<tr>
<td>$id</td>
<td>$name</td>
<td><a href='https://www.atninstruments.com/billing/uploads/$attach' target='_blank'>Technical File</a></td>
</tr>
<tr>";
}

$message .="</table>
<br><br>
PLEASE FIND ATTACHED PRODUCT TECHNICAL PDF FOR YOUR REFERENCE, WILL <br> COME WITH OUR COMPITATIVE OFFER SHORTLY.
<br><br><br>
REGARDS
<br>
$engineer
<br><br>
$companyName";


// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <mktg@saienpl.com>' . "\r\n";
$headers .= 'Cc: <yogeshdims@gmail.com>' . "\r\n";

mail($to,$subject,$message,$headers);

echo "<script>alert('Email Sent succesfully')</script>";
echo "<script>parent.location='send_mail_to_customer.php?cid=$cid&sid=$sid'</script>";
}



?>
<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Items List</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<script type="text/javascript">
    function Ondelete(id)
    {
        var sid = '<?php echo $sid ?>';
        var cid = '<?php echo $cid ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_item_from_customer.php?id="+id+"&cid="+cid+"&sid="+sid;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
              <div class="page-title clearfix">
                    <h3>Customer Details</h3>
                </div>
                <div>
                  <h4><b>Name : </b><?php echo ucwords($item['customer_name']); ?></h4>
                  <h4><b>Address : </b><?php echo ucwords($item['address']); ?></h4>

                  <!-- <img style="width: 210px; height: 140px; position: absolute; left: 350px; top: 130px; cursor: pointer;" src="uploads/<?php echo $item['visit_card']; ?>" data-toggle="modal" data-target="#myModal"> -->
                  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
          <button type="button" class="close" data-dismiss="modal">&times;</button>

            <img style="width: 600px; height: 350px;" src="uploads/<?php echo $item['visit_card']; ?>">

      </div>
      
    </div>
  </div>
                </div>

                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-title clearfix">
                    <h3>More Contacts List</h3>
                        <a href="add_sales_engineer_report.php?id=<?php echo $sid; ?>" class="btn btn-primary">Back</a>
                </div>


  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                                <th>Contact Name</th>
                                <th>Contact Number</th>
                                <th>Contact Email</th>
                                <th>Attachment</th>
                                <th><i style="text-align: center;"><input type="checkbox" name="checkall" id="checkall" value="1">Check All</i></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($cusdata); $i++)
                          {
                            $id = $cusdata[$i]['id'];
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                          <td><?php echo $cusdata[$i]['contact_name']; ?></td>
                          <td><?php echo $cusdata[$i]['contact_number']; ?></td>
                          <td><?php echo $cusdata[$i]['contact_email']; ?></td>
                          <td><a href="uploads/<?php echo $cusdata[$i]['attachment']; ?>" target="_blank"><?php echo $cusdata[$i]['attachment']; ?></a></td>
                          <td><input type='checkbox' name='check[]' value='<?php echo $cusdata[$i]['contact_email']; ?>_<?php echo $cusdata[$i]['contact_name']; ?>' class='check'/></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>

                    <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-success" type="submit" name="send">SEND</button>
                   </div>
                </div>
               </form>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>

       <script>
        
        $(function () {
            $("#checkall").click(function () {
                if ($("#checkall").is(':checked')) {
                    $(".check").prop("checked", true);
                } else {
                    $(".check").prop("checked", false);
                }
            });
        });
    </script>
       
</body>

</html>