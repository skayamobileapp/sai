<?php
include 'connection.php';

  $viewquery = "Select a.*, b.bank_name, c.type_name, d.customer_name from transactions as a INNER JOIN banks as b ON a.id_bank=b.id INNER JOIN transaction_type as c ON a.id_transaction_type=c.id INNER JOIN customer as d ON a.customer_id=d.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['bank_name'] = strtoupper($row['bank_name']);
  $career[$i]['type_name'] = $row['type_name'];
  $career[$i]['date'] = $row['date'];
  $career[$i]['transaction_no'] = $row['transaction_no'];
  $career[$i]['customer'] = $row['customer_name'];
  $career[$i]['transaction_date'] = $row['transaction_date'];
  $career[$i]['value']=$row['value'];
  $career[$i]['remark']=$row['remark'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Transactions</title>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
    
  <script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?user_id='+id;
      }
    }
  </script>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>

                <div class="page-container">
                    <div class="page-title clearfix">
                        <h3>Receipts</h3>
                        <a href="add_transaction.php" class="btn btn-primary">+ Add Receipt</a>
                    </div>

                      <table class="table table-striped" id="example">
                          <thead>
                        <tr>
                          <th>Transaction Date</th>
                          <th>Bank Name</th>
                          <th>Customer</th>
                          <th>Transaction Type</th>
                          <th>Transaction Number</th>
                          <th>Date</th>
                          <th>Value</th>
                          <th>Remark</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $cust = $career[$i]['customer'];
                            $tdate = date("d/m/Y", strtotime($career[$i]['transaction_date']));
                            $date = $career[$i]['date'];
                            $name = $career[$i]['bank_name'];
                            $type = $career[$i]['type_name'];
                            $amt = $career[$i]['amount'];
                            $tNO = $career[$i]['transaction_no'];
                            $value = $career[$i]['value'];
                            $remark = $career[$i]['remark'];
                            ?>
                        <tr>
                            <td><?php echo $tdate; ?></td>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $cust; ?></td>
                          <td><?php echo $type; ?></td>
                          <td><?php echo $tNO; ?></td>
                          <td><?php echo $date; ?></td>
                          <td><?php echo $value; ?></td>
                          <td><?php echo $remark; ?></td>
                          <td><a href="add_transaction.php?id=<?php echo $id; ?>" title="EDIT"><i class="fa fa-edit fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>
                        </tbody>
                      </table>

                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
            <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>
       <script>
            $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
  
</body>

</html>