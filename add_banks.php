<?php
include 'connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from banks where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{

    $name = $_POST['bank_name'];
    $accType = $_POST['account_type'];
    $accName = $_POST['account_name'];
    $accNo = $_POST['account_no'];
    $ifsc = $_POST['ifsc_code'];
    $branch = $_POST['branch_name'];
    $address = $_POST['branch_address'];

   $sql1 = "SELECT * FROM banks WHERE bank_name = '$name' AND ifsc_code='$ifsc'";
      $result1  = $con->query($sql1);
      $resnum = mysqli_num_rows($result1);
      if($resnum < 1){

    $sql = "insert into banks(bank_name, account_type, account_name, account_no, ifsc_code, branch_name, branch_address) values('$name',  '$accType', '$accName',  '$accNo', '$ifsc', '$branch', '$address') ";
    $con->query($sql) or die(mysqli_error($con));
      }
      else{
          echo "<script>alert('Bank Already Exists')</script>";
      }

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: banks.php");
}

if (isset($_POST['update']))
{
    $id  = $item['id'];

    $name = $_POST['bank_name'];
    $accType = $_POST['account_type'];
    $accName = $_POST['account_name'];
    $accNo = $_POST['account_no'];
    $ifsc = $_POST['ifsc_code'];
    $branch = $_POST['branch_name'];
    $address = $_POST['branch_address'];

  $updatequery = "update banks set bank_name = '$name', account_type='$accType', account_name='$accName', account_no='$accNo', ifsc_code = '$ifsc', branch_name='$branch', branch_address='$address' where id = $id";
  
    $res=$con->query($updatequery);
    if ($res==1)
    {
        // echo '<script>alert("Updated successfully")</script>';
        header("location: banks.php");
        
    }
    header("location: banks.php");
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Bank</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Bank</h3>
                    </div>


                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Bank Name</label>
                                <input type="text" class="form-control" name="bank_name" value="<?php if (!empty($item['bank_name'])) {echo $item['bank_name'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Account Type</label>
                                <input type="text" class="form-control" name="account_type" value="<?php if (!empty($item['account_type'])) {echo $item['account_type'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Account Name</label>
                                <input type="text" class="form-control" name="account_name" value="<?php if (!empty($item['account_name'])) {echo $item['account_name'];}?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Account Number</label>
                                <input type="text" class="form-control" name="account_no" value="<?php if (!empty($item['account_no'])) {echo $item['account_no'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>IFSC Code</label>
                                <input type="text" class="form-control" name="ifsc_code" value="<?php if (!empty($item['ifsc_code'])) {echo $item['ifsc_code'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Branch Name</label>
                                <input type="text" class="form-control" name="branch_name" value="<?php if (!empty($item['branch_name'])) {echo $item['branch_name'];}?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Branch Address</label>
                                <input type="text" class="form-control" name="branch_address" value="<?php if (!empty($item['branch_address'])) {echo $item['branch_address'];}?>">
                            </div>

                        </div>
                            <br>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="banks.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                bank_name:"required",
                id_subcategory:"required",
                account_no : "required",
                item_code : "required",
                ifsc_code : "required"
            },
            messages:{
                bank_name:"<span>*Enter Name</span>",
                id_subcategory:"<span>*Select Sub Category Name</span>",
                account_no:"<span>*Enter Account Number</span>",
                item_code:"<span>*Enter Item code</span>",
                ifsc_code:"<span>*Enter IFSC Code</span>"
            },
        })
    })
</script>
<script>
    function getGST(){
        var org_cost= $("#orgrate").val();
        var n_cost= $("#rate").val();
        var tot = (((n_cost - org_cost) *  100) / org_cost);
        var value = tot.toFixed(2);
        $('#cgst_rate').val(value +' %');
        $('#sgst_rate').val(value +' %');
    }
    
    var org_cost= $("#orgrate").val();
        var n_cost= $("#rate").val();
        var tot = (((n_cost - org_cost) *  100) / org_cost);
        $('#cgst_rate').val(tot +' %');
        $('#sgst_rate').val(tot +' %');
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>