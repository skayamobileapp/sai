<?php
include 'connection.php';
if (isset($_GET['cid'])) {

    $cid       = $_GET['cid'];
    $cmid       = $_GET['id'];
    $sql      = "select * from category where id = $cid";
    $result   = $con->query($sql);
    $category = $result->fetch_assoc();

    $sql      = "select * from competitors where id = $cmid";
    $result   = $con->query($sql);
    $competitor = $result->fetch_assoc();

    $query = "SELECT a.*, b.name as subName, b.sub_cat_code, c.name as itemName, c.code from item_competitors as a INNER JOIN sub_category as b ON a.id_subcategory=b.id INNER JOIN item as c ON a.id_item=c.id WHERE a.id_category='$cid' AND a.id_competitior='$cmid' ";
$queryresult = mysqli_query($con,$query);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($queryresult))
{
  $career[$i]['name'] = strtoupper($row['name']);
  $career[$i]['price'] = $row['price'];
  $career[$i]['attachment'] = $row['attachment'];
  $career[$i]['subName'] = $row['subName']."-".$row['sub_cat_code'];
  $career[$i]['itemName'] = $row['itemName']."-".$row['code'];
  $career[$i]['our_item_price'] = $row['our_item_price'];
  $career[$i]['price_difference'] = $row['price_difference'];
  $career[$i]['id'] = $row['id'];
  $i++;
}
   
}else {
    $id= 0 ;
}
      
if (isset($_POST['add'])) {
    
    $name = $_POST['citem'];
      $price = $_POST['unitprice'];
  $subCategoryId = $_POST['id_subcategory'];
  $itemId = $_POST['id_item'];
  $ourPrice = $_POST['our_price'];

  $file = $_FILES['file']['name'];
  $tmpfile = $_FILES['file']['tmp_name'];
  move_uploaded_file($tmpfile, "uploads/".$file);

  $priceDiff = $ourPrice - $price;
  if($priceDiff < 0){
    $remaining = "Loss : ".abs($priceDiff);
  }
  else
  if($priceDiff > 0){
    $remaining = "Profit : ".abs($priceDiff);
  }
  else{
    $remaining = abs($priceDiff);
  }

   $sql = "INSERT into item_competitors(id_category, id_competitior, name, price, attachment, id_subcategory, id_item, our_item_price, price_difference) VALUES('$cid', '$cmid', '$name', '$price', '$file', '$subCategoryId', '$itemId', '$ourPrice', '$remaining')";

  $result = $con->query($sql);

    echo "<script>parent.location='item_competitor_list.php?id=$cmid&cid=$cid'</script>";
    // header("location: categories.php");
}
if (isset($_POST['update'])) {
    
    $name  = $_POST['name'];
    
    $sql = "update category set name = '$name' where id = $cid";

    $con->query($sql);
    header("location: categories.php");
}


?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if(!empty($category['category_id'])){echo "Edit";}else{echo "Add";}?> Items</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
    input{
        text-transform: UPPERCASE;
    }
</style>
<script type="text/javascript">
    function Ondelete(id)
    {
        var cid = '<?php echo $cid ?>';
        var cmid = '<?php echo $cmid ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_competitor_item.php?id="+id+"&cid="+cid+"&cmid="+cmid;
      }
    }
</script>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if(!empty($category['id'])){echo "Add";}else{echo "Add";}?> Items of <?php echo strtoupper($competitor['competitor_name']); ?></h3>
                    <a href="categories.php" class="btn btn-success" >Back</a>

                    </div>

                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-4">
                                <label>Category Name <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="<?php if (!empty($category['name'])) {echo $category['name'];}?>" onblur="checkduplicateName()" readonly>
                              </div>
                              <div class="col-sm-4">
                                <label>Compitator Item Name</label>
                                <input type="text" class="form-control" name="citem" id="citem" autocomplete="off">
                              </div>
                              <div class="col-sm-4">
                                <label>Unit Price</label>
                                <input type="text" class="form-control" name="unitprice" id="unitprice" autocomplete="off">
                              </div>
                            </div>
                            <br>
                        <div class="row">
                          <div class="col-sm-4">
                            <label>Item Attachment</label>
                            <input type="file" class="form-control" name="file" id="file" autocomplete="off">
                          </div>
                          <div class="col-sm-4">
                            <label>Sub Category</label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 310px;">
                                <option value="">SELECT</option>
                            </select>
                          </div>
                          <div class="col-sm-4">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 310px;" onchange="getItemPrice()">
                            <option value="">SELECT</option>
                            </select>
                          </div>
                      </div>
                          <div class="row">
                              <div id="itemprice"></div>
                          </div>
                        </div>
                        <div class="button-block clearfix">
                       <div class="pull-right">
                        <button class="btn btn-success" type="submit" name="add" id="add"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Add";}?></button>
                       </div>
                    </div>
                        <div>
                          <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                          <th>Competitor Item Name</th>
                          <th>Price</th>
                          <th>Attachment</th>
                          <th>Our Sub Category</th>
                          <th>Our Item</th>
                          <th>Our Item Price</th>
                          <th>Price Difference</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            ?>
                        <tr>
                          <td><?php echo $career[$i]['name']; ?></td>
                          <td><?php echo $career[$i]['price']; ?></td>
                          <td><a href="uploads/<?php echo $career[$i]['attachment']; ?>"><?php echo $career[$i]['attachment']; ?> </a></td>
                          <td><?php echo $career[$i]['subName']; ?></td>
                          <td><?php echo $career[$i]['itemName']; ?></td>
                          <td><?php echo $career[$i]['our_item_price']; ?></td>
                          <td><?php echo $career[$i]['price_difference']; ?></td>
                          <td><a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x" title="DELETE"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>


                        </tbody>
                    </table>
                        </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script>
    
<script type="text/javascript">
        function checkduplicateName(){
          var name = $("#name").val();
          var id = "<?php echo $id;?>";
          console.log(id);

          $.ajax({url: "duplicate_categoryname.php?name="+name+"&id="+id, success: function(result){
            if(result==1) {
                alert("Category Name already there");
                $("#name").val(' ');
            }
          }
        });
        }

          var id = '<?php echo $cid; ?>';
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#id_subcategory").html(result);
          }
        });
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#id_item").html(result);
          }
        });
        }

        function getItemPrice(){
          var id = $("#id_item").val();
          console.log(id);

          $.ajax({url: "get_item_price.php?id="+id, success: function(result){
            $("#itemprice").html(result);
          }
        });
        }
</script>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
</body>

<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                citem:"required"
            },
            messages:{
                citem:"<span>Enter Item Name</span>"
            },
        });
    });
</script>
<!-- <script src="js/jquery-3.3.1.js"></script> -->
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
</html>
