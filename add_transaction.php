<?php
include 'connection.php';
session_start();
error_reporting(0);
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from transactions where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();

    $viewquery = "SELECT a.*, d.invoice_no FROM receipt_has_invoice as a INNER JOIN tax_invoice_sales as d ON a.id_tax_sales=d.id WHERE a.id_receipt ='$id'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['invoice_no'] = $row['invoice_no'];
      $career[$i]['invoice_date'] = $row['invoice_date'];
      $career[$i]['invoice_amount'] = $row['invoice_amount'];
      $career[$i]['paid_amount'] = $row['paid_amount'];
      $career[$i]['balance'] = $row['balance'];
      $career[$i]['payment'] = $row['payment'];
      $i++;
    }
}

if (isset($_POST['save']))
{

    $bankId =$_POST['id_bank'];
    $customer =$_POST['customer'];
    $transactionTypeId =$_POST['id_transaction_type'];
    $details = $_POST['transaction_no'];
    $chkdate = date("Y-m-d", strtotime($_POST['chkdate']));
    $date = date("Y-m-d", strtotime($_POST['date']));
    $value = $_POST['value'];
    $remark = $_POST['remark'];

    $sql = "insert into transactions(id_bank, id_transaction_type, customer_id,  transaction_no, transaction_date, date, value, remark) values('$bankId', '$transactionTypeId', '$customer', '$details', '$chkdate', '$date', '$value', '$remark') ";

    $con->query($sql) or die(mysqli_error($con));
      

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

$tempsql = mysqli_query($con,"SELECT * FROM temp_receipt_has_invoice WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['id_tax_sales'] = $row['id_tax_sales'];
    $fetch[$i]['invoice_date'] = $row['invoice_date'];
    $fetch[$i]['invoice_amount'] = $row['invoice_amount'];
    $fetch[$i]['paid_amount'] = $row['paid_amount'];
    $fetch[$i]['balance'] = $row['balance'];
    $fetch[$i]['payment'] = $row['payment'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $ino = $fetch[$i]['id_tax_sales'];
    $idate = $fetch[$i]['invoice_date'];
    $iamt = $fetch[$i]['invoice_amount'];
    $paid = $fetch[$i]['paid_amount'];
    $balance = $fetch[$i]['balance'];
    $paystatus = $fetch[$i]['payment'];

   $insertbill = "INSERT INTO receipt_has_invoice(id_receipt, id_tax_sales, invoice_date, invoice_amount, paid_amount, balance, payment) VALUES ('$last_id', '$ino', '$idate', '$iamt', '$paid', '$balance', '$paystatus')";
    $result = mysqli_query($con,$insertbill);
    if($paystatus == "PAID"){
        $updatetax = "UPDATE tax_invoice_sales SET payment_status='PAID', balance='$balance' WHERE id='$ino'";
        $result = mysqli_query($con,$updatetax);

    }
    else
    {
        $updatetax = "UPDATE tax_invoice_sales SET payment_status='PENDING($balance)', balance='$balance' WHERE id='$ino'";
        $result = mysqli_query($con,$updatetax);
    }
    }

    header("location: transactions.php");
}

if (isset($_POST['update']))
{
    $id  = $item['id'];
    
    $bankId =$_POST['id_bank'];
    $customer =$_POST['customer'];
    $transactionTypeId =$_POST['id_transaction_type'];
    $details = $_POST['transaction_no'];
    $chkdate = date("Y-m-d", strtotime($_POST['chkdate']));
    $date = date("Y-m-d", strtotime($_POST['date']));
    $value = $_POST['value'];
    $remark = $_POST['remark'];

    $tempsql = mysqli_query($con,"SELECT * FROM temp_receipt_has_invoice WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['id_tax_sales'] = $row['id_tax_sales'];
    $fetch[$i]['invoice_date'] = $row['invoice_date'];
    $fetch[$i]['invoice_amount'] = $row['invoice_amount'];
    $fetch[$i]['paid_amount'] = $row['paid_amount'];
    $fetch[$i]['balance'] = $row['balance'];
    $fetch[$i]['payment'] = $row['payment'];
    $fetch[$i]['id_po_no'] = $row['id_po_no'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $ino = $fetch[$i]['id_tax_sales'];
    $idate = $fetch[$i]['invoice_date'];
    $iamt = $fetch[$i]['invoice_amount'];
    $paid = $fetch[$i]['paid_amount'];
    $balance = $fetch[$i]['balance'];
    $paystatus = $fetch[$i]['payment'];
    $poNoId = $fetch[$i]['id_po_no'];

   $insertbill = "INSERT INTO receipt_has_invoice(id_receipt, id_tax_sales, invoice_date, invoice_amount, paid_amount, balance, payment, id_po_no) VALUES ('$id', '$ino', '$idate', '$iamt', '$paid', '$balance', '$paystatus', '$poNoId')";
    $result = mysqli_query($con,$insertbill);
    if($paystatus == "PAID"){
        $updatetax = "UPDATE tax_invoice_sales SET payment_status='PAID', balance='$balance' WHERE id='$ino'";
        $result = mysqli_query($con,$updatetax);
        
        $updatePur = "UPDATE purchase_order_inwards SET payment_status='PAID' WHERE id='$poNoId'";
        $result = mysqli_query($con,$updatePur);

    }
    else
    {
        $updatetax = "UPDATE tax_invoice_sales SET payment_status='PENDING($balance)', balance='$balance' WHERE id='$ino'";
        $result = mysqli_query($con,$updatetax);
    }
    }

 $updatequery = "update transactions set id_bank = '$bankId', id_transaction_type = '$transactionTypeId', customer_id='$customer', transaction_no='$details', transaction_date='$chkdate', value='$value', remark='$remark', date='$date' where id = $id";

    $res=$con->query($updatequery);
    if ($res==1)
    {
        // echo '<script>alert("Updated successfully")</script>';
        header("location: transactions.php");
        
    }
    header("location: transactions.php");
}

  mysqli_query($con, "DELETE FROM temp_receipt_has_invoice");


$sql = "SELECT id, bank_name, account_name FROM banks";
$result = $con->query($sql);
$bankList = array();
while ($row = $result->fetch_assoc()) {
    array_push($bankList, $row);
  }
  
  $sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$customerList = array();
while ($row = $result->fetch_assoc()) {
    array_push($customerList, $row);
  }

$sql = "SELECT id, type_name FROM transaction_type order by type_name asc";
$result = $con->query($sql);
$tTypeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($tTypeList, $row);
  }
  
  $sql = "SELECT id, invoice_no FROM tax_invoice_sales";
$result = $con->query($sql);
$taxBillNoList = array();
while ($row = $result->fetch_assoc()) {
    array_push($taxBillNoList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Transactions</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
</style>
<script type="text/javascript">
    function Ondelete(id)
    {
        var sid = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_invoice_from_receipt.php?id="+id+"&sid="+sid;
      }
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Receipt</h3>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                            <div class="form-group">
                                <label>Transaction Date <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="chkdate" id="chkdate1" autocomplete="off" value="<?php echo $item['transaction_date']; ?>">
                            </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                <label>Bank Name <span class="error"> *</span></label>
                                <select name="id_bank" class="form-control selitemIcon">
                                    <option value="">SELECT</option>
                                    <?php
                                    for($i=0; $i<count($bankList); $i++){?>
                                    <option value="<?php echo $bankList[$i]['id']; ?>" <?php if($bankList[$i]['id']==$item['id_bank']){ echo "selected"; } ?>><?php echo $bankList[$i]['bank_name']."-".$bankList[$i]['account_name']; ?></option>
                                    <?php }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group">
                                <label>Customer Name <span class="error"> *</span></label>
                                <select name="customer" id="customer" class="form-control selitemIcon" onchange="getPurchaseInvNo()">
                                    <option value="">SELECT</option>
                                    <?php
                                    for($i=0; $i<count($customerList); $i++){?>
                                    <option value="<?php echo $customerList[$i]['id']; ?>" <?php if($customerList[$i]['id']==$item['customer_id']){ echo "selected"; } ?>><?php echo $customerList[$i]['customer_name']; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                <label>Transaction Type <span class="error"> *</span></label>
                                <select name="id_transaction_type" class="form-control selitemIcon" onchange="getfirst(this)">
                                    <option value="">SELECT</option>
                                    <?php
                                    for($i=0; $i<count($tTypeList); $i++){?>
                                    <option value="<?php echo $tTypeList[$i]['id']; ?>" <?php if($tTypeList[$i]['id']==$item['id_transaction_type']){ echo "selected"; } ?>><?php echo $tTypeList[$i]['type_name']; ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group">
                                <label>Transaction Number <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="transaction_no" id="transaction_no" autocomplete="off" value="<?php echo $item['transaction_no']; ?>">
                            </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group">
                                <label> Date <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="date" id="date" value="<?php if (!empty($item['date'])) {echo $item['date'];}?>" autocomplete="off">
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                            <div class="form-group">
                                <label>Value <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="value" value="<?php if (!empty($item['value'])) {echo $item['value'];}?>">
                            </div>
                            </div>
                            <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remark <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="remark" value="<?php if (!empty($item['remark'])) {echo $item['remark'];}?>">
                            </div>
                            </div>

                        </div>
                            <br>
                           
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> </label><br>
                            <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Invoice Details</button>
                            </div>
                        </div>
                    </div>
                        </div>
                        <div id="previous"></div>
                    <div style="<?php if($id==''){echo "display:none";}?>">
                                <table class='table table-striped' id="example">
                                   <thead>
                                    <tr>
                                        <th>Invoice NO</th>
                                        <th>Invoice date</th>
                                        <th>Invoice Amont</th>
                                        <th>Paid Amont</th>
                                        <th>Balance</th>
                                        <th>Remark</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0; $i<count($career); $i++){?>
                                        <tr>
                                            <td><?php echo $career[$i]['invoice_no']; ?></td>
                                            <td><?php echo $career[$i]['invoice_date']; ?></td>
                                            <td><?php echo $career[$i]['invoice_amount']; ?></td>
                                            <td><?php echo $career[$i]['paid_amount']; ?></td>
                                            <td><?php echo $career[$i]['balance']; ?></td>
                                            <td><?php echo $career[$i]['payment']; ?></td>
                                            <td><a href="javascript:Ondelete(<?php echo $career[$i]['id']; ?>);">DELETE</a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="transactions.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>

            <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Invoice Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Tax Sales Invoice NO</label>
                    <select name="invoice_no" class="form-control selitemIcon" id="invoice_no" style="width: 260px;" onchange="getInvDetails()">
                        <option value="">SELECT</option>
                    </select>
                </div>
                <div id="fieldResult"></div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" data-dismiss="modal" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

  <script type="text/javascript">
    function getPurchaseInvNo(){
      var id = $("#customer").val();
      console.log(id);

      $.ajax({url: "get_invoice_number.php?rid="+id, success: function(result){
        $("#invoice_no").html(result);
      }
    });
    }
    
    function getInvDetails(){
      var id = $("#invoice_no").val();
      console.log(id);

      $.ajax({url: "get_invoice_details.php?rid="+id, success: function(result){
        $("#fieldResult").html(result);
      }
    });
    }

    function getBalance(){
      var invamt = $("#invoice_amount").val();
      var payamt = $("#paid_amount").val();
      var bal = invamt - payamt;
      $("#balance").val(bal.toFixed(2));
    }
</script>
    
  <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
</script>
       
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
    $( "#chkdate1" ).datepicker();
  } );
  </script>
  <script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var invoice_no = $("#invoice_no").val();
    if(invoice_no == ""){
        alert("Select Invoice Number");
        return false;
    }
    var invoice_date = $("#invoice_date").val();
    if(invoice_date == ""){
        alert("Enter Invoice Date");
        return false;
    }
    var paid_amount = $("#paid_amount").val();
    if(paid_amount == ""){
        alert("Enter Paid Amount");
        return false;
    }
    var invoice_amount = $("#invoice_amount").val();
    var balance = $("#balance").val();
    if (balance <= 0) {
        var payment ="PAID";
    }
    else{
        var payment = "PENDING";
    }
    var ponoid = $("#ponoid").val();
    
      $.ajax({

        url: 'add_invoice_to_receipt.php',
        data:{

          'sid': sid,
          'invoice_no': invoice_no,
          'invoice_date': invoice_date,
           'paid_amount': paid_amount,
           'invoice_amount': invoice_amount,
           'balance': balance,
            'payment': payment,
            'ponoid': ponoid,
        },
        success: function(result){
        $("#previous").html(result);
      }
        });
      });
</script>
    
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_bank:"required",
                customer:"required",
                id_transaction_type:"required",
                transaction_no : "required",
                chkdate : "required",
                remark : "required",
                date : "required",
                value : "required"
            },
            messages:{
                id_bank:"<span>Select Bank Name</span>",
                customer:"<span>Select Customer Name</span>",
                id_transaction_type:"<span>Select Transaction Type</span>",
                transaction_no:"<span>Enter Transaction Number</span>",
                chkdate:"<span>Select Transaction Date</span>",
                remark:"<span>Enter Remark</span>",
                date:"<span>Select Date</span>",
                value:"<span>Enter Value </span>"
            },
        })
    })
</script>

</body>

</html>