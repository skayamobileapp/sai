<?php
include 'connection.php';
if (isset($_GET['id'])) {

    $cid       = $_GET['id'];
    $sql      = "select * from category where id = $cid";
    $result   = $con->query($sql);
    $category = $result->fetch_assoc();

    $query = "SELECT * from competitors WHERE id_category='$cid' ";
$queryresult = mysqli_query($con,$query);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($queryresult))
{
  $career[$i]['name'] = strtoupper($row['competitor_name']);
  $career[$i]['id'] = $row['id'];
  $i++;
}
   
}else {
    $id= 0 ;
}
      
if (isset($_POST['save'])) {
    
    $sql      = "select * from category";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
    $ref= sprintf("%'.03d", $resnum);
    
    $name     = $_POST['name'];

    $sql = "insert into category(name, category_code, status) values('$name', '$ref', 1)";
    $con->query($sql);
    
    echo "<script>alert('category added successfully')</script>";
    echo "<script>parent.location='categories.php'</script>";
    // header("location: categories.php");
}
if (isset($_POST['update'])) {
    
    $name  = $_POST['name'];
    
    $sql = "update category set name = '$name' where id = $cid";

    $con->query($sql);
    header("location: categories.php");
}


?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if(!empty($category['category_id'])){echo "Edit";}else{echo "Add";}?> Category</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
    input{
        text-transform: UPPERCASE;
    }
</style>
<script type="text/javascript">
    function Ondelete(id)
    {
        var cid = '<?php echo $cid ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_competitor.php?id="+id+"&cid="+cid;
      }
    }
</script>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if(!empty($category['id'])){echo "Add";}else{echo "Add";}?> Competitor</h3>
                    <a href="categories.php" class="btn btn-success" >Back</a>

                    </div>


                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-4">
                                <label>Name <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="<?php if (!empty($category['name'])) {echo $category['name'];}?>" onblur="checkduplicateName()" readonly>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-sm-4">
                                    <br>
                                    <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Compitators</button>
                                </div>
                            </div>
                            
                        </div>
                        <div>
                          <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                          <th>Competitor Name</th>
                          <th>Add Items</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            ?>
                        <tr>
                          <td><?php echo $career[$i]['name']; ?></td>
                          <td><a href="item_competitor_list.php?id=<?php echo $id; ?>&cid=<?php echo $cid; ?>" class=""> Add Items</a></td>
                          <td><a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x" title="DELETE"></i></a></td>

                        </tr>
                          <?php
                          }
                          ?>


                        </tbody>
                    </table>
                        </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Compitator</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Compitator Name</label>
                    <input type="text" class="form-control" name="cname" id="cname" autocomplete="off">
                </div>
            </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script>
    
<script type="text/javascript">
        function checkduplicateName(){
          var name = $("#name").val();
          var id = "<?php echo $id;?>";
          console.log(id);

          $.ajax({url: "duplicate_categoryname.php?name="+name+"&id="+id, success: function(result){
            if(result==1) {
                alert("Category Name already there");
                $("#name").val(' ');
            }
          }
        });
        }

          var id = '<?php echo $cid; ?>';
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#id_subcategory").html(result);
          }
        });
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#id_item").html(result);
          }
        });
        }

        function getItemPrice(){
          var id = $("#id_item").val();
          console.log(id);

          $.ajax({url: "get_item_price.php?id="+id, success: function(result){
            $("#itemprice").html(result);
          }
        });
        }
</script>
<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
        <script type="text/javascript">
      $(document).ready(function (e) {
          $('#add').on('click', function () {
              var cid = '<?php echo $cid; ?>';
              var cname = $('#cname').val();
              if(cname == ""){
                alert('ENTER COMPETITOR NAME');
                return false;
              }
              $.ajax({

        url: 'add_competitor.php',
        data:{

          'cid': cid,
          'cname': cname,
        },
        success: function(result){
        $("#previous").html(result);
        location.reload();
      }
        });
          });
      });
</script>
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>

</body>

</html>
