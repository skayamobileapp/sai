<?php
include 'connection.php';
if (isset($_GET['id'])) {
    
    $id          = $_GET['id'];
    $sql         = "select * from vendor_quotations where id = $id";
    $result      = $con->query($sql);
    $vendor_row = $result->fetch_assoc();
}

if (isset($_POST['update'])) {

    $idVendor       = $_POST['id_vendor'];
    $idQtn  = $_POST['id_qtn'];
    $date  = $_POST['date'];
    $remark  = $_POST['remark'];
    $file = $_FILES['file']['name'];
    $temp = $_FILES['file']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);

    $sql    = "update vendor_quotations set id_vendor = '$idVendor', quotation_no = '$idQtn', date='$date', attachment='$file', remark='$remark' where id = $id";
        $con->query($sql);
        header("location:vendor_quotations.php");
    }

if (isset($_POST['save']))
{
    $idVendor       = $_POST['id_vendor'];
    $idQtn  = $_POST['id_qtn'];
    $date  = date("Y-m-d", strtotime($_POST['date']));
    $remark  = $_POST['remark'];
    $file = $_FILES['file']['name'];
    $temp = $_FILES['file']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);

    $sql    = "insert into vendor_quotations(id_vendor, quotation_no, date, attachment, remark) values('$idVendor', '$idQtn', '$date', '$file', '$remark')";

    $result = $con->query($sql);
    echo "<script>parent.location='vendor_quotations.php'</script>";
}

$sql              = "select id, vendor_name from vendor";
$result           = $con->query($sql);
$vendorlist = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorlist, $row);
}

$sql              = "select id, qtn_no from quotation";
$result           = $con->query($sql);
$qtnlist = array();
while ($row = $result->fetch_assoc()) {
    array_push($qtnlist, $row);
}

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Vendor Quotations</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

    <link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
    #name{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
            
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if(!empty($vendor_row['id'])){echo "Edit";}else{echo "Add";}?> Vendor Quotations</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">

                                <label>Vendor Name <span class="text-danger"> *</span></label>
                                <select name="id_vendor" id="id_vendor" class="form-control selitemIcon">
                                    <option value=""> --SELECT-- </option>
                                    <?php
                                        foreach ($vendorlist as $vendor) {
                                        $vendorName = $vendor['vendor_name'];
                                        $id   = $vendor['id'];
                                        if($vendor_row['id_vendor'] == $id){

                                            $selected = "selected";
                                        }
                                        else{
                                            $selected = "";
                                        }
                                        echo "<option value='$id' $selected>$vendorName</option>";
                                        }
                                        ?>
                                </select>
                            </div>
                            <div class="col-sm-4">

                                <label>Quotation No<span class="text-danger"> *</span></label>
                                <input type="text" name="id_qtn" id="id_qtn" class="form-control" value="<?php echo $vendor_row['quotation_no']; ?>">
                            </div>
                              <div class="col-sm-4">

                                <label>Date <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" name="date" id="date" value="<?php if (!empty($vendor_row['date'])) {echo $vendor_row['date'];}?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Attachment <span class="text-danger"> </span></label>
                                <input type="file" name="file" id="file" class="form-control" >
                            </div>
                            <div class="col-sm-4">
                                <label>Remarks <span class="text-danger"> *</span></label>
                                <input type="text" name="remark" id="remark" class="form-control" value="<?php echo $vendor_row['remark']; ?>">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <a href="uploads/<?php echo $vendor_row['attachment']; ?>" target="_blank"><?php echo $vendor_row['attachment']; ?></a>
                            </div>
                        </div>
                    </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <a href="vendor_quotations.php" class="btn btn-error">Cancel</a>
                    <button class="btn btn-success" type="submit" name="<?php if(!empty($vendor_row['id'])){echo "update";}else{echo "save";}?>"><?php if(!empty($vendor_row['id'])){echo "Update";}else{echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_vendor:"required",
                id_qtn : "required",
                date : "required",
                remark : "required"
            },
            messages:{
                id_vendor:"<span>Select Vendor Name</span>",
                id_qtn:"<span>Enter Quotation Number</span>",
                date:"<span>Select Date </span>",
                remark:"<span>Enter Remarks</span>"
            },
        })
    })
</script>
<script type="text/javascript">
        function checkduplicateName(){
          var name = $("#name").val();
          var id = "<?php echo $id;?>";
          console.log(id);

          $.ajax({url: "duplicate_subcategoryname.php?name="+name+"&id="+id, success: function(result){
            if(result==1) {
                alert("Sub Category Name already there");
                $("#name").val(' ');
            }
          }
        });
        }
        
</script>

    <script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
</body>

</html>
