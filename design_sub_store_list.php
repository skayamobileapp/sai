<?php

include 'connection.php';

$viewquery = "Select s.*, m.name as mainstore from sub_store s inner join main_store m on m.id=s.id_main_store ";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{

  $career[$i]['mainstore'] = $row['mainstore'];
  $career[$i]['name'] = $row['name'];
  $career[$i]['category'] = $row['category'];
  $career[$i]['subcategory'] = $row['subcategory'];
  $career[$i]['item'] = $row['item'];
  $career[$i]['contact_no'] = $row['contact_no'];
  $career[$i]['total_racks'] = $row['total_racks'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Design Sub Store</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?sstore_id='+id;
      }
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Sub Stores</h3>
                     <!--<a href="add_sub_store.php" class="btn btn-primary">+ Create Sub Store</a>-->
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Main Store Name</th>
                          <th>Sub Store Name</th>
                          <th>Contact Number</th>
                          <th>Total Racks</th>
                          <th>Design Sub Store</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $mainstore = ucwords($career[$i]['mainstore']);
                            $name = ucwords($career[$i]['name']);
                            $category = ucwords($career[$i]['category']);
                            $subcategory = ucwords($career[$i]['subcategory']);
                            $item = ucwords($career[$i]['item']);
                            $mobile = $career[$i]['contact_no'];
                            $email = $career[$i]['total_racks'];
                            ?>
                        <tr>
                            <td><?php echo $mainstore; ?></td>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $mobile; ?></td>
                          <td><?php echo $email; ?></td>
                          <td><a href="design_sub_store.php?id=<?php echo $id; ?>">Design</a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>