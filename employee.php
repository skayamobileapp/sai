<?php

include 'connection.php';

$viewquery = "Select * from employee";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['employee_id'] = $row['employee_id'];
  $career[$i]['employee_name'] = $row['employee_name'];
  $career[$i]['email'] = $row['email'];
  $career[$i]['phone'] = $row['phone'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employee</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?emp_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Employee</h3>
                     <a href="employee_add.php" class="btn btn-primary">+ Create Employee</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Employee Id</th>
                          <th>Employee Name</th>
                          <th>Mobile</th>
                          <th>Email</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $name = ucwords($career[$i]['employee_name']);
                            $mobile = $career[$i]['phone'];
                            $email = $career[$i]['email'];
                            $eid = $career[$i]['employee_id'];
                            ?>
                        <tr>
                        <td><?php echo $eid; ?></td>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $mobile; ?></td>
                          <td><?php echo $email; ?></td>
                          <td><a href="employee_add.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $id; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>