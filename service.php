<?php

include 'connection.php';
error_reporting(0);
$viewquery = "Select a.*, b.employee_name, b.employee_id, b.phone, b.designation, c.customer_name, c.address from service as a inner join employee as b on a.id_employee=b.id inner join customer as c on a.id_company=c.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['customer_name'] = strtoupper($row['customer_name'])."<br>".$row['address'];
  $career[$i]['employee_name'] = ucwords($row['employee_name'])."-".$row['employee_id']."<br>".$row['designation']."<br>".$row['phone'];
  $career[$i]['date'] = $row['date'];
  $career[$i]['status'] = $row['status'];
  $career[$i]['remarks']= $row['remarks'];
  $career[$i]['attachment']=$row['attachment'];
  $career[$i]['service_no']=$row['service_no'];
  $career[$i]['id'] = $row['id'];
  $i++;
}
if($_POST){
    $checklist = $_POST['check'];
    foreach($checklist as $row){
    $sql = "UPDATE service SET status='Completed' WHERE id=$row";
    $con->query($sql) or die(mysqli_error($con));
    }
    echo "<script>parent.location='service.php'</script>";
}
?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Services</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?comp_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Services</h3>
                        <a href="add_service.php" class="btn btn-primary">+ Create Service</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                            <th>Service No</th>
                            <th>Customer Name</th>
                            <th>Employee Name</th>
                          <th>Date</th>
                          <th>Remarks</th>
                          <th>Check All</th>
                          <th>Approve</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php echo $career[$i]['service_no']; ?></td>
                        <td><?php echo $career[$i]['customer_name']; ?></td>
                          <td><?php echo $career[$i]['employee_name']; ?></td>
                          <td><?php echo $career[$i]['date']; ?></td>
                          <td><?php echo $career[$i]['remarks']; ?></td>
                          <td><?php if($career[$i]['status'] !='Completed'){?>
                              <input type="checkbox" name="check[]" id="check" value="<?php echo $career[$i]['id']; ?>">
                              <?php }else{
                              echo "<h5>Completed</h5>";
                          } ?>
                          </td>
                          <td><a href="add_service.php?id=<?php echo $career[$i]['id']; ?>">Edit/Approve</a></td>
                          <!--<td>-->
                          <!--    <a data-toggle="modal" id="my_id_value" data-target="#myModal" href="#"><img src="<?php echo "uploads/".$career[$i]['attachment']; ?>" id="imgid" width="60px;" onclick="showLargePicture(this.src)" height="30px;" alt="Not Found"></a>-->
                          <!--</td>-->
                        </tr>
                          <?php
                          }
                          ?>
                        </tbody>
                    </table>
                    <div class="page-title clearfix">
                        <button class="btn btn-success" type="submit" name="submit">Approve</button>
                    </div>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>