<?php

include 'connection.php';

$viewquery = "Select b.*, v.name from billing as b inner join vendor as v on b.id_vendor=v.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = $row['name'];
  $career[$i]['billing_number'] = $row['billing_number'];
  $career[$i]['date'] = $row['date'];
  $career[$i]['amount'] = $row['amount'];
  $career[$i]['type'] = $row['type'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Billing</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

  <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
      <link href="../css/dataTables.jqueryui.min.css" rel="stylesheet">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?subid='+id;
      }
    }
  </script>

<style>
   body
   {
    margin:0;
    padding:0;
    background-color:#f1f1f1;
   }
   .box
   {
    width:1270px;
    padding:20px;
    background-color:#fff;
    border:1px solid #ccc;
    border-radius:5px;
    margin-top:25px;
   }
   #page_list li
   {
    padding:16px;
    background-color:#f9f9f9;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
   #page_list li.ui-state-highlight
   {
    padding:24px;
    background-color:#ffffcc;
    border:1px dotted #ccc;
    cursor:move;
    margin-top:12px;
   }
  </style>

</head>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>

                <div class="page-container">
                    <div class="page-title clearfix">
                        <h3>Purchase Bills</h3>
                        <a href="billing_add.php" class="btn btn-primary">+ Add Purchase Bill</a>
                    </div>
                    <div class="header">
                      <table class="table table-striped" id="example">
                        <tr>
                            <th>Name</th>
                          <th>Billing Number</th>
                          <th>Date</th>
                          <th>Amount</th>
                          <th>Edit</th>
                        </tr>
                          <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $name = $career[$i]['name'];
                            $bno = $career[$i]['billing_number'];
                            $date = $career[$i]['date'];
                            $amount = $career[$i]['amount'];
                            ?>
                        <tr>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $bno; ?></td>
                          <td><?php echo date("d M Y", strtotime($date)); ?></td>
                          <td><?php echo $amount; ?></td>
                          <td><a href="billing_add.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a></td>
                          <!--<font size="5px">&nbsp; | &nbsp;</font> <a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x"></i></a>-->
                        </tr>
                          <?php
                          }
                          ?>

                        
                      </table>
                    </div>

                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <script src="../js/jquery-3.3.1.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
       <script src="../js/dataTables.jqueryui.min.js"></script>
       <script>
            $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
  
</body>

</html>