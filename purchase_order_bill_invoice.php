<?php
function numberTowords($num)
{
$ones = array(
0 =>"zero",
1 => "one",
2 => "two",
3 => "three",
4 => "four",
5 => "five",
6 => "six",
7 => "seven",
8 => "eight",
9 => "nine",
10 => "ten",
11 => "eleven",
12 => "twelve",
13 => "thirteen",
14 => "fourteen",
15 => "fifteen",
16 => "sixteen",
17 => "seventeen",
18 => "eighteen",
19 => "nineteen",
"014" => "fourteen"
);
$tens = array( 
0 => "zero",
1 => "ten",
2 => "twenty",
3 => "thirty", 
4 => "forty", 
5 => "fifty", 
6 => "sixty", 
7 => "seventy", 
8 => "eighty", 
9 => "ninety" 
); 
$hundreds = array( 
"hundred",
"thousand", 
"million", 
"billion", 
"trillion", 
"quardrillion" 
); /*limit t quadrillion */
$num = number_format($num,2,".",","); 
$num_arr = explode(".",$num); 
$wholenum = $num_arr[0]; 
$decnum = $num_arr[1]; 
$whole_arr = array_reverse(explode(",",$wholenum)); 
krsort($whole_arr,1); 
$rettxt = ""; 
foreach($whole_arr as $key => $i){
  
while(substr($i,0,1)=="0")
    $i=substr($i,1,5);
if($i < 20){ 
/* echo "getting:".$i; */
$rettxt .= $ones[$i]; 
}elseif($i < 100){ 
if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
}else{ 
if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
} 
if($key > 0){ 
$rettxt .= " ".$hundreds[$key]." "; 
}
} 
if($decnum > 0){
$rettxt .= " and ";
if($decnum < 20){
$rettxt .= $ones[$decnum];
}elseif($decnum < 100){
$rettxt .= $tens[substr($decnum,0,1)];
$rettxt .= " ".$ones[substr($decnum,1,1)];
}
}
return $rettxt;
}

?>
<?php
include('connection.php');
date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
$date = date('d/m/Y');

$id = $_GET['id'];
$poId = $_GET['poid'];
$sql = "SELECT a.*, b.vendor_name, b.address, b.mobile, b.gst, b.contact_person, b.email, c.po_no, c.po_date FROM purchase_bill as a INNER JOIN vendor as b ON a.id_vendor=b.id INNER JOIN purchase_order as c ON a.po_no_id=c.id where a.id='$id' ";
$result = $con->query($sql) or die($con->error);
while ($row = mysqli_fetch_array($result))
{
	$vendor_name = $row['vendor_name'];
	$address = strtolower($row['address']);
  $phone = $row['mobile'];
  $city = ucfirst($row['city']);
  $zipcode = $row['zipcode'];
  $invNo = $row['grn_no'];
  $invDate = $row['grn_date'];
	$gstin = $row['gst'];
	$sgst = $row['sgst_rate'];
	$cgst = $row['cgst_rate'];
	$igst = $row['igst_rate'];
	$poNo = $row['po_no'];
	$totalAmount = $row['total_amount'];
	$totalWithTax = $row['tax_include_amount'];
  $poDate = $row['po_date'];
	$email = $row['email'];
	$remarks = $row['remarks'];
	$kaPerson = ucfirst($row['contact_person']);
	$otherCharges = round($row['other_charges'], 2);
	$esugamNo = $row['esugam'];
	$time = date("H:i:sa");
	$ino = rand();
}

$viewquery = "SELECT a.*, b.name, b.code, b.description, b.hsn_code, b.rate FROM po_outward_items_sold as a INNER JOIN item as b ON a.id_item=b.id where a.id_po_bill='$id'";

$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = $row['name'];
  $career[$i]['code'] = $row['code'];
  $career[$i]['description'] = $row['description'];
  $career[$i]['unit_price']= $row['unit_price'];
  $career[$i]['quantity']= $row['sold_quantity'];
  $career[$i]['total']= round($row['unit_price'] * $row['sold_quantity']);
  $career[$i]['supplied_charge']= $row['supplied_charge'];
  $career[$i]['hsn_code']= $row['hsn_code'];
  $career[$i]['discount']= $row['discount'];
  if($career[$i]['discount'] ==""){
        $career[$i]['discount'] = "0";
      }
  $career[$i]['discount_value']= $row['discount'] / 100;
  $career[$i]['rate']= $row['rate'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

$viewquery = "SELECT a.*, b.name, b.code, b.description, b.hsn_code, b.rate, b.sgst_rate, b.cgst_rate, b.igst_rate FROM po_outward_items_sold as a INNER JOIN item as b ON a.id_item=b.id where a.id_po_bill='$id' ";
$viewqueryresult = mysqli_query($con,$viewquery);
$taxData = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $taxData[$i]['name'] = $row['name'];
  $taxData[$i]['code'] = $row['code'];
  $taxData[$i]['description'] = $row['description'];
  $taxData[$i]['unit_price']= $row['unit_price'];
  $taxData[$i]['quantity']= $row['sold_quantity'];
  $taxData[$i]['total']= round($row['unit_price'] * $row['sold_quantity']);
  $taxData[$i]['hsn_code']= $row['hsn_code'];
  $taxData[$i]['discount']= $row['discount'];
  if($taxData[$i]['discount'] =="")
  {
    $taxData[$i]['discount'] = "0";
  }
  $taxData[$i]['taxable_amount']= $row['taxable_amount'];
  $taxData[$i]['gst_amount']= $row['gst_amount'];
  $taxData[$i]['rate']= $row['rate'];
  $taxData[$i]['sgst_rate']= $row['sgst_rate'];
  $taxData[$i]['cgst_rate']= $row['cgst_rate'];
  $taxData[$i]['igst_rate']= $row['igst_rate'];
  $taxData[$i]['id'] = $row['id'];
  $i++;
}

$sql= "SELECT b.terms_condtion FROM purchase_order_inwards AS a INNER JOIN terms_conditions as b ON a.terms_name=b.id WHERE a.id='$poId'";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $termsCondtion = $row['terms_condtion'];
}

$sql="SELECT sum(sold_quantity) as totalQty FROM po_outward_items_sold WHERE id_po_bill ='$id' ";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $totalQty = $row['totalQty'];
}

$sql="SELECT sum(supplied_charge) as total FROM po_outward_items_sold WHERE id_po_bill ='$id' ";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $totalAmount = round($row['total'], 2);
}

$sql="SELECT sum(taxable_amount) as total FROM po_outward_items_sold WHERE id_po_bill ='$id' ";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $taxableAmount = round($row['total'], 2);
}

$sql="SELECT sum(gst_amount) as total FROM po_outward_items_sold WHERE id_po_bill ='$id' ";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $gstAmount = $row['total']/2;
  $gstTotAmount = $row['total'];
}

 $sql="SELECT sum(gst_rate) as totalgst FROM po_outward_items_sold WHERE id_po_bill ='$id' ";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $totalGST = number_format($row['totalgst']);
}

$totnum = round($taxableAmount + $gstTotAmount + $otherCharges, 2);

$search = '29';
if(preg_match("/{$search}/i", $gstin)) {
    
}

// $totalPO = $totalAmount;
  $amountInWords = ucfirst(numberTowords($gstTotAmount));
  $totalInWords = ucfirst(numberTowords($totnum));

$currentDate = date('d-m-Y');
        $fromDate = $from_date;
    
        $currentTime = date('h:i:s a');
        

        $file_data = $file_data ."
		
		<table width='100%' border='1' style='border-collapse:collapse;'>
          <tr>
            <td colspan='8'>
            	<b style='font-size: 36px; color: blue; text-align: right;'>SAI ENTERPRISES</b> <br>
              <i style='font-size: 12px'>(AN ISO 9001-2015 CERTIFIED COMPANY)</i> <br>
              #107, 1<sup>ST</sup> FLOOR, MEI COLONY, 
              LAGGERE MAIN ROAD,
              PEENYA INDUSTRIAL AREA(WA),
              BENGALURU- 560058. <br>
              <b>GSTIN: 29AHRPA5654N1ZN</b>
            </td>
          </tr>
          <tr><td style='text-align: left; font-size:15px;' colspan='4' rowspan='4'><b>Details of Receiver (BILL TO): </b> 
          <br> M/S.$vendor_name 
          <br> $address <br> PH: $phone <br> KA : $kaPerson &emsp; &emsp; PH: $kaPhone
          <br> GST NO: $gstin </td>
              <td style='text-align: center; font-size:15px;' colspan='2'><b>Invoice No </b></td>
              <td style='text-align: right; font-size:15px;' colspan='2'>$invNo</td>
          </tr>
          <tr>
              <td style='text-align: center; font-size:15px;' colspan='2'><b>Invoice Date </b></td>
              <td style='text-align: right; font-size:15px;' colspan='2'>$invDate</td>
          </tr>
          <tr>
              <td style='text-align: center; font-size:15px;' colspan='2'><b>Ref PO No </b></td>
              <td style='text-align: right; font-size:15px;' colspan='2'>$poNo</td>
          </tr>
          <tr>
              <td style='text-align: center; font-size:15px;' colspan='2'><b>Ref PO Date </b></td>
              <td style='text-align: right; font-size:15px;' colspan='2'>$poDate</td>
          </tr>
          <tr>
          <th style='background-color: #FF8C00'>Sl. No</th>
          <th style='background-color: #FF8C00' >Item Description</th>
          <th style='background-color: #FF8C00'>HSN</th>
          <th style='background-color: #FF8C00'>Qty</th>
          <th style='background-color: #FF8C00'>Unit Price</th>
          <th style='background-color: #FF8C00'>Disc%</th>
          <th style='background-color: #FF8C00'>GST</th>
          <th style='background-color: #FF8C00' >Taxable Amount</th>
          </tr>";

          for ($i=0; $i<count($career); $i++){

          $itemname = $career[$i]['name'];
          $itemcode = $career[$i]['code'];
          $itemdesc = $career[$i]['description'];
          $itemprice = $career[$i]['unit_price'];
          $itemqnty = $career[$i]['quantity'];
          $itemtotal = $career[$i]['total'];
          // $total = $career[$i]['total'];
          $hsncode = $career[$i]['hsn_code'];
          $discount = $career[$i]['discount'];
          $rate = $career[$i]['rate'];
          $percent = $career[$i]['discount_value'] * $itemtotal;
          $totIncDis = round($career[$i]['total'] - $percent, 2);
          $no = $i+1;
       
         $file_data .="
         <tr>
         <td>$no</td>
         <td ><b>$itemname</b> $itemdesc </td>
         <td>$hsncode</td>
         <td>$itemqnty</td>
         <td >$itemprice</td>
         <td>$discount%</td>
         <td>$rate%</td>
         <td>$totIncDis</td>
         </tr>";
          }

          $sql="SELECT sum(discount_amount) as totaldis FROM po_inward_items_sold WHERE id_po_bill ='$id' ";
            $result = mysqli_query($con,$sql);
          while ($row = mysqli_fetch_array($result)){
            $totalDis = round($row['totaldis'], 2);
          }

        $file_data .="<tr><td colspan='2' style='text-align: left'> <b>E-Sug :</b> $esugamNo  </td>
            <td><b>Quantity</b> </td>
            <td> $totalQty </td>
            <td colspan='2'><b> Total Taxable Amount:</b> </td>
            <td colspan='2' style='text-align: right'> $taxableAmount </td>
        </tr>
         <tr><td colspan='4' rowspan='4'> <b>Total Invoice Value In Words:</b> <br> $totalInWords  </td>
         <td colspan='2'><b>SGST:</b> </td>
             <td colspan='2'> $gstAmount </td>
         </tr>
         <tr>
         <td colspan='2'><b>CGST:</b> </td>
             <td colspan='2'> $gstAmount </td>
         </tr>
         <tr>
         <td colspan='2'><b>Total Discount:</b> </td>
             <td colspan='2'> $totalDis</td>
         </tr>
         <tr>
         <td colspan='2'><b>Other Charges:</b> </td>
             <td colspan='2'> $otherCharges</td>
         </tr>
         <tr>
         <td colspan='4'><b> Total Tax In Words:</b> <br> $amountInWords </td>
         <td colspan='2'><b>Total Invoice Value:</b> </td>
             <td colspan='2'>$totnum </td>
         </tr>
         <tr><td style='text-align: left; font-size:15px; background-color: #F4F4F4;' colspan='4'><b>Bank Account Details </b></td>
         <td colspan='4' style='text-align: left; font-size:15px; background-color: #F4F4F4;'> <b>Invoice Remark :</b> </td></tr>
         <tr><td colspan='4'>
         A/c No: 1146201007427 <br>
         Bank Branch: Vijaynagar <br>
         IFSC Code: CNRB 0001146
         </td>
         <td colspan='4' style='vertical-align: top;'>$remarks</td>
         </tr>
         <tr><td style='text-align: left; font-size:15px; background-color: #F4F4F4;' colspan='4'><b>Terms & Conditions </b></td>
             <td style='text-align: left; font-size:15px; background-color: #F4F4F4;' colspan='4'> <b>Receiver’s Seal & Signature</b>
             </td>
         </tr>
         <tr><td colspan='4' >
         <b>Its always a pleasure doing business with you. We will happy to serve you in the futured. Please visit again. <br> Thank You</b>
         <br>
         Goods once sold will not be taken back or exchanged. <br>
         Subject to Banglore Jurisdiction only.
         </td>
         <td colspan='4' style='vertical-align: top;'>
             Receiver Name : <br>
             Ph. No. : <br>
             Seal : </td>
         </tr>
         <tr><td style='text-align: right' colspan='8'><b>For SAI ENTERPRISES</b> <br> <br> <br> <br>  <b>Authorised Signatory</b></td></tr>

      </table>
      <br><br>
      <table width='100%' border='1' style='border-collapse:collapse;'>
      <tr>
      <td rowspan='2' colspan='2'>HSN/SAC</td>
      <td rowspan='2' colspan='2'>TAXABLE VALUE</td>
      <td colspan='2'>CENTRAL TAX</td>
      <td colspan='2'>STATE TAX</td>
      <td rowspan='2'>TOTAL TAX AMOUNT</td>
      </tr><tr>
      <td>RATE %</td>
      <td>AMOUNT</td>
      <td>RATE %</td>
      <td>AMOUNT</td>
      </tr>";
      for ($i=0; $i<count($taxData); $i++){

          $itemname = $taxData[$i]['name'];
          $itemcode = $taxData[$i]['code'];
          $itemdesc = $taxData[$i]['description'];
          $itemprice = $taxData[$i]['unit_price'];
          $itemqnty = $taxData[$i]['total'];
          $itemtotal = $taxData[$i]['unit_price'] * $taxData[$i]['sold_quantity'];
          
          $discount = $taxData[$i]['discount'];
          $taxableAmount = $taxData[$i]['taxable_amount'];
          $rate = $taxData[$i]['rate'];
          $gstAmount = $taxData[$i]['gst_amount'];
          $sgstrate = $taxData[$i]['sgst_rate'];
          $cgstrate = $taxData[$i]['cgst_rate'];

          $hsncode = $taxData[$i]['hsn_code'];
          $pernum = $taxData[$i]['rate']/100;
        $gstnum = $pernum * $itemtotal;
        $divGst = $gstAmount/2;
          $no = $i+1;
      $file_data.="
      <tr>
      <td colspan='2'>$hsncode</td>
      <td colspan='2'>$taxableAmount</td>
      <td>$cgstrate</td>
      <td>$divGst</td>
      <td>$sgstrate</td>
      <td>$divGst</td>
      <td>$gstAmount</td>
      </tr>";
      }
      $file_data.="</table>
      ";

$currentDate = date('d_M_Y_H_i_s');

include("library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->SetFooter('<div style="text-align: center">This is Computer generated bill For SAI ENTERPRISES</div>');
$mpdf->WriteHTML($file_data);
$filename = "PO"."_" .$currentDate.".pdf";
$mpdf->Output($filename, 'I');
        echo "<script>parent.location='generate_po_bill.php?id=$id'</script>";

exit;