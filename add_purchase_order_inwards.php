<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from purchase_order_inwards where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}



if (isset($_POST['save']))
{
	$podate = date("Y-m-d", strtotime($_POST['po_date']));
	$customerId = $_POST['id_customer'];
    $poNo = $_POST['po_no'];
    $date = date("Y-m-d", strtotime($_POST['date']));
    $poValue = $_POST['po_value'];
    $poPending = $_POST['po_value'];
    $remarks = $_POST['remarks'];
    $status = $_POST['status'];

    $file = $_FILES['attachment']['name'];
    $temp = $_FILES['attachment']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);
   
    $sql="INSERT INTO purchase_order_inwards(po_date, id_customer, customer_po_no, date, po_value, po_pending, proforma_no, po_attach, remark, status) VALUES('$podate', '$customerId', '$poNo', '$date', '0.00', '0.00', 'NA', '$file', '$remarks', '$status')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: purchase_order_inwards.php");
}

if (isset($_POST['update']))
{
    $podate = date("Y-m-d", strtotime($_POST['po_date']));
    $customerId = $_POST['id_customer'];
    $poNo = $_POST['po_no'];
    $date = date("Y-m-d", strtotime($_POST['date']));
     $poValue = $_POST['po_value'];
    $remarks = $_POST['remarks'];
    $file = $_FILES['attachment']['name'];
    if($file==""){
        $file = $item['attachment'];
    }else
    {
        $file = $_FILES['attachment']['name'];
        $temp = $_FILES['attachment']['tmp_name'];
        move_uploaded_file($temp, "uploads/".$file);
    }
    
    $id  = $item['id']; 

    $updatequery = "UPDATE purchase_order_inwards SET po_date='$podate', id_customer='$customerId', customer_po_no='$poNo', date='$date', po_value='poValue', po_attach='$file', remark='$remarks' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>parent.location="purchase_order_inwards.php"</script>';
}

  mysqli_query($con, "DELETE FROM temp_items_in_returnable_dc");

$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }

  $sql = "SELECT id, terms_condtion, terms_name FROM terms_conditions";
$result = $con->query($sql);
$termsList = array();
while ($row = $result->fetch_assoc()) {
    array_push($termsList, $row);
  }
  
$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$customerList = array();
while ($row = $result->fetch_assoc()) {
    array_push($customerList, $row);
  }
  $sql = "SELECT id, vendor_name FROM vendor";
$result = $con->query($sql);
$vendorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorList, $row);
  }
 

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> PO Inwards</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    
</style>
<script type="text/javascript">
    function Ondelete(id)
    {
        var sid = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_item_in_nonreturnable.php?id="+id+"&sid="+sid;
      }
    }
</script>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Purchase Order (Inwards)</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PO Received Date<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="po_date" id="po_date" maxlength="50" autocomplete="off" value="<?php echo $item['po_date']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Customer Name<span class="error"> *</span></label>
                            <select name="id_customer" class="form-control selitemIcon" id="id_customer" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i<count($customerList); $i++) {  ?>
                                <option value="<?php echo $customerList[$i]['id']; ?>" <?php if($customerList[$i]['id'] == $item['id_customer_vendor']) { echo "selected=selected"; }
                                    ?>><?php echo $customerList[$i]['customer_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Customer PO NO<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="po_no" id="po_no" maxlength="50" autocomplete="off" value="<?php if(!empty($item['customer_po_no'])){ echo $item['customer_po_no']; } ?>" >
                            </div>
                        </div>
                    </div>
                
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="date" id="date" maxlength="50" autocomplete="off" value="<?php echo $item['date']; ?>">
                        </div>
                    </div>
                        <div class="col-sm-4">
                        <div class="form-group">
                            <label>Attachment<span class="error"> </span></label>
                            <input type="file" class="form-control" name="attachment" id="attachment" autocomplete="off" >
                            </div>
                            <?php if(!empty($item['id'])){ ?>
                                <p>Attached File :</p>
                                <a href="uploads/<?php echo $item['attachment']; ?>" target="_blank"><?php echo $item['attachment']; ?></a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remark']; ?>">
                            </div>
                    </div>
                    
                </div>
                <div id="previous"></div>
                <div style="<?php if($id==''){echo "display:none";}?>">
                                <table id="example" class="table table-striped">
                                   <thead>
                                    <tr>
                                        <th>Item Name</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Discount</th>
                                        <th>Total</th>
                                        <th>Add</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0; $i<count($career); $i++){?>
                                        <tr>
                                            <td><?php echo $career[$i]['item']; ?></td>
                                            <td><?php echo $career[$i]['quantity']; ?></td>
                                            <td><?php echo $career[$i]['unit_price']; ?></td>
                                            <td><?php echo $career[$i]['discount']; ?>%</td>
                                            <td><?php echo $career[$i]['total']; ?></td>
                                            <td><a href="#" name="modal1" data-toggle="modal" data-target="#myModal1" onclick="pass(this)" id="<?php echo $career[$i]['id']; ?>">ADD</a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="purchase_order_inwards.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
  <input type="hidden" name="poinId" id="poinId">  

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;" onchange="getPrice()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                    <div id="qtnfields"></div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Item Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> Supplied<span class="error"> *</span></label>
                        <input type="text" name="supplied" id="supplied" class="form-control">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> Pending<span class="error"> *</span></label>
                        <input type="text" name="pending" id="pending" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> Remarks<span class="error"> *</span></label>
                        <input type="text" name="remark" id="remark" class="form-control">
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="addDetail" id="addDetail" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
    $("#po_date").datepicker();
  });
  </script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            date : "required",
            id_customer: "required",
            terms: "required",
            po_no: "required",
            po_value:"required",
            po_date:"required",
            id_item:"required",
            qnty:"required",
            remarks:"required",
            material_status:"required"
        },
        messages:{
            
            date :"<span>Select Date</span>",
            id_customer:"<span>Select Customer</span>",
            terms:"<span>Select Terms & Conditions</span>",
            po_no:"<span>Enter po number</span>",
            po_value:"<span>Enter po value</span>",
            po_date:"<span>Select po date</span>",
            id_item:"<span>Select Item</span>",
            qnty:"<span>Enter Quantity</span>",
            remarks:"<span>Enter Remarks</span>",
            material_status:"<span>Select Material Status</span>"
    }
    });
});
</script>
<script type="text/javascript">
    function getSubcategory(){
      var id = $("#id_category").val();
      console.log(id);

      $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
        $("#id_subcategory").html(result);
      }
    });
    }
    
    function getItem(){
      var id = $("#id_subcategory").val();
      console.log(id);

      $.ajax({url: "get_items.php?id="+id, success: function(result){
        $("#id_item").html(result);
      }
    });
    }

    function getPrice(){
          var id = $("#id_item").val();
          console.log(id);

          $.ajax({url: "get_fields_for_purchase.php?id="+id, success: function(result){
            $("#qtnfields").html(result);
          }
        });

      }

    function getCalculate(){
        var unit= parseInt($('#unit').val());
            var totqnty = $("#totqnty").val();
            var purchase_cost = parseInt(unit * totqnty);
            // alert(purchase_cost);
            // console.log(purchase_cost);

        var per= $("#discount").val();
        var pernum = (purchase_cost * 0.01 * (100 - per));
        var discountnum = (purchase_cost - pernum);
        var totnum = purchase_cost - discountnum;
        $('#total').val(totnum.toFixed(2));
    }
</script>
<script type="text/javascript">
    function pass(id){
        var strid = $(id).attr('id');
        console.log(strid);
        $("#poinId").val(strid);
    }

    $("#addDetail").on('click',function(){
    var poinId = $("#poinId").val();
    var supplied = $("#supplied").val();
    if(supplied == ""){
        alert('Enter Supplied Quantity');
        return false;
    }
    var pending = $("#pending").val();
    if(pending == ""){
        alert('Enter Pending Quantity');
        return false;
    }

    var remark = $("#remark").val();
    if(remark == ""){
        alert('Enter Remark');
        return false;
    }
    
      $.ajax({

        url: 'add_items_to_po_inwards.php',
        data:{

          'poinId': poinId,
            'remark': remark,
            'supplied': supplied,
            'pending': pending,
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var id_category= $("#id_category").val();
    if(id_category == ""){
        alert('Select Category Name');
        return false;
    }
    var id_subcategory= $("#id_subcategory").val();
    if(id_subcategory == ""){
        alert('Select Sub Category Name');
        return false;
    }
    var item = $("#id_item").val();
    if(item == ""){
        alert('Select Item Name');
        return false;
    }
    var unit = $("#unit").val();
    if(unit == ""){
        alert('Enter Unit Price');
        return false;
    }
    var totqnty = $("#totqnty").val();
    if(totqnty == ""){
        alert('Enter Quantity');
        return false;
    }
    var total = $("#total").val();
    if(total == ""){
        alert('Enter Total');
        return false;
    }
    var discount = $("#discount").val();
    if(discount == ""){
        alert('Enter Discount');
        return false;
    }
    
      $.ajax({

        url: 'add_items_to_po_inwards.php',
        data:{

          'sid': sid,
          'item': item,
           'id_category': id_category,
            'id_subcategory': id_subcategory,
            'unit': unit,
            'totqnty': totqnty,
            'discount': discount,
            'total': total,
        },
        success: function(result){
            $("#previous").html(result);
      }
        });
      });
</script>
<script type="text/javascript">
    function selectField(){
        var value = $("#customer_vendor").val();
        if(value== "customer"){
            $(".customer").show();
            $(".vendor").hide();
        }
        if (value == "vendor") {
            $(".vendor").show();
            $(".customer").hide();
        }
    }
    var value = $("#customer_vendor").val();
        if(value== "customer"){
            $(".customer").show();
            $(".vendor").hide();
        }
        if (value == "vendor") {
            $(".vendor").show();
            $(".customer").hide();
        }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
<script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
</script>
</html>