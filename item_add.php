<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{
    $id = $_GET['id'];
    $sql = "select * from item where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
    
    $viewquery = "Select * from item_has_images where id_item='$id'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['attachment'] = $row['attachment'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }
} else {
    $id=0;
}

if (isset($_POST['save']))
{

    $id_category = $_POST['id_category'];
    $id_subcategory = $_POST['id_subcategory'];
    $itemname = strtoupper($_POST['item_name']);
    
    $sql = "select * from category where id = $id_category";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    
    $sql = "select * from sub_category where id = $id_subcategory" ;
    $result = $con->query($sql);
    $row1 = $result->fetch_assoc();
    
    $sql      = "select * from item where id_subcategory=$id_subcategory";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
    $ref= sprintf("%'.05d", $resnum);

    $itemcode = $row1['sub_cat_code']."-".$ref;

    $hsncode = $_POST['hsn_code'];
    $description = $_POST['description'];
    $re_order_level = $_POST['re_order_level'];
    $unit = $_POST['measurement_unit'];
    $basic_purchase = $_POST['basic_purchase_rate'];
    $margin = $_POST['margin'];
    $sales_basic = $_POST['sales_basic_price'];
    $net_profit = $_POST['net_profit'];
    $discount = $_POST['discount'];
    $gst_type = $_POST['gst_type'];
    $unitrate = $_POST['rate'];
    $orgrate = $_POST['orgrate'];
    $brand_name = $_POST['brand_name'];
    $technical_detail = $_POST['technical_detail'];
    $file = $_FILES['technical_attach']['name'];
    $temp = $_FILES['technical_attach']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);
    $sgst = $_POST['sgst_rate'];
    $cgst = $_POST['cgst_rate'];
    $igst = $_POST['igst_rate'];
    $total = $_POST['total'];

   $sql1 = "SELECT * FROM item WHERE name like '%$itemname%' AND id_category='$id_category' AND id_subcategory='$id_subcategory'";
      $result1  = $con->query($sql1);
      $resnum = mysqli_num_rows($result1);
      if($resnum < 1){

    $sql = "insert into item(id_category, id_subcategory, code, name, description, re_order_level, hsn_code, measurement_unit, basic_purchase_rate, margin, sales_basic_price, net_profit, discount, gst_type, rate, original_cost, sgst_rate, cgst_rate, igst_rate, total_amount, id_brand, technical_detail, technical_attach, status) values('$id_category', '$id_subcategory', '$itemcode', '$itemname', '$description', '$re_order_level', '$hsncode', '$unit',  '$basic_purchase', '$margin', '$sales_basic', '$net_profit', '$discount', '$gst_type', '$unitrate', '$orgrate', '$sgst', '$cgst', '$igst', '$total', '$brand_name', '$technical_detail', '$file', 1) ";
    
    $con->query($sql) or die(mysqli_error($con));
    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];
    
    $tempsql = mysqli_query($con,"SELECT * FROM temp_item_has_images WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['attachment'] = $row['attachment'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $cname = $fetch[$i]['attachment'];

   $insertbill = "INSERT INTO item_has_images(id_item, attachment) VALUES ('$last_id', '$cname')";
    $result = mysqli_query($con,$insertbill);
    }
      }
      else{
          echo "<script>alert('Item Already Exists')</script>";
           echo "<script>parent.locaiton='item_add.php'</script>";
      }

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: item.php");
}

if (isset($_POST['update']))
{
    $id  = $item['id'];

    $id_category = $_POST['id_category'];
    $id_subcategory = $_POST['id_subcategory'];
    $itemname = strtoupper($_POST['item_name']);
    $hsncode = $_POST['hsn_code'];
    $description = $_POST['description'];
    $re_order_level = $_POST['re_order_level'];
    $unit = $_POST['measurement_unit'];
    $basic_purchase = $_POST['basic_purchase_rate'];
    $margin = $_POST['margin'];
    $sales_basic = $_POST['sales_basic_price'];
    $net_profit = $_POST['net_profit'];
    $discount = $_POST['discount'];
    $gst_type = $_POST['gst_type'];
    $gstrate = $_POST['rate'];
    $orgrate = $_POST['orgrate'];
    $brand_name = $_POST['brand_name'];
    $technical_detail = $_POST['technical_detail'];
    $file = $_FILES['technical_attach']['name'];
    if($file == ""){
        $file == $item['technical_attach'];
    }
    else{
        $file = $_FILES['technical_attach']['name'];
        $temp = $_FILES['technical_attach']['tmp_name'];
        move_uploaded_file($temp, "uploads/".$file);
    }

    $sgst = $_POST['sgst_rate'];
    $cgst = $_POST['cgst_rate'];
    $igst = $_POST['igst_rate'];
    $total = $_POST['total'];
 
  $tempsql = mysqli_query($con,"SELECT * FROM temp_item_has_images WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['attachment'] = $row['attachment'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $cname = $fetch[$i]['attachment'];

   $insertbill = "INSERT INTO item_has_images(id_item, attachment) VALUES ('$id', '$cname')";
    $result = mysqli_query($con,$insertbill);
    }
    
    $updatequery = "update item set id_category = '$id_category', id_subcategory = '$id_subcategory', name='$itemname', hsn_code='$hsncode', description='$description', re_order_level='$re_order_level', measurement_unit='$unit', rate='$gstrate', basic_purchase_rate='$basic_purchase', margin='$margin', sales_basic_price='$sales_basic', net_profit='$net_profit', discount='$discount', gst_type='$gst_type', original_cost='$orgrate', sgst_rate='$sgst', cgst_rate='$cgst', igst_rate='$igst', total_amount='$total', id_brand='$brand_name', technical_detail='$technical_detail', technical_attach='$file'  where id = $id";
 
    $res=$con->query($updatequery);
    if($res){
        echo "<script>parent.location='item.php'</script>";
        exit;
    }
    
echo "<script>parent.locaiton='item.php'</script>";
}

 $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  $sql = "SELECT id, brand_name FROM brands";
$result = $con->query($sql);
$brandList = array();
while ($row = $result->fetch_assoc()) {
    array_push($brandList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_item_has_images");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Item</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    #item_name{
        text-transform: UPPERCASE;
    }
    #file{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Item</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                <label>Category <span class="error"> *</span></label>
                                <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="category">
                                    <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                        <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                            </option>;
                                    <?php
                                    }
                                    ?>
                                </select>
                              </div>

                              <div class="col-sm-4">
                                <label>Sub Category <span class="error"> *</span></label>
                                <select name="id_subcategory" class="form-control selitemIcon" id="subcategory">
                                    <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_subcategory'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                    <?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?>
                                    <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_subcategory'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                                </select>
                            
                            </div>
                            <div class="col-sm-4">
                                <label>Item Name <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="item_name" id="item_name" value="<?php if (!empty($item['name'])) {echo $item['name'];}?>" autocomplete="off"  onblur="checkduplicateName()">
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Description <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="description" value="<?php if (!empty($item['description'])) {echo $item['description'];}?>" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <label>Re-Order Level <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="re_order_level" value="<?php if (!empty($item['re_order_level'])) {echo $item['re_order_level'];}?>" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <label>HSN Code <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="hsn_code" value="<?php if (!empty($item['hsn_code'])) {echo $item['hsn_code'];}?>" autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Measurement Unit <span class="error"> *</span></label>
                                <select class="form-control selitemIcon" name="measurement_unit">
                                    <option value="">SELECT</option>
                                    <option value="kg" <?php if($item['measurement_unit']=='kg') { echo "selected";}?>>Kilogram (kg), for mass.</option>
                                    <option value="li" <?php if($item['measurement_unit']=='li') { echo "selected";}?>>Litres (li), for liquid.</option>
                                    <option value="m" <?php if($item['measurement_unit']=='m') { echo "selected";}?>>Meter (m), for distance.</option>
                                    <option value="no" <?php if($item['measurement_unit']=='no') { echo "selected";}?>>Number (no), for unit.</option>
                                    <option value="other" <?php if($item['measurement_unit']=='other') { echo "selected";}?>>Others (o).</option>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label>Basic Purchase Rate <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="basic_purchase_rate" id="basic_purchase_rate" value="<?php if (!empty($item['basic_purchase_rate'])) {echo $item['basic_purchase_rate'];}?>" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <label> Margin in Percentage(%)<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="margin" id="margin" value="<?php if (!empty($item['margin'])) {echo $item['margin'];}?>" autocomplete="off" onkeyup="getSalesBasicPrice()">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label> Sales Basic Price <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="sales_basic_price" id="sales_basic_price" value="<?php if (!empty($item['sales_basic_price'])) {echo $item['sales_basic_price'];}?>" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <label> Net Profit <span class="error"> *</span></label>
                                <input type="text" class="form-control" name="net_profit" id="net_profit" value="<?php if (!empty($item['net_profit'])) {echo $item['net_profit'];}?>" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <label>Discount in Percentage(%)</label>
                                <input type="text" class="form-control" name="discount" id="discount" value="<?php if (!empty($item['discount'])) {echo $item['discount'];}?> " onkeyup="getdiscount()" autocomplete="off">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Purchase value</label>
                                <input type="text" class="form-control" name="orgrate" id="orgrate" value="<?php if (!empty($item['original_cost'])) {echo $item['original_cost'];}?>" onkeyup="getGST()" autocomplete="off">
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group"> <br>
                                    <p><label>GST Type <span class='error'>*</span></label></p>
                                    <label class="radio-inline">
                                    <input type="radio" name="gst_type" id="gst_type1" value="local" onchange="getGstRate1()" <?php if($item['gst_type']=='local'){ echo "checked"; }?>><span class="check-radio"></span> Local
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="gst_type" id="gst_type2" value="inter-state" onchange="getGstRate2()" <?php if($item['gst_type']=='inter-state'){ echo "checked"; }?>><span class="check-radio"></span> Inter State
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label>GST</label>
                                <select class="form-control selitemIcon" name="rate" id="rate" onchange="getGST()">
                                    <option value="">SELECT</option>
                                    <option value="5" <?php if($item['rate']=='5') { echo "selected";}?>>5% GST</option>
                                    <option value="12" <?php if($item['rate']=='12') { echo "selected";}?>>12% GST</option>
                                    <option value="18" <?php if($item['rate']=='18') { echo "selected";}?>>18% GST</option>
                                    <option value="28" <?php if($item['rate']=='28') { echo "selected";}?>>28% GST</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 local" style="display: none" id="local">
                                <label>SGST Rate</label>
                                <input type="text" class="form-control" name="sgst_rate" id="sgst_rate" value="<?php if (!empty($item['sgst_rate'])) {echo $item['sgst_rate'];}?>">
                            </div>
                            <div class="col-sm-4 local" style="display: none" id="local">
                                <label>CGST Rate</label>
                                <input type="text" class="form-control" name="cgst_rate" id="cgst_rate" value="<?php if (!empty($item['cgst_rate'])) {echo $item['cgst_rate'];}?>">
                            </div>
                            <div class="col-sm-4" style="display: none" id="inter">
                                <label>IGST Rate</label>
                                <input type="text" class="form-control" name="igst_rate" id="igst_rate" value="<?php if (!empty($item['igst_rate'])) {echo $item['igst_rate'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <label>Brand Name</label>
                                <select name="brand_name" class="form-control selitemIcon" id="brand_name">
                                    <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i<count($brandList); $i++) {  ?>
                                    <option value="<?php echo $brandList[$i]['id']; ?>" <?php if($brandList[$i]['id'] == $item['id_brand'])  { echo "selected=selected"; }
                                    ?>><?php echo $brandList[$i]['brand_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Total Cost</label>
                                <input type="text" class="form-control" name="total" id="total" value="<?php if (!empty($item['total_amount'])) {echo $item['total_amount'];}?>" readonly>
                            </div>
                            <div class="col-sm-4">
                                <label>Technical Attachment</label>
                                <input type="file" class="form-control" name="technical_attach" value="<?php if (!empty($item['technical_attach'])) {echo $item['technical_attach'];}?>">
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add More Attachments</button>
                                </div>
                            </div>
                        </div> <br>
                        <div id="addedfiles"></div>
                        <div class="row">
                            <div class="col-sm-10">
                                <label>Technical Detail</label>
                                <textarea class="form-control ckeditor" name="technical_detail" id="technical_detail"><?php if (!empty($item['technical_detail'])) {echo $item['technical_detail'];}?></textarea>
                            </div>
                        </div>
                        <br>
                        
                        <div style="<?php if($id==''){echo "display:none";}?>">
                            <table class="table table-striped" id="example">
                               <thead>
                            		<tr>
                                		<th>Attachment List</th>
                                		<th>Action</th>
                            		</tr>
                            	</thead>
                            	<tbody>
                            		    <?php for($i=0; $i<count($career); $i++){
                            		        $cid = $career[$i]['id'];
                            		    ?>
                            		    <tr>
                            		        <td><a href='uploads/<?php echo $career[$i]['attachment']; ?>'><?php echo $career[$i]['attachment']; ?></a></td>
                            		  <td><a href="item_attachment_delete.php?id=<?php echo $cid; ?>&strId=<?php echo $id; ?>">DELETE</a></td>
                            		    </tr>
                            		    <?php } ?>
                            	</tbody>
                        	</table>
                        </div>
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="item.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add More Attachments</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Upload Attachment</label>
                    <input type="file" class="form-control" name="file" id="file" autocomplete="off">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <!-- Placed at the end of the document so the pages load faster -->
   <!--  <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script> -->
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#subcategory").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_category:"required",
                id_subcategory:"required",
                item_name : "required",
                item_code : "required",
                description : "required",
                hsn_code : "required",
                brand_name : "required",
                gst_type : "required"
            },
            messages:{
                id_category:"<span>Select Category Name</span>",
                id_subcategory:"<span>Select Sub Category Name</span>",
                item_name:"<span>Enter Item Name</span>",
                item_code:"<span>Enter Item code</span>",
                description:"<span>Enter Description</span>",
                hsn_code:"<span>Enter HSN Code</span>",
                brand_name:"<span>Select Brand Name</span>",
                gst_type:"<span>Select Gst Type</span>"
            },
        })
    })
</script>
<script>
    function getSalesBasicPrice(){
        var purchase_cost= parseInt($('#basic_purchase_rate').val());
        var per= parseInt($("#margin").val());
        var pernum = (purchase_cost * 0.01 * (100 - per));
        var marginum = (purchase_cost - pernum);
        var totnum = purchase_cost + marginum;
        $('#sales_basic_price').val(totnum.toFixed(2));
    }

    function getGST(){
        var org_cost= parseInt($('#orgrate').val());
        var per= $("#rate").val();
        var pernum = (org_cost * 0.01 * (100 - per));
        var gstnum = (org_cost - pernum);
        var totnum = org_cost + gstnum;
        
        var sales= $("#sales_basic_price").val();
        var diffnum = sales - org_cost;
        $('#net_profit').val(diffnum.toFixed(2));
        
        var tot = (((totnum - org_cost) * 100) / org_cost);
        var value = tot.toFixed(2)/2;
        $('#cgst_rate').val(value +' %');
        $('#sgst_rate').val(value +' %');
        $('#igst_rate').val(per +' %');
        $('#total').val(totnum.toFixed(2));
    }
    
    function getdiscount(){
        var purchase_cost= parseInt($('#basic_purchase_rate').val());
        var per= $("#discount").val();
        var pernum = (purchase_cost * 0.01 * (100 - per));
        var discountnum = (purchase_cost - pernum);
        var totnum = purchase_cost - discountnum;
        $('#orgrate').val(totnum.toFixed(2));
    }
    
    function getGstRate1(){
        var type = $("#gst_type1").val();
        if(type == 'local'){
            $(".local").show();
            $("#inter").hide();
        }
    }
    
    function getGstRate2(){
        var type = $("#gst_type2").val();
        if(type == 'inter-state')
        {
            $(".local").hide();
            $("#inter").show();
        }
    }
    var org_cost= parseInt($('#orgrate').val());
        var per= $("#rate").val();
        var pernum = (org_cost * 0.01 * (100 - per));
        var gstnum = (org_cost - pernum);
        var totnum = org_cost + gstnum;
        
        var tot = (((totnum - org_cost) *  100) / org_cost);
        var value = tot.toFixed(2)/2;
        $('#cgst_rate').val(value +' %');
        $('#sgst_rate').val(value +' %');
        $('#igst_rate').val(per +' %');
        $('#total').val(totnum.toFixed(2));
        
        var gstType = '<?php echo $item["gst_type"]; ?>';
        if(gstType == 'local'){
            $(".local").show();
        }
        if(gstType == 'inter-state'){
            $("#inter").show();
        }
        
</script>
<script type="text/javascript">
    function Deleteimg(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
         parent.location='item_attachment_delete.php?id='+id;
          
      }
    }
  </script>
<script type="text/javascript">
    $("#addCompetitor").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var name= $("#name").val();
    if(name == ""){
        alert('Enter Name');
        return false;
    }
    var price= $("#price").val();
    if(price == ""){
        alert('Enter Price');
        return false;
    }
    
      $.ajax({

        url: 'add_item_competitors.php',
        data:{

          'sid': sid,
          'name': name,
           'price': price,
        },
        success: function(result){
            $("#competitors").html(result);
      }
        });
      });
</script>
  <script type="text/javascript">
        function checkduplicateName(){
          var name = $("#item_name").val();
          var id = "<?php echo $id;?>";
          console.log(id);

          $.ajax({url: "duplicate_itemname.php?name="+name+"&id="+id, success: function(result){
            if(result==1) {
                alert("Item Name already there");
                $("#item_name").val(' ');
            }
          }
        });
        }
        
</script>

</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
<script type="text/javascript">
      $(document).ready(function (e) {
          $('#add').on('click', function () {
              var sid = '<?php echo $sid; ?>';
              var file_data = $('#file').prop('files')[0];
              if(file_data == undefined){
                alert('UPLOAD FILE');
                return false;
              }
              var form_data = new FormData();
              form_data.append('file', file_data);
              form_data.append('sid', sid);

              $.ajax({
                  url: 'add_item_images.php', // point to server-side PHP script 
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function (response) {
                      $('#addedfiles').html(response); // display success response from the PHP script
                      $('#file').val('');
                  },
              });
          });
      });
</script>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
          var sid = '<?php echo $sid; ?>';
          var option = 'delete';
      $.ajax({

        url: 'add_item_images.php',
        data:{

          'sid': sid,
          'option': option,
           'id': id,
        },
        success: function(response){
        $("#addedfiles").html(response);
      }
        });
      }
    }
  </script>
<!-- <script src="js/jquery-3.3.1.js"></script> -->
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
</html>