<?php

include 'connection.php';
$item_id = $_GET['id'];

if($id = $_GET['id']){
	$subList = [];
$sql = "SELECT *  FROM sub_store WHERE id = '$id' ";
$result = $con->query($sql);
$subList = $result->fetch_assoc();
}
$sub_tore_name = $subList['name'];

$viewquery = "SELECT a.*, d.code, d.hsn_code, b.name as subName, c.name as mainName, d.name as itemName, e.name as subcatName, f.name as catName FROM sub_store_has_items as a INNER JOIN sub_store as b ON b.id=a.id_sub_store INNER JOIN main_store as c ON c.id=b.id_main_store INNER JOIN item as d ON d.id=a.id_item INNER JOIN sub_category as e ON d.id_subcategory=e.id INNER JOIN category as f ON d.id_category=f.id WHERE a.id_sub_store=$item_id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
    $career[$i]['code'] = $row['code'];
  $career[$i]['subcatName'] = $row['subcatName'];
  $career[$i]['catName'] = $row['catName'];
  $career[$i]['itemName'] = $row['itemName'];
  $career[$i]['mainName'] = $row['mainName'];
  $career[$i]['subName'] = $row['subName'];
  $career[$i]['hsn_code'] = $row['hsn_code'];
  $career[$i]['rack_no'] = $row['rack_no'];
  $career[$i]['self_no'] = $row['self_no'];
  $career[$i]['unit_price'] = $row['unit_price'];
  $career[$i]['gst_rate'] = $row['gst_rate'];
  $career[$i]['total_value'] = $row['total_value'];
  $career[$i]['quantity'] = $row['quantity'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sub Store Item List</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?comp_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Items in Sub Store - <?php echo $sub_tore_name; ?></h3>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                            <th>Item Code</th>
                            <th>Category Name</th>
                          <th>Sub Category Name</th>
                          <th>Item Name</th>
                          <th>HSN Code</th>
                          <th>Rack No</th>
                          <th>Self No</th>
                          <th>Qnty</th>
                          <th>Unit Price</th>
                          <th>GST Rate</th>
                          <th>Total Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $name = $career[$i]['itemName'];
                            $subcatName = $career[$i]['subcatName'];
                            $catname = $career[$i]['catName'];
                            $code = $career[$i]['code'];
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                        <td><?php echo $code; ?></td>
                        <td><?php echo $catname; ?></td>
                        <td><?php echo $subcatName; ?></td>
                        <td><?php echo $name; ?></td>
                        <td><?php echo $career[$i]['hsn_code']; ?></td>
                        <td><?php echo $career[$i]['rack_no']; ?></td>
                        <td><?php echo $career[$i]['self_no']; ?></td>
                        <td><?php echo $career[$i]['quantity']; ?></td>
                        <td><?php echo $career[$i]['unit_price']; ?></td>
                        <td><?php echo $career[$i]['gst_rate']; ?></td>
                        <td><?php echo $career[$i]['total_value']; ?></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>