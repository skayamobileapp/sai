<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select a.name as subName, b.name as mainName, b.address, b.mobile, b.email from sub_store as a INNER JOIN main_store as b ON a.id_main_store=b.id where a.id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

 $viewquery = "SELECT a.*, b.name as item, b.code, c.name as category, sc.name as subcat FROM design_sub_store as a INNER JOIN item as b ON a.id_item=b.id INNER JOIN category as c ON a.id_category=c.id INNER JOIN sub_category as sc ON a.id_sub_category=sc.id WHERE a.id_sub_store ='$id' order by a.rack_alloted_for_item, a.self_alloted_for_item asc";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $career[$i]['id'] = $row['id'];
      $career[$i]['item'] = $row['item']."-".$row['code'];
      $career[$i]['rack_no'] = $row['rack_alloted_for_item'];
      $career[$i]['self_no'] = $row['self_alloted_for_item'];
      $career[$i]['quantity'] = $row['no_items_self'];
      $i++;
    }

if (isset($_POST['update']))
{

    
    $id  = $_GET['id'];
    
    $tempsql = mysqli_query($con,"SELECT * FROM temp_sub_store_has_items WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['id_item'] = $row['id_item'];
    $fetch[$i]['rack_no'] = $row['rack_no'];
    $fetch[$i]['self_no'] = $row['self_no'];
    $fetch[$i]['unit_price'] = $row['unit_price'];
    $fetch[$i]['quantity'] = $row['quantity'];
    $fetch[$i]['total_quantity'] = $row['total_quantity'];
    $fetch[$i]['unit'] = $row['unit'];
    $fetch[$i]['gst_rate'] = $row['gst_rate'];
    $fetch[$i]['total_value'] = $row['total_value'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $id_item = $fetch[$i]['id_item'];
    $rackNo = $fetch[$i]['rack_no'];
    $selfNo= $fetch[$i]['self_no'];
    $up= $fetch[$i]['unit_price'];
    $qnty= $fetch[$i]['quantity'];
    $totqnty= $fetch[$i]['total_quantity'];
    $gst= $fetch[$i]['gst_rate'];
    $total= $fetch[$i]['total_value'];
    
    $check ="SELECT * FROM sub_store_has_items WHERE rack_no='$rnum' AND self_no = '$snum' AND id_sub_store='$strid' ORDER BY id DESC LIMIT 0, 1";
  $checkresult = mysqli_query($con,$check);

    $subStoreItem = $checkresult->fetch_assoc();
    $final = $subStoreItem['quantity']+$qnty;
  if($final <= $totqnty){

   $insertbill = "INSERT INTO sub_store_has_items(id_sub_store, id_item, rack_no, self_no, unit_price, quantity, total_quantity, gst_rate, total_value) VALUES ('$id', '$id_item', '$rackNo', '$selfNo', '$up', '$final', '$totqnty', '$gst', '$total')";
   
    $result = mysqli_query($con,$insertbill);
      
    }else{
      echo "<script>alert('self is full!! Cannot add items anymore.')</script>";
  }
  }

        echo '<script>alert("Item added successfully")</script>';
        echo '<script>parent.location="sub_store_list.php"</script>';
}

$sql = "SELECT id, name FROM main_store";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT a.id, a.name, a.category_code FROM category as a INNER JOIN design_sub_store as b ON  b.id_category=a.id WHERE b.id_sub_store='$id'";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, vendor_name FROM vendor";
$result = $con->query($sql);
$vendorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_sub_store_has_items");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Add";} else {echo "Add";}?> Items To Store</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3> Material Issue Note (Stock Reduction)</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Main Store Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" maxlength="50" autocomplete="off" value="<?php echo $item['mainName']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Sub Store Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" maxlength="50" autocomplete="off" value="<?php echo $item['subName']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Address<span class="error">*</span></label>
                                <input type="text" class="form-control" name="address" readonly id="address" autocomplete="off" value="<?php echo $item['address']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Phone Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="mobile" id="mobile" maxlength="50" autocomplete="off" value="<?php echo $item['mobile']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email<span class="error">*</span></label>
                                <input type="email" class="form-control" name="email" id="email" autocomplete="off" value="<?php echo $item['email']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <!--<div class="row">-->
                    <!--    <div class="col-sm-6">-->
                    <!--        <div class="form-group">-->
                    <!--            <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Items To Store</button>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <div id="previous"></div>
                    
                    <div class="">
                      <table class="table table-striped" id="example">
                          <thead>
                        <tr>
                           <th >Item Name</th>
                    		<th>Rack Number</th>
                    		<th>Self Number</th>
                    		<th>Max Qty</th>
                    		<th>Sale Qty</th>
                    		<th>History</th>
                        </tr>
                        </thead>
                          <?php
                          for ($i=0; $i<count($career); $i++)
                          { ?>
                        <tr>
                            <td><?php echo $career[$i]['item']; ?></td>
                          <td><?php echo $career[$i]['rack_no']; ?></td>
                          <td><?php echo $career[$i]['self_no']; ?></td>
                          <td><?php echo $career[$i]['quantity']; ?></td>
                          <td><a href="#" data-toggle="modal" data-target="#myModal" onclick="pass(this)" id="<?php echo $career[$i]['id']; ?>">Sale</a> </td> 
                          <td><a href="sale_history.php?strId=<?php echo $id; ?>&rno=<?php echo $career[$i]['rack_no']; ?>&sno=<?php echo $career[$i]['self_no']; ?>">HISTORY</a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        
                      </table>
                    </div>

                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <input type="hidden" name="store" id="store" >
             <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Material Issue Note(Stock Reduction)</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Sale Quantity</label>
                    <input type="text" name="qntysale" id="qntysale" class="form-control">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
   
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#bill_date" ).datepicker();
  } );
  </script>
<script type="text/javascript">
        function getPrice(){
        //   var id = $("#item").val();
          var id = $("#store").val();
          console.log(id);

          $.ajax({url: "get_price.php?id="+id, success: function(result){
            $("#result").html(result);
          }
        });
        }
    </script>
    <script type="text/javascript">
        function getSubcategory(){
          var id = $("#id_category").val();
          console.log(id);
          var store_id = '<?php echo $id; ?>';

          $.ajax({url: "get_subcategory_for_transfer.php?id="+id+"&strId="+store_id, success: function(result){
            $("#id_subcategory").html(result);
          }
        });
        }
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);
          var store_id = '<?php echo $id; ?>';

          $.ajax({url: "get_items_for_transfer.php?id="+id+"&strId="+store_id, success: function(result){
            $("#item").html(result);
          }
        });
        }
</script>
<script type="text/javascript">
    function pass(id){
        var strid = $(id).attr('id');
        console.log(strid);
        $("#store").val(strid);
    }
        
    $("#add").on('click',function(){
    var strid = '<?php echo $id; ?>';
     var id = $("#store").val();
    var qntysale = $("#qntysale").val();
    if(qntysale == ""){
        alert('Enter Sale Quantity');
        return false;
    }
    
      $.ajax({

        url: 'sale_items_from_store.php',
        data:{

          'id': id,
          'strid': strid,
           'qntysale': qntysale,
            
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
    </script>
    <script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
          var sid = '<?php echo $sid; ?>';
          var option = 'delete';
      $.ajax({

        url: 'add_items_to_store.php',
        data:{

          'sid': sid,
          'option': option,
           'id': id,
        },
        success: function(result){
        $("#previous").html(result);
      }
        });
      }
    }
  </script>

</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>