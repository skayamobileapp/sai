<?php

include 'connection.php';

$viewquery = "Select i.*, s.name as subcatName, c.name as categoryName from item as i inner join sub_category as s on i.id_subcategory=s.id inner join category as c on i.id_category=c.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = strtoupper($row['name']);
  $career[$i]['subcatName'] = strtoupper($row['subcatName']);
  $career[$i]['categoryName'] = strtoupper($row['categoryName']);
  $career[$i]['code'] = $row['code'];
  $career[$i]['basic_purchase_rate'] = $row['basic_purchase_rate'];
  $career[$i]['sales_basic_price'] = $row['sales_basic_price'];
  $career[$i]['net_profit'] = $row['net_profit'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Items</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='?item_id='+id;
        alert('Item added somewhere cant be deleted');
      }
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Items</h3>
                     <a href="item_add.php" class="btn btn-primary">+ Create Item</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                            <th>Item Code</th>
                            <th>Item Name</th>
                          <th>Sub Category Name</th>
                          <th>Category Name</th>
                          <th>Purchase Basic Rate</th>
                          <th>Sales Basic Price</th>
                          <th>Net Profit</th>
                          <th>Technical specification</th>
                                                    <th>Actions</th>

                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $name = $career[$i]['name'];
                            $subcatName = $career[$i]['subcatName'];
                            $catname = $career[$i]['categoryName'];
                            $code = $career[$i]['code'];
                            $purRate = $career[$i]['basic_purchase_rate'];
                            $sales = $career[$i]['sales_basic_price'];
                            $profit = $career[$i]['net_profit'];
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                        <td><?php echo $code; ?></td>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $subcatName; ?></td>
                          <td><?php echo $catname; ?></td>
                          <td><?php echo $purRate; ?></td>
                          <td><?php echo $sales; ?></td>
                          <td><?php echo $profit; ?></td>
                          
                          <td><a href="item_technical.php?id=<?php echo $id; ?>">Add Technical Specification</a></td>
                          <td><a href="item_add.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x" title="DELETE"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>