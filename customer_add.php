<?php
include 'connection.php';
error_reporting(0);
date_default_timezone_set("Asia/Kolkata");

session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from customer where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
    
    $viewquery = "Select * from customer_contact_details where customer_id='$id'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['contact_name'] = $row['contact_name'];
      $career[$i]['designation'] = $row['designation'];
      $career[$i]['contact_mobile'] = $row['contact_mobile'];
      $career[$i]['contact_email'] = $row['contact_email'];
      $career[$i]['department'] = $row['department'];
      $career[$i]['land_line'] = $row['land_line'];
      $career[$i]['unit'] = $row['unit'];
      $career[$i]['remark'] = $row['remark'];
      $career[$i]['send_mail'] = $row['send_mail'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }
}

if (isset($_POST['save']))
{

    $customerName = $_POST['customer_name'];
    $address=$_POST['address'];
    $customerCode=$_POST['customer_code'];
    $balance= $_POST['balance'];
    $scope= $_POST['scope'];
    $contactPerson = $_POST['contact_person'];
    $bType = $_POST['btype'];
    $mobile = $_POST['mobile'];
    $email = $_POST['email'];
    $gst = $_POST['gst'];
    $nature = $_POST['nature'];
    $ka = $_POST['kindly_attention'];
    $kaph = $_POST['ka_phone'];
    $address2=$_POST['address2'];
    $city=$_POST['city'];
    $zipcode=$_POST['zipcode'];
    $landLine = $_POST['land_line'];
    $tanNo = $_POST['tan_no'];
    $panNo = $_POST['pan_no'];
    
   
    $sql="INSERT INTO customer(customer_name, address, customer_code, balance, contact_person, scope, business_type, id_nature_of_company, mobile, email, gst_no,address2,city,zipcode, kindly_attention, ka_phone, land_line, tan_no, pan_no) VALUES('$customerName','$address','$customerCode','$balance','$scope','$contactPerson','$bType', '$nature', '$mobile','$email', '$gst','$address2','$city','$zipcode', '$ka', '$kaph', '$landLine', '$tanNo', '$panNo')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

$tempsql = mysqli_query($con,"SELECT * FROM temp_customer_contact_details WHERE session_id='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['contact_name'] = $row['contact_name'];
    $fetch[$i]['designation'] = $row['designation'];
    $fetch[$i]['contact_mobile'] = $row['contact_mobile'];
    $fetch[$i]['contact_email'] = $row['contact_email'];
    $fetch[$i]['department'] = $row['department'];
    $fetch[$i]['land_line'] = $row['land_line'];
    $fetch[$i]['unit'] = $row['unit'];
    $fetch[$i]['remark'] = $row['remark'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $cname = $fetch[$i]['contact_name'];
    $cdesig = $fetch[$i]['designation'];
    $cmobile = $fetch[$i]['contact_mobile'];
    $cmail= $fetch[$i]['contact_email'];
    $dept= $fetch[$i]['department'];
    $ladline= $fetch[$i]['land_line'];
    $unit= $fetch[$i]['unit'];
    $remark= $fetch[$i]['remark'];

   $insertbill = "INSERT INTO customer_contact_details(customer_id, contact_name, designation, department, land_line, contact_mobile, contact_email, unit, remark) VALUES ('$last_id', '$cname', '$cdesig', '$dept', '$ladline', '$cmobile', '$cmail', '$unit', '$remark')";
    $result = mysqli_query($con,$insertbill);
    }

    header("location: customer.php");
}

if (isset($_POST['update']))
{

    $customerName = $_POST['customer_name'];
    $address=$_POST['address'];
    $customerCode=$_POST['customer_code'];
    $balance= $_POST['balance'];
    $scope= $_POST['scope'];
    $contactPerson = $_POST['contact_person'];
    $bType = $_POST['btype'];
    $mobile = $_POST['mobile'];
    $email = $_POST['email'];
    $nature = $_POST['nature'];
    $gst = $_POST['gst'];
    $ka = $_POST['kindly_attention'];
    $kaph = $_POST['ka_phone'];
    $address2=$_POST['address2'];
    $city=$_POST['city'];
    $zipcode=$_POST['zipcode'];
    $landLine = $_POST['land_line'];
    $tanNo = $_POST['tan_no'];
    $panNo = $_POST['pan_no'];
    
    $id  = $item['id'];
    $updatequery = "update customer set customer_name = '$customerName', address = '$address', customer_code='$customerCode', balance='$balance', contact_person='$contactPerson', scope='$scope', business_type='$bType', id_nature_of_company='$nature', mobile='$mobile', email='$email', gst_no='$gst', kindly_attention='$ka', ka_phone='$kaph', 
    address2='$address2', city='$city', zipcode='$zipcode', land_line='$landLine', tan_no='$tanNo', pan_no='$panNo' where id = $id";

    $tempsql = mysqli_query($con,"SELECT * FROM temp_customer_contact_details WHERE session_id='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['contact_name'] = $row['contact_name'];
    $fetch[$i]['designation'] = $row['designation'];
    $fetch[$i]['contact_mobile'] = $row['contact_mobile'];
    $fetch[$i]['contact_email'] = $row['contact_email'];
    $fetch[$i]['department'] = $row['department'];
    $fetch[$i]['land_line'] = $row['land_line'];
    $fetch[$i]['unit'] = $row['unit'];
    $fetch[$i]['remark'] = $row['remark'];
    $i++;
  }

  for($i=0; $i<count($fetch); $i++){
    $cname = $fetch[$i]['contact_name'];
    $cdesig = $fetch[$i]['designation'];
    $cmobile = $fetch[$i]['contact_mobile'];
    $cmail= $fetch[$i]['contact_email'];
    $dept= $fetch[$i]['department'];
    $ladline= $fetch[$i]['land_line'];
    $unit= $fetch[$i]['unit'];
    $remark= $fetch[$i]['remark'];

   $insertbill = "INSERT INTO customer_contact_details(customer_id, contact_name, designation, department, land_line, contact_mobile, contact_email, unit, remark) VALUES ('$id', '$cname', '$cdesig', '$dept', '$ladline', '$cmobile', '$cmail', '$unit', '$remark')";
    $result = mysqli_query($con,$insertbill);
    }

    $res=$con->query($updatequery);

        echo '<script>parent.location="customer.php"</script>';
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name FROM business_types";
$result = $con->query($sql);
$businessTypeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($businessTypeList, $row);
  }
  
  $sql = "SELECT id, name FROM nature_of_company";
$result = $con->query($sql);
$companyNatureList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyNatureList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_customer_contact_details");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Customer</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>
<script type="text/javascript">
    function Ondelete(cid)
    {
        var id = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_customer_contact.php?cid='+cid+"&id="+id;
      }
    }
    
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <!-- <li class="dropdown"> -->
                          <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mani <span class="caret"></span></a> -->
                          <!-- <ul class="dropdown-menu"> -->
                            <!-- <li><a href="#">Action</a></li> -->
                            <!-- <li><a href="#">Another action</a></li> -->
                            <!-- <li><a href="#">Something else here</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="../index.php">Logout</a></li>
                          <!-- </ul> -->
                        <!-- </li> -->
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Customer</h3>
                    </div>
                    
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2">Customer Name<span class="error">*</span></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="customer_name" id="customer_name" maxlength="50" autocomplete="off" value="<?php echo $item['customer_name']; ?>">
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-2">Address<span class="error">*</span></label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="address" id="address" maxlength="150" autocomplete="off" rows="3"><?php echo $item['address']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <br>
                      <div class="row">
                        <label class="col-sm-2">City<span class="text-danger"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="city" id="city" maxlength="40" autocomplete="off" value="<?php echo $item['city']; ?>">
                            </div>
                        </div>
                        
                        <label class="col-sm-1">ZipCode<span class="text-danger"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="zipcode" id="zipcode" maxlength="20" autocomplete="off" value="<?php echo $item['zipcode']; ?>">
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <label class="col-sm-2">Customer Code<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="customer_code" id="customer_code" maxlength="50" autocomplete="off" value="<?php echo $item['customer_code']; ?>">
                            </div>
                        </div>
                        <label class="col-sm-1">Opening Balance<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="balance" id="balance" maxlength="50" autocomplete="off" value="<?php echo $item['balance']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2">Business Type<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control selitemIcon" name="btype" id="btype">
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($businessTypeList); $i++){?>
                                    <option value="<?php echo $businessTypeList[$i]['id']?>" <?php if($item['business_type']==$businessTypeList[$i]['id']){ echo "selected=selected"; }?>><?php echo $businessTypeList[$i]['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <label class="col-sm-1">Company Nature<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select class="form-control selitemIcon" name="nature" id="nature">
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($companyNatureList); $i++){?>
                                    <option value="<?php echo $companyNatureList[$i]['id']?>" <?php if($item['id_nature_of_company']==$companyNatureList[$i]['id']){ echo "selected=selected"; }?>><?php echo $companyNatureList[$i]['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2">Email Id<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" id="email" maxlength="40" autocomplete="off" value="<?php echo $item['email']; ?>">
                            </div>
                        </div>
                        <label class="col-sm-1">Contact Person<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="contact_person" id="contact_person" maxlength="50" autocomplete="off" value="<?php echo $item['contact_person']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2">Phone<span class="text-danger"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="mobile" id="mobile" maxlength="20" autocomplete="off" value="<?php echo $item['mobile']; ?>">
                            </div>
                        </div>
                        <label class="col-sm-1">Landline No<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="land_line" id="land_line" maxlength="40" autocomplete="off" value="<?php echo $item['land_line']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2">Tan<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="tan_no" id="tan_no" maxlength="40" autocomplete="off" value="<?php echo $item['tan_no']; ?>">
                            </div>
                        </div>
                        <label class="col-sm-1">Pan<span class="text-danger"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="pan_no" id="pan_no" maxlength="50" autocomplete="off" value="<?php echo $item['pan_no']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2">Kindly Attention<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="kindly_attention" id="kindly_attention" maxlength="40" autocomplete="off" value="<?php echo $item['kindly_attention']; ?>">
                            </div>
                        </div>
                        <label class="col-sm-1">KA Phone<span class="text-danger"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="ka_phone" id="ka_phone" maxlength="50" autocomplete="off" value="<?php echo $item['ka_phone']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2">GST No.<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="gst" id="gst" maxlength="30" autocomplete="off" value="<?php echo $item['gst_no']; ?>">
                            </div>
                        </div>
                        
                         <label class="col-sm-1">Invoice Days<span class="error"></span></label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="invoice_days" id="invoice_days" maxlength="30" autocomplete="off" value="<?php echo $item['invoice_days']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add More Contacts</button>
                            </div>
                        </div>
                    </div>
    
                            <div id="previous"></div>
                            
                            <div style="<?php if($id==''){echo "display:none";}?>">
                                <table class='table table-striped' id="example">
                                   <thead>
                            		<tr>
                                		<th>Contact Name</th>
                                		<th>Designation</th>
                                		<th>Contact Number</th>
                                		<th>Contact Email</th>
                                		<th>Department</th>
                                		<th>Land Line</th>
                                		<th>Unit</th>
                                        <th>Contact for Mail</th>
                                        <th>Actions</th>
                            		</tr>
                            		</thead>
                            		<tbody>
                            		    <?php for($i=0; $i<count($career); $i++){
                                            $mailnote = $career[$i]['send_mail']; ?>
                            		    <tr>
                            		        <td><?php echo $career[$i]['contact_name']; ?></td>
                            		        <td><?php echo $career[$i]['designation']; ?></td>
                            		        <td><?php echo $career[$i]['contact_mobile']; ?></td>
                            		        <td><?php echo $career[$i]['contact_email']; ?></td>
                            		        <td><?php echo $career[$i]['department']; ?></td>
                            		        <td><?php echo $career[$i]['land_line']; ?></td>
                            		        <td><?php echo $career[$i]['unit']; ?></td>
                                            <td><input type="checkbox" name="mailcontact" id="mailcontact" value="<?php echo $career[$i]['id'] ?>" <?php if($mailnote=="yes"){ echo "checked=checked"; }?>></td>
                                            <td><a href="javascript:Ondelete(<?php echo $career[$i]['id']; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                            		    </tr>
                            		    <?php } ?>
                            		</tbody>
                        		</table>
                            </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="customer.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>

  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add More Contacts</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-4">
                    <label>Contact Name</label>
                    <input type="text" class="form-control" name="cname" id="cname" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Designation</label>
                    <input type="text" class="form-control" name="designation" id="designation" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Contact Number</label>
                    <input type="text" class="form-control" name="cnum" id="cnum" autocomplete="off" maxlength="10">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label>Land Line Number</label>
                    <input type="text" class="form-control" name="lnum" id="lnum" autocomplete="off" maxlength="10">
                </div>
                <div class="col-sm-4">
                    <label>Contact Email Id</label>
                    <input type="text" class="form-control" name="cmail" id="cmail" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Department</label>
                    <input type="text" class="form-control" name="department" id="department" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Group/Unit</label>
                    <input type="text" class="form-control" name="unit" id="unit" autocomplete="off">
                </div>
                <div class="col-sm-4">
                    <label>Remark</label>
                    <input type="text" class="form-control" name="remark" id="remark" autocomplete="off">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
  <!-- <script src="js/jquery-3.3.1.js"></script> -->
   
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
    $('#address').keyup(function () {
       var new_stuff = $(this).val();
           new_stuff = new_stuff.replace(/[\n\r]+/g,"");
       var test = new_stuff.replace(/(.{63})/g,"$1\n");
       $(this).val(test);
    });
</script>
<script>    
    $(function () {
        $("#mailcontact").click(function () {
            if ($("#mailcontact").is(':checked')) {
                var id = $("#mailcontact").val();
                var status = "yes";
                $.ajax({url: "attach_contact_for_mail.php?id="+id+"&status="+status, success: function(result){
                    $("#mailResult").html(result);
                    location.reload();
              }
            });
            }
            else{
            var id = $("#mailcontact").val();
            var status = "no";

                $.ajax({url: "attach_contact_for_mail.php?id="+id+"&status="+status, success: function(result){
                    $("#mailResult").html(result);
                    location.reload();
              }
            });
            }
        });
    });
    </script>
<script type="text/javascript">
    $('#address').keyup(function () {
       var new_stuff = $(this).val();
           new_stuff = new_stuff.replace(/[\n\r]+/g,""); // clean out newlines, so we dont get dups!
       var test = new_stuff.replace(/(.{50})/g,"$1\n");
       $(this).val(test);
    });
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var cname = $("#cname").val();
    if(cname == ""){
        alert("Enter Customer Name");
        return false;
    }
    var desig = $("#designation").val();
    var cnum = $("#cnum").val();
    if(cnum == ""){
        alert("Enter Contact Number");
        return false;
    }
    var cmail = $("#cmail").val();
    var dept = $("#department").val();
    var lnum = $("#lnum").val();
    var unit = $("#unit").val();
    var remark = $("#remark").val();
    
      $.ajax({

        url: 'add_customer_contact.php',
        data:{

          'sid': sid,
          'cname': cname,
          'desig': desig,
           'cnum': cnum,
            'cmail': cmail,
            'dept': dept,
            'lnum': lnum,
            'unit': unit,
            'remark': remark,
        },
        success: function(result){
        $("#previous").html(result);
        $('#cname').val('');
        $('#designation').val('');
        $('#cnum').val('');
        $('#cmail').val('');
        $('#department').val('');
        $('#lnum').val('');
        $('#unit').val('');
        $('#remark').val('');
      }
        });
      });
    </script>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

       <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            customer_name : "required"
        },
        messages:{
            customer_name : "<span>Enter Customer name</span>"
    }
    })
})
</script>
<script src="js/jquery.dataTables.min.js"></script>
   <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
</script>
</body>
</html>