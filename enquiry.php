<?php

include 'connection.php';

$viewquery = "SELECT a.*, b.name as enqName, c.customer_name, c.address, c.mobile, d.employee_name, d.employee_id, d.address as empAddress, d.phone FROM enquiry as a INNER JOIN enquiry_type as b ON a.enq_type=b.id INNER JOIN customer as c ON a.customer_id=c.id INNER JOIN employee as d ON a.enquire_handler=d.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{

  $career[$i]['enq_no'] = $row['enq_no'];
  $career[$i]['customer_name'] = $row['customer_name']."<br>".$row['address']."<br>".$row['mobile'];
  $career[$i]['enqName'] = $row['enqName'];
  $career[$i]['employee_name'] = $row['employee_name']."-".$row['employee_id']."<br>".$row['empAddress']."<br>".$row['phone'];
  $career[$i]['remarks'] = $row['remarks'];
  $career[$i]['status'] = $row['status'];
  $career[$i]['date'] = $row['date'];
  $career[$i]['enq_date'] = $row['enq_date'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Enquiry</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?enq_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Enquiry</h3>
                     <a href="enquiry_add.php" class="btn btn-primary">+ Create Enquiry</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                              <th>Enquiry No</th>
                            <th>Date</th>
                                <th>Enquiry Type</th>
                          <th>Enquiry Handeld Engineer</th>
                          <th>Customer Name</th>
                          <th>Qtn Status</th>
                          <th>Qtn Date</th>
                          <th>Remarks</th>
                          <!-- <th>Quotaion Sent</th> -->
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $name = $career[$i]['enq_no'];
                            $type = $career[$i]['enqName'];
                            $bill = $career[$i]['employee_name'];
                            $quote = $career[$i]['customer_name'];
                            $remarks = $career[$i]['remarks'];
                            $qtn = $career[$i]['status'];
                            $enqdate = $career[$i]['date'];
                            $date = $career[$i]['enq_date'];
                            ?>
                        <tr>
                          <td><?php echo $name; ?></td>
                          <td><?php echo $enqdate; ?></td>
                          <th><?php echo $type; ?></th>
                          <td><?php echo $quote; ?></td>
                          <td><?php echo $bill; ?></td>
                          <td><?php echo $qtn; ?></td>
                          <td><?php echo $date; ?></td>
                          <td><?php echo $remarks; ?></td>
                          <!-- <td><?php echo $qtn; ?></td> -->
                          <td><a href="enquiry_add.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $id; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>