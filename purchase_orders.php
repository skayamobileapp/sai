<?php

include 'connection.php';

$viewquery = "SELECT a.*, b.vendor_name, b.address, b.mobile FROM purchase_order as a INNER JOIN vendor as b ON a.id_vendor=b.id ORDER BY a.id DESC";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['po_no'] = $row['po_no'];
  $career[$i]['vendor_name'] = $row['vendor_name']."<br>".substr($row['address'], 20)." <br>".$row['mobile'];
  $career[$i]['status'] = $row['status'];
  $career[$i]['remarks'] = $row['remarks'];
  $career[$i]['po_date']= $row['po_date'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Purchase Order</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?qtn_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Purchase Order (Outwards)</h3>
                     <a href="purchase_order.php" class="btn btn-primary">Back</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                            <th>PO No.</th>
                          <th>Vendor</th>
                          <th>Fallup Date</th>
                          <th>Total Value</th>
                          <th>Status</th>
                          <th>Remarks</th>
                          <th>Generate PO</th>
                          <!-- <th>Edit&nbsp; | &nbsp;Delete</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            ?>
                        <tr>
                          <td><?php echo $career[$i]['po_no']; ?></td>
                          <td><?php echo $career[$i]['vendor_name']; ?></td>
                          <td><?php echo $career[$i]['po_date']; ?></td>
                          <td><?php
                          $sql="SELECT sum(total) as totalAmount FROM po_outwards_items WHERE id_poout ='".$career[$i]['id']."' ";
                                $result = mysqli_query($con,$sql);
                              while ($row = mysqli_fetch_array($result)){
                               echo $totalAmount = $row['totalAmount'];
                              } 
                          ?> </td>
                          <td><?php echo $career[$i]['status']; ?></td>
                          <td><?php echo $career[$i]['remarks']; ?></td>
                          <td><a href="generate_po_outwards.php?id=<?php echo $career[$i]['id']; ?>" class="btn btn-primary">Generate PO</a></td>
                          <!-- <td><a href="quotation_add.php?id=<?php echo $career[$i]['id']; ?>"><i class="fa fa-edit fa-2x"></i></a><font size="4px">&nbsp; | &nbsp;</font> <a href="javascript:Ondelete(<?php echo $career[$i]['id']; ?>);"><i class="fa fa-trash fa-2x"></i></a></td> -->
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>