<?php
include('connection.php');

date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
$date = date('d/m/Y');

$id = $_GET['id'];

$query = "select po.*, v.vendor_name, v.email, v.address, v.mobile, v.gst from purchase_order AS po INNER JOIN vendor AS v ON po.id_vendor=v.id where po.id = $id";

    $result1 = $con->query($query);
    $poVendor = $result1->fetch_assoc();
    
    $poNo = $poVendor['po_no'];
      $vendorEmail = $poVendor['email'];
      $vendorAddress = $poVendor['address'];
      $vendorMobile = $poVendor['mobile'];
      $vendorGSTNo = $poVendor['gst'];
    $vendorName = strtoupper($poVendor['vendor_name']);
    $regardName = ucwords($poVendor['regard_name']);
      $regardPhone = $poVendor['regard_phone'];
      $billingAddress = $poVendor['billing_address'];


$currentDate = date('d-m-Y');
        $fromDate = $from_date;
    
        $currentTime = date('h:i:s a');

        $file_data = $file_data ."<br>
        <h3 style='text-align: center; color: red;'>DISPATCH INSTRUCTION</h3>
      <table cellspacing='0' cellpadding='0' style='width: 100%'>
          <tr>
            <th style='text-align: right; color: red;'>  </th>
          </tr>
          <tr>
            <th style='text-align:left'>DEAR SIR/MADAM, </th>
          </tr>
          <tr>
            <td style='text-align:center; color:blue;'><br> <br> PO NO : $poNo DATE :  $date</td>
          </tr>
      </table>
      <br><br>
      <table cellspacing='0' cellpadding='0' style='width: 100%'>
      <tr><th style='text-align: left;'>
        <b style='color: blue;'>KINDLY DISPATCH THE MATERIAL AS PER BELOW INSTRUCTIONS WITH PROPER PACKING TO AVOID ANY DAMAGES DURING TRANSIT, </b> <b style='color:red'>DO THE NEEDFUL CC ATTACH.</b><th>
        </tr>
        <tr><th style='text-align: left; color: blue;'><br><br>COURIER CHARGES  PAID /TO-PAY </th></tr>
        <tr><th style='color:red; text-align: left'><br>BILLING ADDRESS: <br><br></th></tr>
        <tr>
        <td style='text-align: left;'><br><br>$billingAddress</td>
        </tr>
        <tr><th style='color:red; text-align: left'><br><br>
      <b >DELIVERY ADDRESS: - PAID BASIS – TECHTROL SCOPE</b> <br></th></tr>
      <tr><td style='text-align: left;'>
      <br><br>
      $vendorName <br><br>
      $vendorAddress <br><br>
      $vendorMobile <br><br>
      $vendorEmail <br><br>
      GST: $vendorGSTNo<br>
      </td></tr>
      <tr><td>
      <br><br>
      <b style='color: red'>REGARDS</b> <br><br>
      M/S. SAIENTERPRISES<br><br>
      $regardName - $regardPhone
      </td></tr> 
      </table>";

$currentDate = date('d_M_Y_H_i_s');

include("library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->SetFooter('<div style="text-align: center">No. 107, 1st Floor, MEI Colony, Laggere main Road, Peenya Industrial Area (Wd.), Bengaluru 560058 <br>
  Ph: 080 - 2839 8596, Mobile: +91 93797 94006, E-mail: saientp_ajit@hotmail.com, Website: saienpl.com</div>');
$mpdf->WriteHTML($file_data);
$filename = "DI"."_" .$currentDate.".pdf";
$mpdf->Output($filename, 'I');
exit;