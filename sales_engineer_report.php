<?php

include 'connection.php';

 $viewquery = "SELECT a.id, a.date, b.employee_name, b.employee_id FROM sales_engineer_report AS a INNER JOIN employee AS b ON a.id_employee=b.id  
ORDER BY `a`.`date`  DESC";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = strtoupper($row['itemName']);
  $career[$i]['date'] = $row['date'];
  $career[$i]['employee_name'] = $row['employee_name']."-".$row['employee_id'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sales Engineer Report</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function popup(id)
    {
        alert('Sent Succefully');
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Sales Engineer Report List</h3>
                     <a href="add_sales_engineer_report.php" class="btn btn-primary">+ Create Sales Engineer Report.</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                            <th>Date</th>
                            <th>Employee Name</th>
                          <th>Add Customers</th>
                          <!--<th>Send Mail</th>-->
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            ?>
                        <tr>
                          <td><?php echo $career[$i]['date']; ?></td>
                            <td><?php echo $career[$i]['employee_name']; ?></td>
                          <td><a href="add_sales_engineer_report.php?id=<?php echo $id; ?>" class="btn btn-primary">ADD CUSTOMER</a></td>
                          <!--<td><a href="#?id=<?php echo $id; ?>" onclick="popup()">SEND</a></td>-->
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>