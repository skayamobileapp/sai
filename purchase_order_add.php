<?php
include 'connection.php';
date_default_timezone_set("Asia/Kolkata");
session_start();
$sid = session_id();
$eid = $_SESSION['employee']['id'];
$empId = $_SESSION['employee']['employee_id'];

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from purchase_order where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

$sql      = "select * from purchase_order";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
      $currMonth = date("m");
      $currYear = date("y");
      $nxtYear = date("y", strtotime("+1 year"));
    $ref= sprintf("%'.04d", $resnum);
    $serialNo = $ref."/".$currMonth."/".$currYear."-".$nxtYear;

if (isset($_POST['save']))
{
    $poNo =$_POST['po_no'];
    $vendor = $_POST['id_vendor'];
    $remarks = $_POST['remarks'];
    $status = $_POST['status'];
    if($status == ""){
        $status = "pending";
    }
    $file = $_FILES['attachment']['name'];
    $temp = $_FILES['attachment']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);
    $date = date("Y-m-d", strtotime($_POST['po_date']));
   
    $sql="INSERT INTO purchase_order(po_no, id_vendor, status, remarks, attachment, po_date) VALUES('$poNo','$vendor', '$status', '$remarks', '$file', '$date')";

    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: purchase_order.php");
}

if (isset($_POST['update']))
{

    $poNo =$_POST['po_no'];
    $vendor = $_POST['id_vendor'];
    $remarks = $_POST['remarks'];
    $status = $_POST['status'];
    $terms = $_POST['terms'];

    $file = $_FILES['attachment']['name'];
    if($file == ""){
        $file = $item['attachment'];
    }
    else{
        $file = $_FILES['attachment']['name'];
     $temp = $_FILES['attachment']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);   
    }

    $date = date("Y-m-d", strtotime($_POST['po_date']));
    
    $id  = $item['id'];
    $updatequery = "update purchase_order set po_no='$poNo', id_vendor='$vendor', status='$status', remarks='$remarks', attachment='$file', po_date='$date', id_terms_conditions='$terms' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="purchase_order.php"</script>';
}


$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, vendor_name FROM vendor";
$result = $con->query($sql);
$vendorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorList, $row);
  }
  
  $sql = "SELECT id, employee_name FROM employee";
$result = $con->query($sql);
$employeeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($employeeList, $row);
  }
  
  $sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }

  $sql = "SELECT id, terms_condtion, terms_name FROM terms_conditions";
$result = $con->query($sql);
$termsList = array();
while ($row = $result->fetch_assoc()) {
    array_push($termsList, $row);
  }

  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_quotation_has_items");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Purchase Order</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#po_date" ).datepicker();
  } );
  </script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Purchase Order (Outwards)</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PO NO<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="po_no" id="po_no" maxlength="50" autocomplete="off" value="<?php if(!empty($item['po_no'])){ echo $item['po_no']; } else { echo $serialNo; } ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Vendor<span class="error"> *</span></label>
                                <select class="form-control selitemIcon" name="id_vendor" id="id_vendor" onchange="getEnquiryfields()">
                                    <option value="">select</option>
                                    <?php for($i=0; $i<count($vendorList); $i++){?>
                                    <option value="<?php echo $vendorList[$i]['id']; ?>" <?php if($vendorList[$i]['id']==$item['id_vendor']){ echo "selected=selected"; } ?>><?php echo $vendorList[$i]['vendor_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Fallup Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="po_date" id="po_date" autocomplete="off" value="<?php echo $item['po_date']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks<span class="error">*</span></label>
                                <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remarks']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Attachment<span class="error"></span></label>
                                <input type="file" class="form-control" name="attachment" id="attachment" autocomplete="off" value="<?php echo $item['attachment']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <br>
                                <label>Status <span class="error"> *</span></label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" id="status" value="pending" <?php if($item['status']=='pending'){ echo "checked"; }?>><span class="check-radio"></span> Pending
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="status" id="status" value="approve"  <?php if($item['status']=='approve'){ echo "checked"; }?>><span class="check-radio"></span> Approve
                                    </label>
                            </div>
                        </div>
                    </div>
                    </div>

                </div>
                <!--<div class="row">-->
                <!--        <div class="col-sm-6">-->
                <!--            <div class="form-group">-->
                <!--                <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Items</button>-->
                <!--            </div>-->
                <!--        </div>-->
                <!--</div>-->
                <div id="previous"></div>
                    
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="purchase_order.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
        <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items To Store</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{

            po_no : "required",
            id_vendor:"required",
            po_date:"required",
            remarks : "required",
            terms : "required",
            status : "required",
            esi_code : "required",
            address : "required",
            email : "required",

            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{

            po_no : "<span>Enter PO Number</span>",
            id_vendor:"<span>Select Vendor Name</span>",
            po_date:"<span>Select PO Date</span>",
            remarks:"<span>Enter Remarks</span>",
            terms:"<span>Select Terms & Conditions</span>",
            status : "<span>Select Status</span>",
            esi_code : "<span>Enter ESI Code Number</span>",
            address : "<span>Enter Address</span>",
            email : "<span>Enter Email Id</span>",
           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
        function getSubcategory(){
          var id = $("#id_category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#id_subcategory").html(result);
          }
        });
        }
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#id_item").html(result);
          }
        });
        }
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $sid; ?>';
    var id_category= $("#id_category").val();
    if(id_category == ""){
        alert('Select Category Name');
        return false;
    }
    var id_subcategory= $("#id_subcategory").val();
    if(id_subcategory == ""){
        alert('Select Sub Category Name');
        return false;
    }
    var item = $("#id_item").val();
    if(item == ""){
        alert('Select Item Name');
        return false;
    }
    
      $.ajax({

        url: 'add_item_to_quotation.php',
        data:{

          'sid': sid,
          'item': item,
           'id_category': id_category,
            'id_subcategory': id_subcategory,
        },
        success: function(result){
            $("#previous").html(result);
        
      }
        });
      });
</script>
<script type="text/javascript">
        function getEnquiryfields(){
        //   var id = $("#item").val();
          var id = $("#enquiry").val();
          console.log(id);

          $.ajax({url: "get_enquiry_fields.php?id="+id, success: function(result){
            $("#result").html(result);
          }
        });
        }
        
        var id = $("#enquiry").val();
        if(id != ''){
          console.log(id);

          $.ajax({url: "get_enquiry_fields.php?id="+id, success: function(result){
            $("#result").html(result);
          }
        });
        }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>