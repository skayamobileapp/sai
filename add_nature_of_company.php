<?php
include 'connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from nature_of_company where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{

    $nature = strtoupper($_POST['name']);
   
    $sql="INSERT INTO nature_of_company(name) VALUES('$nature')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: nature_of_company.php");
}

if (isset($_POST['update']))
{

    $nature = strtoupper($_POST['name']);
    
    $id  = $item['id'];
    $updatequery = "update nature_of_company set name = '$nature' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>parent.location="nature_of_company.php"</script>';
}


?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Nature Of Company</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    input{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <!-- <li class="dropdown"> -->
                          <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mani <span class="caret"></span></a> -->
                          <!-- <ul class="dropdown-menu"> -->
                            <!-- <li><a href="#">Action</a></li> -->
                            <!-- <li><a href="#">Another action</a></li> -->
                            <!-- <li><a href="#">Something else here</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="../index.php">Logout</a></li>
                          <!-- </ul> -->
                        <!-- </li> -->
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Nature Of Company</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="name" id="name" maxlength="100" autocomplete="off" value="<?php echo $item['name']; ?>">
                            </div>
                        </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="nature_of_company.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{

            name : "required"
        },
        messages:{

            name : "<span>Enter Company Nature Name</span>"
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>