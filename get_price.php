<?php

include 'connection.php';

if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "SELECT a.id, a.rack_alloted_for_item, a.self_alloted_for_item, a.no_items_self, b.name as catName, b.category_code, c.name as subCat, c.sub_cat_code, d.name as item, d.code, d.total_amount, d.rate, d.original_cost FROM design_sub_store as a INNER JOIN category as b ON a.id_category=b.id INNER JOIN sub_category as c ON c.id=a.id_sub_category INNER JOIN item as d ON a.id_item=d.id WHERE a.id = '$id'";
    $result = $con->query($sql);
    $item = $result->fetch_assoc();

 $table =   "<div class='col-sm-6'>
                <label>Category Name</label>
                <input type='text' class='form-control' name='id_category' id='id_category' autocomplete='off' value='".$item['catName']."-".$item['category_code']."' readonly>
            </div>
            <div class='col-sm-6'>
                <label>Sub Category</label>
                <input type='text' class='form-control' name='id_subcategory' id='id_subcategory' autocomplete='off' value='".$item['subCat']."-".$item['sub_cat_code']."' readonly>
            </div>
            <div class='col-sm-6'>
                <label>Item Name</label>
                <input type='text' class='form-control' name='item' id='item' autocomplete='off' value='".$item['item']."-".$item['code']."' readonly>
            </div>
            <div class='col-sm-6'>
                <label>Purchase Price</label>
                <input type='text' class='form-control' name='uprice' id='uprice' autocomplete='off' value='".$item['original_cost']."' readonly>
            </div>
            <div class='col-sm-6'>
                <label>GST Rate</label>
                <input type='text' class='form-control' name='rate' id='rate' autocomplete='off' value='".$item['rate']."%"."' readonly>
            </div>
            <div class='col-sm-6'>
                <label>Total Price</label>
                <input type='text' class='form-control' name='total' id='total' autocomplete='off' value='".$item['total_amount']."' readonly>
            </div>
            <div class='col-sm-6'>
                <label>Rack Number</label>
                <input type='text' class='form-control' name='rnum' id='rnum' autocomplete='off' value='".$item['rack_alloted_for_item']."'readonly >
            </div>
            <div class='col-sm-6'>
                <label>Self Number</label>
                <input type='text' class='form-control' name='snum' id='snum' autocomplete='off' value='".$item['self_alloted_for_item']."' readonly>
            </div>
            <div class='col-sm-6'>
                <label>Max Quantity</label>
                <input type='text' class='form-control' name='totqnty' id='totqnty' autocomplete='off' value='".$item['no_items_self']."' readonly>
            </div>";

echo $table;

}

?>
