<?php

include 'connection.php';

$viewquery = "Select a.*, b.customer_name, d.name as subcatName, e.name as categoryName, c.name as itemName from sr_no as a inner join customer as b on a.id_customer=b.id inner join item as c on a.id_item=c.id inner join sub_category as d on a.id_subcategory=d.id inner join category as e on a.id_category=e.id";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = strtoupper($row['itemName']);
  $career[$i]['date'] = $row['date'];
  $career[$i]['categoryName'] = strtoupper($row['categoryName']);
  $career[$i]['customer_name'] = $row['customer_name'];
  $career[$i]['po_num'] = $row['po_num'];
  $career[$i]['po_date'] = $row['po_date'];
  $career[$i]['remark'] = $row['remark'];
  $career[$i]['sr_num'] = $row['sr_num'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SR. NO Sales Update</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?item_id='+id;
      }
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>SR. NO. List</h3>
                     <a href="add_sr_no.php" class="btn btn-primary">+ Create SR. NO.</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                            <th>SR. NO</th>
                            <th>Date</th>
                          <th>Customer Name</th>
                          <th>PO NO</th>
                          <th>PO Date</th>
                          <th>Item Name</th>
                          <th>Remarks</th>
                          <th>Edit&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                          <td><?php echo $career[$i]['sr_num']; ?></td>
                          <td><?php echo $career[$i]['date']; ?></td>
                          <td><?php echo $career[$i]['customer_name']; ?></td>
                          <td><?php echo $career[$i]['po_num']; ?></td>
                          <td><?php echo $career[$i]['po_date']; ?></td>
                          <td><?php echo $career[$i]['name']; ?></td>
                          <td><?php echo $career[$i]['remark']; ?></td>
                          <td><a href="add_sr_no.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a><font size="5px">&nbsp;</td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>