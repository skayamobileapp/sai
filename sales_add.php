<?php
include 'connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from sales where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{

    $enquiry =$_POST['enquiry'];
    $quotation = $_POST['quotation'];
    $bill = $_POST['sales_bill'];
   
    $sql="INSERT INTO sales(enquiry, quotation, sales_bill) VALUES('$enquiry','$quotation', '$bill')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: sales.php");
}

if (isset($_POST['update']))
{

    $enquiry =$_POST['enquiry'];
    $quotation = $_POST['quotation'];
    $bill = $_POST['sales_bill'];
    
    $id  = $item['id'];
    $updatequery = "update sales set enquiry = '$purchase_order', quotation='$quotation', sales_bill='$bill' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="sales.php"</script>';
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Sales</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <!-- <li class="dropdown"> -->
                          <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mani <span class="caret"></span></a> -->
                          <!-- <ul class="dropdown-menu"> -->
                            <!-- <li><a href="#">Action</a></li> -->
                            <!-- <li><a href="#">Another action</a></li> -->
                            <!-- <li><a href="#">Something else here</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="../index.php">Logout</a></li>
                          <!-- </ul> -->
                        <!-- </li> -->
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Sale</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Enquiry<span class="error">*</span></label>
                                <input type="text" class="form-control" name="enquiry" id="enquiry" maxlength="50" autocomplete="off" value="<?php echo $item['enquiry']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Quotation<span class="error">*</span></label>
                                <input type="text" class="form-control" name="quotation" id="quotation" autocomplete="off" value="<?php echo $item['quotation']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Sales Bill No<span class="error">*</span></label>
                                <input type="text" class="form-control" name="sales_bill" id="sales_bill" autocomplete="off" value="<?php echo $item['sales_bill']; ?>">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="sales.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{

            enquiry : "required",
            quotation:"required",
            sales_bill:"required",
            gaurdian : "required",
            department : "required",
            pf_code : "required",
            esi_code : "required",
            address : "required",
            email : "required",

            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{

            enquiry : "<span>Enter enquiry </span>",
            quotation:"<span>Enter quotation</span>",
            sales_bill:"<span>Enter Sales bill number</span>",
            gaurdian:"<span>Enter Father/Husband Name</span>",
            department:"<span>Select Department</span>",
            pf_code : "<span>enter PF code number</span>",
            esi_code : "<span>Enter ESI Code Number</span>",
            address : "<span>Enter Address</span>",
            email : "<span>Enter Email Id</span>",
           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>