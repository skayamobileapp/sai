<?php
include 'connection.php';
if (isset($_GET['id'])) {
    
    $id          = $_GET['id'];
    $sql         = "select * from sub_category where id = $id";
    $result      = $con->query($sql);
    $subcategory = $result->fetch_assoc();
    

} else {
    $id=0;
}
$sql              = "select * from category";
$result           = $con->query($sql);
$categorylist = array();
while ($row = $result->fetch_assoc()) {
    array_push($categorylist, $row);
}
if (isset($_POST['update'])) {
    
        $name       = strtoupper($_POST['name']);
        $id_category  = $_POST['id_category'];

        $sql    = "update sub_category set name = '$name',id_category = '$id_category' where id = $id";
        $con->query($sql);
        header("location:sub_category.php");
    }
    $gst = "";
if (isset($_POST['save']))
{
    $name       = strtoupper($_POST['name']);
    $id_category  = $_POST['id_category'];
    
    $sql = "select * from category where id = $id_category";
    $result = $con->query($sql);
    $row = $result->fetch_assoc();
    
    $sql      = "select * from sub_category Where id_category='$id_category'";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
    $ref= sprintf("%'.03d", $resnum);

    $subcode = $row['category_code']."-".$ref;

    $sql    = "insert into sub_category(name, sub_cat_code, id_category, status) values('$name', '$subcode', '$id_category',1);";
    $result = $con->query($sql);
    echo "<script>parent.location='sub_category.php'</script>";
}

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add Sub Category</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

    <link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
    #name{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
            
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if(!empty($subcategory['sub_category_id'])){echo "Edit";}else{echo "Add";}?> Sub Category</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">

                                <label>Category <span class="text-danger"> *</span></label>
                                <select name="id_category" class="form-control selitemIcon">
                                    <option value=""> --SELECT-- </option>
                                    <?php
                                        foreach ($categorylist as $category) {
                                        $category_name = $category['name']."-".$category['category_code'];
                                        $category_id   = $category['id'];
                                        if($subcategory['id_category'] == $category_id){

                                            $selected = "selected";
                                        }
                                        else{
                                            $selected = "";
                                        }
                                        echo "<option value='$category_id' $selected>$category_name</option>";
                                        }
                                        ?>
                                </select>
                            </div>
                              <div class="col-sm-4">

                                <label>Sub Category Name <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" name="name" id="name" value="<?php if (!empty($subcategory['name'])) {echo $subcategory['name'];}?>" onblur="checkduplicateName()">
                            </div>
                            
                            
                           
                        </div>
                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <a href="sub_category.php" class="btn btn-error">Cancel</a>
                    <button class="btn btn-success" type="submit" name="<?php if(!empty($subcategory['id'])){echo "update";}else{echo "save";}?>"><?php if(!empty($subcategory['id'])){echo "Update";}else{echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script>
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_category:"required",
                name : "required"
            },
            messages:{
                id_category:"<span>Select Category Name</span>",
                name:"<span>*Enter Sub category Name</span>"
            },
        })
    })
</script>
<script type="text/javascript">
        function checkduplicateName(){
          var name = $("#name").val();
          var id = "<?php echo $id;?>";
          console.log(id);

          $.ajax({url: "duplicate_subcategoryname.php?name="+name+"&id="+id, success: function(result){
            if(result==1) {
                alert("Sub Category Name already there");
                $("#name").val(' ');
            }
          }
        });
        }
        
</script>

    <script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
</body>

</html>
