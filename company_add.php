<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from company where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{

    $companyName = $_POST['company_name'];
    $address=$_POST['address'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];

   
    $sql="INSERT INTO company(company_name, full_address, phone, email) VALUES('$companyName','$address','$phone','$email')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: company.php");
}

if (isset($_POST['update']))
{

    $companyName = $_POST['company_name'];
    $address=$_POST['address'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    
    $id  = $item['id'];
    $updatequery = "update company set company_name = '$companyName', full_address = '$address', phone='$phone', email='$email' where id = $id";
    
    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="company.php"</script>';
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_customer_contact_details");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Company</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <!-- <li class="dropdown"> -->
                          <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Mani <span class="caret"></span></a> -->
                          <!-- <ul class="dropdown-menu"> -->
                            <!-- <li><a href="#">Action</a></li> -->
                            <!-- <li><a href="#">Another action</a></li> -->
                            <!-- <li><a href="#">Something else here</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="../index.php">Logout</a></li>
                          <!-- </ul> -->
                        <!-- </li> -->
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Company</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Company Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="company_name" id="company_name" maxlength="50" autocomplete="off" value="<?php echo $item['company_name']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Address<span class="error">*</span></label>
                                <textarea class="form-control" name="address" id="address" maxlength="250" autocomplete="off"><?php echo $item['full_address']; ?></textarea>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Phone Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="phone" id="phone" maxlength="50" autocomplete="off" value="<?php echo $item['phone']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Email Id<span class="error">*</span></label>
                                <input type="text" class="form-control" name="email" id="email" maxlength="50" autocomplete="off" value="<?php echo $item['email']; ?>">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="company.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>

  
  
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            company_name : "required",
            address:"required",
            supplier_code:"required",
            balance : "required",
            scope : "required",
            contact_person : "required",
            btype : "required",
            email : "required",

            phone: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{
            company_name : "<span>Enter Company name</span>",
            address:"<span>Enter Full Address</span>",
            supplier_code:"<span>Enter supplier code</span>",
            balance:"<span>Enter balance in rupees</span>",
            email:"<span>Enter Valid Email Id</span>",
            scope : "<span>enter scope of supply</span>",
            contact_person : "<span>enter name of contact person</span>",
            btype : "<span>Select business type</span>",
           phone:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>


</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>