<?php
include 'connection.php';
error_reporting(0);
if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from tax_invoice_services where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();

   $viewquery = " SELECT a.*, d.name as itemName, d.code FROM po_inwards_items as a INNER JOIN item as d ON a.id_item=d.id WHERE a.id_poin ='".$item['po_num_id']."' ";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['itemName'] = $row['itemName']."-".$row['code'];
      $career[$i]['quantity'] = $row['quantity'];
    $career[$i]['unit_price'] = $row['unit_price'];
    $career[$i]['supplied_charge'] = $row['supplied_charge'];
    $career[$i]['total'] = $row['total'];
    $career[$i]['supplied'] = $row['supplied'];
    $career[$i]['gst_rate'] = $row['gst_rate'];
    $career[$i]['pending'] = $row['pending'];
    $career[$i]['discount'] = $row['discount'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }

    $sql = "select * from tax_sales_invoice_notes where id_tax_sales = $id";

    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $reviewList[$i]['id'] = $row['id'];
      $reviewList[$i]['note'] = $row['note'];
      $reviewList[$i]['date'] = $row['date'];
      $i++;
    }
    
    $sql="SELECT sum(total) as totalAmount FROM po_inwards_items WHERE id_poin ='".$item['po_num_id']."' ";
      $result = mysqli_query($con,$sql);
    while ($row = mysqli_fetch_array($result)){
      $totalSum = $row['totalAmount'];
    }

    $sql="SELECT sum(supplied_charge) as totalCharge FROM po_inwards_items WHERE id_poin ='".$item['po_num_id']."' ";
      $result = mysqli_query($con,$sql);
    while ($row = mysqli_fetch_array($result)){
      $totalPaid = $row['totalCharge'];
    }
    $pending = $totalSum - $totalPaid;
    $payStatus = "PENDING"."($pending)";
    
    $updatequery = "update tax_invoice_services set total_amount = '$totalSum', payment_status='$payStatus', balance='$pending' where id = $id";

    $res=$con->query($updatequery);

    $updatesql = "update purchase_order_inwards set po_pending = '$pending' where id = '".$item['po_num_id']."'";

    $res=$con->query($updatesql);
}

$sql      = "select * from tax_invoice_services";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
      $currMonth = date("m");
      $currYear = date("y");
      $nxtYear = date("y", strtotime("+1 year"));
    $ref= sprintf("%'.04d", $resnum);
    $serialNo = $ref."/".$currYear."-".$nxtYear;

if (isset($_POST['save']))
{
    $invoiceNo = $_POST['invoice_no'];
    $customer_name =$_POST['id_customer'];
    $poNoId = $_POST['po_no'];
    $total = $_POST['total'];
    $gstRate = $_POST['rate'];
    $gstType = $_POST['gst_type'];
    $sgst = $_POST['sgst_rate'];
    $cgst = $_POST['cgst_rate'];
    $igst = $_POST['igst_rate'];
    $totalAmount = $_POST['totalinclude'];
    $payStatus = "PENDING"."($totalAmount)";
    $balance = $_POST['totalinclude'];
    $remarks = $_POST['remarks'];
    $file = $_FILES['attachment']['name'];
    $temp = $_FILES['attachment']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);
    $date = date("Y-m-d", strtotime($_POST['date']));
    $days = $_POST['days'];

   $sql="INSERT INTO tax_invoice_services(invoice_no, date, id_customer, po_num_id, total_amount, balance, gst_rate, gst_type, sgst_rate, cgst_rate, igst_rate, tax_include_amount, payment_status, remarks, days_no, attachment) VALUES('$invoiceNo', '$date', '$customer_name', '$poNoId ', '$total', '$balance', '$gstRate', '$gstType', '$sgst', '$cgst', '$igst', '$totalAmount', '$payStatus', '$remarks', '$days', '$file')";

    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: tax_invoice_services.php");
}

if (isset($_POST['add']))
{
    $cosigneeType = $_POST['cosigdetail'];
    $otherCharges = $_POST['other_charges'];
    $vehicleNo = $_POST['vehicle_no'];
    $lrNo = $_POST['lrno'];
    $lrDate = date("Y-m-d", strtotime($_POST['lr_date']));
    $esugamNo = $_POST['esugam'];
    
    if($cosigneeType=='1'){
        $sql="Update tax_invoice_services SET cosignee='same as above', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    }
    if($cosigneeType=='2'){
        $customer = $_POST['customer'];
        $sql="Update tax_invoice_services SET cosignee='$customer', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    }
    if($cosigneeType=='3'){
        $sql="Update tax_invoice_services SET cosignee='new cosignee', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    
    $cosigneeName = $_POST['cosignee'];
    $address =$_POST['address'];
    $gstNo = $_POST['gstno'];
    
    $sql="INSERT INTO tax_invoice_consignee_details(id_tax_sales, name, address, gst_no) VALUES('$id', '$cosigneeName', '$address', '$gstNo')";

    $con->query($sql) or die(mysqli_error($con));
    }
    echo "<script>parent.location='generate_tax_invoice.php?id=$id'</script>";
}

if (isset($_POST['edit']))
{
    $cosigneeType = $_POST['cosigdetail'];
    $otherCharges = $_POST['other_charges'];
    $vehicleNo = $_POST['vehicle_no'];
    $lrNo = $_POST['lrno'];
    $lrDate = date("Y-m-d", strtotime($_POST['lr_date']));
    $esugamNo = $_POST['esugam'];
    if($cosigneeType=='1'){
        $sql="Update tax_invoice_services SET cosignee='same as above', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo', lr_date='$lrDate' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    }
    if($cosigneeType=='2'){
        $customer = $_POST['customer'];
        $sql="Update tax_invoice_services SET cosignee='$customer', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo', lr_date='$lrDate' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    }
    if($cosigneeType=='3'){
        $sql="Update tax_invoice_services SET cosignee='new cosignee', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo', lr_date='$lrDate' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
        
    $cosigneeName = $_POST['cosignee'];
    $address =$_POST['address'];
    $gstNo = $_POST['gstno'];
    
    $sql="Update tax_invoice_consignee_details SET name='$cosigneeName', address='$address', gst_no='$gstNo' WHERE id_tax_sales='$id'";

    $con->query($sql) or die(mysqli_error($con));
    }
    echo "<script>parent.location='generate_tax_invoice.php?id=$id'</script>";
}

if (isset($_POST['update']))
{

    $invoiceNo = $_POST['invoice_no'];
    $customer_name =$_POST['id_customer'];
    $invValue = $_POST['invoice_value'];
    $remarks = $_POST['remarks'];
    $file = $_FILES['attachment']['name'];
    if($file == ""){
        $file = $item['attachment'];
    }
    else{
        $file = $_FILES['attachment']['name'];
        $temp = $_FILES['attachment']['tmp_name'];
        move_uploaded_file($temp, "uploads/".$file);
    }
    $date = date("Y-m-d", strtotime($_POST['date']));
    $days = $_POST['days'];
    
    $id  = $item['id'];
    $updatequery = "update tax_invoice_services set id_customer = '$customer_name', invoice_no='$invoiceNo', invoice_value='$invValue', attachment='$file', remarks='$remarks', date='$date', days_no='$days' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>parent.location="tax_invoice_services.php"</script>';
}

$sql = "SELECT id, name FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }
  
  $sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, customer_po_no FROM purchase_order_inwards";
$result = $con->query($sql);
$poInList = array();
while ($row = $result->fetch_assoc()) {
    array_push($poInList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Generate Tax Invoice Sales </title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>
<script type="text/javascript">
    
function Ondelete(cid)
    {
        var id = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_tax_sales_notes.php?cid='+cid+"&id="+id;
      }
    }
</script>
<body onload="myFunction()">
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Generate Tax Invoice Sale</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Invoice Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="invoice_no" id="invoice_no" autocomplete="off" value="<?php if($item['invoice_no']) { echo $item['invoice_no']; } else { echo $serialNo; } ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Date <span class="error">*</span></label>
                                <input type="text" class="form-control" name="date" id="date" autocomplete="off" value="<?php echo $item['date']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Customer Name<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="id_customer" id="id_customer" onchange="getpoNo()" disabled>
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($companyList); $i++){?>
                                    <option value="<?php echo $companyList[$i]['id']; ?>" <?php if($companyList[$i]['id']==$item['id_customer']){ echo "selected=selected"; } ?>><?php echo $companyList[$i]['customer_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PO NO<span class="error">*</span></label>
                                <select class="form-control selitemIcon" name="po_no" id="po_no" onchange="getPoItems()" disabled>
                                    <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['po_num_id'])){
                                    for ($i=0; $i<count($poInList); $i++) {  ?>
                                    <option value="<?php echo $poInList[$i]['id']; ?>" <?php if($poInList[$i]['id'] == trim($item['po_num_id']," ")) { echo "selected=selected"; }
                                    ?>><?php echo $poInList[$i]['customer_po_no']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No of Days<span class="error"></span></label>
                                <input type="text" class="form-control" name="days" id="days" autocomplete="off" value="<?php echo $item['days_no']; ?>" maxlength="3" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks<span class="error"></span></label>
                                <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remarks']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Attachment<span class="error"></span></label>
                                <input type="file" class="form-control" name="attachment" id="attachment" autocomplete="off" disabled>
                            </div>
                            <br>
                            <?php if(!empty($item['attachment'])){?>
                            <a href="uploads/<?php echo $item['attachment']; ?>" target="_blank">Click Here To View File</a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Other Charges<span class="error"></span></label>
                                <input type="text" class="form-control" name="other_charges" id="other_charges" autocomplete="off" value="<?php echo $item['other_charges']; ?>" >
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>E Sugam Number<span class="error"></span></label>
                                <input type="text" class="form-control" name="esugam" id="esugam" autocomplete="off" value="<?php echo $item['esugam']; ?>">
                            </div>
                        </div>
                    </div>
                    <div>
                    </div>
                </div>
                <br>
                <div style="<?php if($id==''){echo "display:none";}?>">
                            <table class="table table-striped" id="example">
                               <thead>
                            		<tr>
                                		<th>Item Name</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Discount</th>
                                        <th>Supplied</th>
                                        <th>Pending</th>
                                        <th>GST Rate</th>
                                        <th>Total</th>
                                        <th>Supplied Charges</th>
                                		<th>Action</th>
                            		</tr>
                            	</thead>
                            	<tbody>
                            		    <?php for($i=0; $i<count($career); $i++){
                            		        $cid = $career[$i]['id'];
                            		        $qid = $career[$i]['quantity'];
                            		        $sid = $career[$i]['supplied'];
                            		        $pid = $career[$i]['pending'];
                            		    ?>
                            		    <tr>
                            		        <td><?php echo $career[$i]['itemName']; ?></td>
                                            <td><?php echo $career[$i]['quantity']; ?></td>
                                            <td><?php echo $career[$i]['unit_price']; ?></td>
                                            <td><?php echo $career[$i]['discount']; ?></td>
                                            <td><?php echo $career[$i]['supplied']; ?></td>
                                            <td><?php echo $career[$i]['pending']; ?></td>
                                            <td><?php echo $career[$i]['gst_rate']; ?></td>
                                            <td><?php echo $career[$i]['total']; ?></td>
                                            <td><?php echo $career[$i]['supplied_charge']; ?></td>
                            		  <td><a href="#" name="modal1" data-toggle="modal" data-target="#myModal1" id="todolink" class="todolink" onclick="pass(<?php echo $cid; ?>); passQty(<?php echo $qid; ?>); passSup(<?php echo $sid; ?>); passPend(<?php echo $pid; ?>)"><i class="fa fa-shopping-cart fa-2x"></i></a> </td>
                            		    </tr>
                            		    <?php } ?>
                            	</tbody>
                        	</table>
                        </div>
                        <br>
                        
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="tax_invoice_services.php">Cancel</a></button>
                    <a href="tax_invoice_services_invoice.php?poid=<?php echo $item['po_num_id']; ?>&id=<?php echo $id ?>" class="btn btn-success" name="generate" target="_blank">Generate</a>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "edit";} else {echo "add";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>

                <div class="page-title clearfix col-sm-12">
                    <h3>Notes</h3>
                    <div class="pull-right">
                     <a href="javascript:Onsend(<?php echo $cid; ?>);" title="DELETE" class="btn btn-success">Send Mail</a>
                   </div>
                </div>
                <div class="page-container">
                <div class="row add">
                    <h4 class="col-sm-12">Add New Note</h4> <br>
                        <div class="col-sm-4">
                            <label>Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="note_date" id="note_date" value="">
                        </div>
                        <div class="col-sm-4">
                            <label>Note<span class="error">*</span></label>
                                <textarea  class="form-control" name="note" id="note" value=""></textarea>
                        </div>
                         <div class="col-sm-1">
                            <input type="button" name="Review" id="Review" class="btn btn-danger"  value="Add Note" onclick="saveReview()" style="margin-top: 30px;"/>
                        </div>
                   </div>
                   <div id="edit_note"></div>
                   
                   <hr/>
                       
                        <table class="table table-striped" id="example2">
                        <thead>
                            <tr>
                          <th>Date</th>
                          <th>Note</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($reviewList); $i++)
                          {
                              $cid = $reviewList[$i]['id'];
                          ?>
                        <tr>
                          <td><?php echo $reviewList[$i]['date']; ?></td>
                          <td><?php echo $reviewList[$i]['note']; ?></td>
                        <td><a href="javascript:Onedit(<?php echo $cid; ?>);"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $cid; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                       <div id="addfiles"></div>
                   </div>

                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
<input type="hidden" name="poItemId" id="poItemId">
<input type="hidden" name="itemQty" id="itemQty">
<input type="hidden" name="itemSup" id="itemSup">
<input type="hidden" name="itemPend" id="itemPend">
     <!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Required Quantity</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Quantity in Numbers<span class="error"> *</span></label>
                    <input type="text" class="form-control" name="quantity" id="quantity" maxlength="5" autocomplete="off" onkeyup="checkQty()">
                </div>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#lr_date" ).datepicker();
    $( "#note_date" ).datepicker();
  } );
  </script>
  <script type="text/javascript">
        function getpoNo(){
          var id = $("#id_customer").val();
          console.log(id);

          $.ajax({url: "get_po_number.php?id="+id, success: function(result){
            $("#po_no").html(result);
          }
        });
        }
        
        function getPoItems(){
            var poid = $("#po_no").val();
            $.ajax({url: "get_po_items.php?id="+poid, success: function(result){
            $("#po_items").html(result);
          }
        });
        }
        
        function getCosigneeFields(){
            var cosig = $("#cosigdetail").val();
            if(cosig == "1"){
                $(".existing_customer").hide();
                $(".cosignee_fields").hide();
            }
            if(cosig == "2"){
                $(".existing_customer").show();
                $(".cosignee_fields").hide();
            }
            if(cosig == "3"){
                $(".cosignee_fields").show();
                $(".existing_customer").hide();
            }
        }
        
        var cosig = $("#cosigdetail").val();
            if(cosig == "1"){
                $(".existing_customer").hide();
                $(".cosignee_fields").hide();
            }
            if(cosig == "2"){
                $(".existing_customer").show();
                $(".cosignee_fields").hide();
            }
            if(cosig == "3"){
                $(".cosignee_fields").show();
                $(".existing_customer").hide();
            }
        
    
    function getGST(){
        var org_cost= parseInt($('#total').val());
        var per= $("#rate").val();
        var pernum = (org_cost * 0.01 * (100 - per));
        var gstnum = (org_cost - pernum);
        var totnum = org_cost + gstnum;
        
        var tot = (((totnum - org_cost) * 100) / org_cost);
        var value = tot.toFixed(2)/2;
        $('#cgst_rate').val(value +' %');
        $('#sgst_rate').val(value +' %');
        $('#igst_rate').val(per +' %');
        $('#totalinclude').val(totnum.toFixed(2));
    }
    
    function getGstRate1(){
        var type = $("#gst_type1").val();
        if(type == 'local'){
            $(".local").show();
            $("#inter").hide();
        }
    }
    
    function getGstRate2(){
        var type = $("#gst_type2").val();
        if(type == 'inter-state')
        {
            $(".local").hide();
            $("#inter").show();
        }
    }
        
        function myFunction() {
            var poid = $("#po_no").val();
            $.ajax({url: "get_po_items.php?id="+poid, success: function(result){
            $("#po_items").html(result);
          }
        });
        
        var type = $("#gst_type2").val();
        if(type == 'inter-state')
        {
            $(".local").hide();
            $("#inter").show();
        }
        
        var type = $("#gst_type1").val();
        if(type == 'local'){
            $(".local").show();
            $("#inter").hide();
        }
        
        }

        function Onsend(){
            var id = '<?php echo $id ?>';
            alert("Email Sent Successfully");
        }
    
    
    </script>

  <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_customer:"required",
                customer:"customer",
                remarks:"required",
                date : "required",
                rate : "required",
                po_no : "required"
            },
            messages:{
                id_customer:"<span>Select customer Name</span>",
                customer:"<span>Select customer Name</span>",
                remarks:"<span>Enter Remarks</span>",
                date:"<span>select Date</span>",
                rate:"<span>Select GST Rate</span>",
                po_no:"<span>select PO NO</span>"
            },
        });
    });
</script>

<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">

    function pass(id){
        
        $("#poItemId").val(id);
    }
    
    function passQty(qid){
        
        $("#itemQty").val(qid);
    }
    
    function passSup(sid){
        
        $("#itemSup").val(sid);
    }
    
    function passPend(pid){
        
        $("#itemPend").val(pid);
    }
    
    function checkQty(){
        var id = $("#poItemId").val();
        var pend = parseInt($("#itemPend").val());
       var qty = parseInt($("#itemQty").val());
        var sup = parseInt($("#itemSup").val());
        var quantity = parseInt($("#quantity").val());
        var totqty = parseInt(quantity + sup);
        if(totqty > qty){
            alert("Item pending quantity is "+pend);
            return false;
        }
    }
    
    
    
</script>
<script type="text/javascript">    
    $("#add").on('click',function(){
    var tid = '<?php echo $id; ?>';
    var quantity = parseInt($("#quantity").val());
    if(quantity == ""){
        alert("Enter Required Quantity");
        return false;
    }
      var id = $("#poItemId").val();
      var pend = parseInt($("#itemPend").val());
       var qty = parseInt($("#itemQty").val());
        var sup = parseInt($("#itemSup").val());
        var totqty = parseInt(quantity + sup);
        if(totqty > qty){
            alert("Item pending quantity is "+pend);
            return false;
        }
    
      $.ajax({

        url: 'add_required_quantity.php',
        data:{

          'tid': tid,
          'id': id,
            'quantity': quantity,
        },
        success: function(result){
        $("#previous").html(result);
        location.reload();
      }
        });
      });
    </script>
    <script type="text/javascript">
    function Onedit(cid)
    {
      $.ajax({url: "get_tax_sales_note.php?id="+cid, success: function(result){
        $("#edit_note").html(result);
        $(".add").hide();
      }
    });
    }

        function saveReview() {
        
            var note = $("#note").val();
            if(note== ""){
                alert('Enter Note');
                exit;
            }
            var date = $("#note_date").val();
            if(date== ""){
                alert('Select Date');
                exit;
            }
            
            var tid = '<?php echo $id; ?>';         
            $.ajax({

        url: 'add_tax_sales_invoice_notes.php',
        method:'POST',
        data:{

          'note': note,
          'date': date,
           'tid': tid
        },
        success: function(result){
            location.reload();
      }
        });
            
        }

        function editReview(id) {
        
            var note = $("#editnote").val();
            if(note== ""){
                alert('Enter Note');
                exit;
            }
            
            var date = $("#editdate").val();
            if(date== ""){
                alert('Select Date');
                exit;
            }

         $.ajax({

        url: 'add_tax_sales_invoice_notes.php',
        method:'POST',
        data:{

          'note': note,
          'date': date,
           'id': id
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
  
      
        
    }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

<script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
    $('#example2').DataTable();
});
</script>
</html>