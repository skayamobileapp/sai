<?php
include 'connection.php';


if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from attendance where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

if (isset($_POST['save']))
{
    $employee = $_POST['employee'];
    $employeeStatus = $_POST['employee_status'];
    $date = $_POST['date'];
    $timeIn = $_POST['time_in'];
    $timeOut = $_POST['time_out'];
          
    $sql="INSERT INTO attendance(employee, employee_status, attendance_date, time_in, time_out) VALUES('$employee', '$employeeStatus','$date','$timeIn','$timeOut')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: attendance_list.php");
}

if (isset($_POST['update']))
{
    $employee = $_POST['employee'];
    $employeeStatus = $_POST['employee_status'];
    $date = date("Y-m-d", strtotime($_POST['date']));
    $timeIn = $_POST['time_in'];
    $timeOut = $_POST['time_out'];
    
    $id  = $item['id'];
    $updatequery = "update attendance set employee = '$employee', employee_status = '$employeeStatus', attendance_date='$date', time_in='$timeIn', time_out='$timeOut' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="attendance_list.php"</script>';
}

$sql = "SELECT id, company_name FROM company";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, employee_id, employee_name FROM employee";
$result = $con->query($sql);
$employeeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($employeeList, $row);
  }

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Attendance</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
            
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Attendance</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Employee <span class="error">*</span></label>
                                <select name="employee" class="form-control selitemIcon" required>
                                <option value=""> --SELECT-- </option>
                                <?php
                                for($i=0; $i<count($employeeList); $i++){
                                    ?>
                                    <option value="<?php echo $employeeList[$i]['id']; ?>" <?php if($employeeList[$i]['id']==$item['employee_id']){ echo "selected";} ?>><?php echo $employeeList[$i]['employee_name']; ?></option>
                                    <?php } ?>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Employee Attendance <span class="error">*</span></label>
                                <select name="employee_status" class="form-control selitemIcon">
                                <option value=""> --SELECT-- </option>
                                <option value="P"> PRESENT </option>
                                <option value="AB"> ABSENT </option>
                                <option value="L"> LEAVE </option>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="date" id="date" maxlength="20" autocomplete="off" value="<?php if(!empty($item['attendance_date'])){ echo $item['attendance_date']; } else { echo date("Y-m-d"); }; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Time In<span class="error">*</span></label>
                                <input type="text" class="form-control" name="time_in" id="time_in" maxlength="50" autocomplete="off" value="<?php echo $item['time_in']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Time Out<span class="error">*</span></label>
                                <input type="text" class="form-control" name="time_out" id="time_out" maxlength="50" autocomplete="off" value="<?php echo $item['time_out']; ?>">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="attendance_list.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            employee:"required",
            employee_status : "required",
            date : "required",
            time_in : "required",
            time_out : "required"
        },
        messages:{
            employee:"<span>Select Employee</span>",
            employee_status : "<span>Select Employee Attendance</span>",
            date : "<span>Select Date</span>",
            time_in : "<span>Enter Time In</span>",
            time_out : "<span>Enter Time Out</span>"
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
  } );
  </script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>