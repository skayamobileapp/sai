<?php

include 'connection.php';


    $query = "SELECT a.*, b.customer_name, b.address, b.mobile FROM purchase_order_inwards as a INNER JOIN customer as b ON a.id_customer=b.id";
    $queryresult = mysqli_query($con,$query);
  $career = [];
  $i=0;
  while ($row = mysqli_fetch_array($queryresult))
  {
    $career[$i]['po_date']= date("d-m-Y", strtotime($row['po_date']));
    $career[$i]['customer_po_no']= $row['customer_po_no'];
    $career[$i]['date']= date("d-m-Y", strtotime($row['date']));
    $career[$i]['customer_name']= $row['customer_name'];
    $career[$i]['po_value']= $row['po_value'];
    $career[$i]['po_pending']= $row['po_pending'];
    $career[$i]['proforma_no']= $row['proforma_no'];
    $career[$i]['oa_no']=$row['oa_no'];
    $career[$i]['remark']= $row['remark'];
    $career[$i]['status']= strtoupper($row['status']);
    $career[$i]['id']= $row['id'];
    $i++;
  }
?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Purchase Order</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?item_id='+id;
      }
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Purchase Order (Inwards)</h3>
                     <a href="add_purchase_order_inwards.php" class="btn btn-primary">+ Create Purchase Order(Inwards)</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                                <th>PO Received Date</th>
                          <th>Customer Name</th>
                            <th>PO NO</th>
                            <th>PO Date</th>
                          <th>PO Value</th>
                          <th>Generate PI No.</th>
                          <th>PO Pending</th>
                          <th>Status</th>
                          <th>OA NO</th>
                          <th>Remarks</th>
                          <th>Generate</th>
                           <!--<th>Actions</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                            <td><?php echo $career[$i]['po_date']; ?></td>
                          <td><?php echo $career[$i]['customer_name']; ?></td>
                          <td><?php echo $career[$i]['customer_po_no']; ?></td>
                          <td><?php echo $career[$i]['date']; ?></td>
                          <td><?php echo $career[$i]['po_value']; ?></td>
                          <td><?php echo $career[$i]['proforma_no']; ?></td>
                          <td><?php echo $career[$i]['po_pending']; ?></td>
                          <td><?php if($career[$i]['po_pending']==0){ echo "PAID";}else { echo "Pending"; } ?></td>
                          <td><?php echo $career[$i]['oa_no']; ?></td>
                          <td><?php echo $career[$i]['remark']; ?></td>
                          <td><a href="generate_po_inwards.php?id=<?php echo $career[$i]['id']; ?>" class="btn btn-primary">Generate</a></td>
                           <!--<td><a href="add_purchase_order_inwards.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a></td> -->
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>