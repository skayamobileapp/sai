<?php
include 'connection.php';
session_start();
$sid = session_id();
$eid = $_SESSION['employee']['id'];
$empId = $_SESSION['employee']['employee_id'];

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from quotation_revision where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
    $idquotation = $item['id_quotation'];
    
      $sql = "select * from quotation_revision_review where id_quotation_revision = $id";

    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $reviewList[$i]['date'] = $row['review_date'];
      $reviewList[$i]['reason'] = $row['review_reason'];
      $i++;
    }
    
    
}

$viewquery = "SELECT a.*, b.name as catName, b.category_code, c.name as subcatName, c.sub_cat_code, d.name as itemName, d.code FROM quotation_revision_has_items as a INNER JOIN category as b ON a.id_category=b.id INNER JOIN sub_category as c ON a.id_subcategory=c.id INNER JOIN item as d ON a.id_item=d.id WHERE a.id_quotation_revision ='$id' ";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['item'] = $row['itemName']."-".$row['code'];
      $career[$i]['category'] = $row['catName']."-".$row['category_code'];
      $career[$i]['subcat'] = $row['subcatName']."-".$row['sub_cat_code'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }

if (isset($_POST['update']))
{
    $id = $_GET['id'];
        echo "<script>parent.location='quotation_invoice.php?id=$id'</script>";
}


$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }

$sql = "SELECT id, enq_no FROM enquiry";
$result = $con->query($sql);
$enquiryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($enquiryList, $row);
  }
  
  $sql = "SELECT id, employee_name FROM employee";
$result = $con->query($sql);
$employeeList = array();
while ($row = $result->fetch_assoc()) {
    array_push($employeeList, $row);
  }
  
  $sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  mysqli_query($con, "DELETE FROM temp_quotation_has_items");

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Quotation</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#qtn_date" ).datepicker();
            $( "#review_date" ).datepicker();

  } );
  </script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Generate Revised Quotation</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Revised Quotation Number<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="qtn_no" id="qtn_no" maxlength="50" autocomplete="off" value="<?php echo $item['qtn_no']; ?>" readonly>
                            </div>
                        </div>
                        <div id="result"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Enquiry Number<span class="error"> *</span></label>
                                <select class="form-control selitemIcon" name="enquiry" id="enquiry" disabled>
                                    <option value="">select</option>
                                    <?php for($i=0; $i<count($enquiryList); $i++){?>
                                    <option value="<?php echo $enquiryList[$i]['id']; ?>" <?php if($enquiryList[$i]['id']==$item['enquiry_id']){ echo "selected=selected"; } ?>><?php echo $enquiryList[$i]['enq_no']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Fallup Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="qtn_date" id="qtn_date" autocomplete="off" value="<?php echo $item['qtn_date']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks<span class="error">*</span></label>
                                <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remarks']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!--<div class="col-sm-4">-->
                        <!--    <div class="form-group">-->
                        <!--        <label>Attachment<span class="error">*</span></label>-->
                        <!--        <input type="file" class="form-control" name="attachment" id="attachment" autocomplete="off" value="<?php echo $item['attachment']; ?>" readonly>-->
                        <!--    </div>-->
                        <!--</div>-->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Status <span class="error"> *</span></label>
                                <label class="radio-inline">
                                    <input type="radio" name="status" id="status" value="pending" <?php if($item['status']=='pending'){ echo "checked"; }?> disabled><span class="check-radio"></span> Pending
                                    </label>
                                    <label class="radio-inline">
                                    <input type="radio" name="status" id="status" value="approve"  <?php if($item['status']=='approve'){ echo "checked"; }?> disabled><span class="check-radio"></span> Approve
                                    </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal">Add Items</button>
                            </div>
                        </div>
                </div>
                <div id="previous"></div>
                <div>
                        <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                          <th>Category</th>
                    		<th>Sub Category</th>
                          <th>Item Name</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {?>
                        <tr>
                          <td><?php echo $career[$i]['category']; ?></td>
                          <td><?php echo $career[$i]['subcat']; ?></td>
                          <td><?php echo $career[$i]['item']; ?></td>
                          <td><a href="delete_quotation_item.php?id=<?php echo $career[$i]['id']; ?>&qid=<?php echo $id; ?>">DELETE</a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                    </div>
                    
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="quotation_revision.php?idquotation=<?php echo $idquotation;?>">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "ADD";} else {echo "Save";}?></button>
                   </div>
                </div>
                
                  <div class="row">
                        <div class="col-sm-4">
                             <label>Review Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="review_date" id="review_date" autocomplete="off" value="" >
                                
                        </div>
                        <div class="col-sm-6">
                            <label>Review Reason<span class="error">*</span></label>
                                <textarea  class="form-control" name="review_reason" id="review_reason" value=""></textarea>
                        </div>
                         <div class="col-sm-1">
                            <input type="button" name="Review" id="Review" class="btn btn-danger"  value="Save Review" onclick="saveReview()" style="    margin-top: 30px;"/>
                        </div>
                   </div>
                   
                   <hr/>
                   <div class="row">
                       
                        <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                          <th>Reason</th>
                    		<th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($reviewList); $i++)
                          {?>
                        <tr>
                          <td style='width:70%'><?php echo $reviewList[$i]['reason']; ?></td>
                          <td><?php echo $reviewList[$i]['date']; ?></td>
                        
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                       
                       
                   </div>
                   
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
        <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items To Store</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;" onchange="getPrice()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                    <div id="qtnfields"></div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
    <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{

            quotation : "required",
            customer_name:"required",
            customer_mobile:"required",
            customer_email : "required",
            qtn_model : "required",
            qtn_date : "required",
            esi_code : "required",
            address : "required",
            email : "required",

            mobile: 
            {
                required:true,
                number:true,
                minlength:10,
                maxlength:10
            }
        },
        messages:{

            quotation : "<span>Enter quotation Number</span>",
            customer_name:"<span>Enter Customer Name</span>",
            customer_mobile:"<span>Enter Customer Contact number</span>",
            customer_email:"<span>Enter Customer Email</span>",
            qtn_model:"<span>Enter quotation Model</span>",
            qtn_date : "<span>enter quotation date</span>",
            esi_code : "<span>Enter ESI Code Number</span>",
            address : "<span>Enter Address</span>",
            email : "<span>Enter Email Id</span>",
           mobile:
           {
            required:"<span>Enter Phone Number</span>",
            number:"<span>Enter Numbers Only</span>",
            minlength:"<span>Enter 10 Digit Number</span>",
            maxlength:"<span>Don't Enter More Than 10 Digit</span>"
        }
    }
    })
})
</script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">
        function getSubcategory(){
          var id = $("#id_category").val();
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#id_subcategory").html(result);
          }
        });
        }
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#id_item").html(result);
          }
        });
        }
        
        function getPrice(){
          var id = $("#id_item").val();
          console.log(id);

          $.ajax({url: "get_fields_for_quotation.php?id="+id, success: function(result){
            $("#qtnfields").html(result);
          }
        });
            
            
        }
        
        function getTotal() {
            var unit = $("#unit").val();
            var totqnty = $("#totqnty").val();
            var value = parseFloat(unit * totqnty);
            $("#qtn_value").val(value);
        }
        
        
          function saveReview() {
        
          var review_date = $("#review_date").val();
            var review = $("#review_reason").val();
            
            
         $.ajax({

        url: 'add_review_quotation_revision.php',
        method:'POST',
        data:{

          'review_date': review_date,
          'review': review,
           'id_quotation':'<?php echo $id; ?>'
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
  
      
        
    }
    
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $id; ?>';
    var id_category= $("#id_category").val();
    if(id_category == ""){
        alert('Select Category Name');
        return false;
    }
    var id_subcategory= $("#id_subcategory").val();
    if(id_subcategory == ""){
        alert('Select Sub Category Name');
        return false;
    }
    var item = $("#id_item").val();
    if(item == ""){
        alert('Select Item Name');
        return false;
    }
    var unit = $("#unit").val();
    if(unit == ""){
        alert('Enter Unit');
        return false;
    }
    var totqnty = $("#totqnty").val();
    if(totqnty == ""){
        alert('Enter Quantity');
        return false;
    }
    var qtn_value = $("#qtn_value").val();
    if(qtn_value == ""){
        alert('Enter Total Quotation Value');
        return false;
    }
    
      $.ajax({

        url: 'add_item_to_quotation_revision.php',
        data:{

          'sid': sid,
          'item': item,
           'id_category': id_category,
            'id_subcategory': id_subcategory,
            'unit': unit,
            'totqnty': totqnty,
            'qtn_value': qtn_value,
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
      
       function saveReview() {
        
          var review_date = $("#review_date").val();
            var review = $("#review_reason").val();
            
            
         $.ajax({

        url: 'add_review_quotation_revision.php',
        method:'POST',
        data:{

          'review_date': review_date,
          'review': review,
           'id_quotation':'<?php echo $id; ?>'
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
  
      
        
    }
</script>
<script type="text/javascript">
        
        var id = $("#enquiry").val();
          console.log(id);

          $.ajax({url: "get_enquiry_fields.php?id="+id, success: function(result){
            $("#result").html(result);
          }
        });
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>

</html>