<?php
include 'connection.php';

    $id       = $_GET['id'];
    $sql      = "select * from item_technical where id_item = $id";
$viewqueryresult = mysqli_query($con,$sql);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = strtoupper($row['name']);
  $i++;
}

 
if (isset($_POST['save'])) {
    
  
    $name     = $_POST['name'];

    $sql = "insert into item_technical(id_item, name, status) values('$id','$name',1)";
    $con->query($sql);
    
    echo "<script>alert(' added successfully')</script>";
    echo "<script>parent.location='item_technical.php?id=$id'</script>";
    // header("location: categories.php");
}



?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if(!empty($category['category_id'])){echo "Edit";}else{echo "Add";}?> Category</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
    input{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                      <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kiran <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container-fluid -->
                </nav>
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if(!empty($category['category_id'])){echo "Edit";}else{echo "Add";}?> Technical specification</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-4">

                                <label>Name <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="" >
                            </div>
                          
                            </div>
                            
                        </div>
                
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <a href="categories.php" class="btn btn-error">Cancel</a>
                    <button class="btn btn-success" type="submit" name="<?php if(!empty($category['id'])){echo "update";}else{echo "save";}?>"><?php if(!empty($category['id'])){echo "Update";}else{echo "Save";}?></button>
                   </div>
                </div>
                </form>
                
                 <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                          <th>Technical Specification Name</th>
                          <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $code = $career[$i]['category_code'];
                            $name = $career[$i]['name'];
                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                          <td><?php echo $name; ?></td>
                          <td> <a href="javascript:Ondelete(<?php echo $id; ?>);"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>


                        </tbody>
                    </table>
              
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script>
    
     <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                name:"required"
            },
            messages:{
                name:"<span>Enter Category Name</span>"
            },
        })
    })
</script>
<script type="text/javascript">
        function checkduplicateName(){
          var name = $("#name").val();
          var id = "<?php echo $id;?>";
          console.log(id);

          $.ajax({url: "duplicate_categoryname.php?name="+name+"&id="+id, success: function(result){
            if(result==1) {
                alert("Category Name already there");
                $("#name").val(' ');
            }
          }
        });
        }
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#id_item").html(result);
          }
        });
        }
</script>

</body>

</html>
