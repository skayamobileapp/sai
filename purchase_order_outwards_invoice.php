<?php 
function numberTowords($num)
{ 
$ones = array( 
1 => "one", 
2 => "two", 
3 => "three", 
4 => "four", 
5 => "five", 
6 => "six", 
7 => "seven", 
8 => "eight", 
9 => "nine", 
10 => "ten", 
11 => "eleven", 
12 => "twelve", 
13 => "thirteen", 
14 => "fourteen", 
15 => "fifteen", 
16 => "sixteen", 
17 => "seventeen", 
18 => "eighteen", 
19 => "nineteen" 
); 
$tens = array( 
1 => "ten",
2 => "twenty", 
3 => "thirty", 
4 => "forty", 
5 => "fifty", 
6 => "sixty", 
7 => "seventy", 
8 => "eighty", 
9 => "ninety" 
); 
$hundreds = array( 
"hundred", 
"thousand", 
"million", 
"billion", 
"trillion", 
"quadrillion" 
); //limit t quadrillion 
$num = number_format($num,2,".",","); 
$num_arr = explode(".",$num); 
$wholenum = $num_arr[0]; 
$decnum = $num_arr[1]; 
$whole_arr = array_reverse(explode(",",$wholenum)); 
krsort($whole_arr); 
$rettxt = ""; 
foreach($whole_arr as $key => $i){ 
if($i < 20){ 
$rettxt .= $ones[$i]; 
}elseif($i < 100){ 
$rettxt .= $tens[substr($i,0,1)]; 
$rettxt .= " ".$ones[substr($i,1,1)]; 
}else{ 
$rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
$rettxt .= " ".$tens[substr($i,1,1)]; 
$rettxt .= " ".$ones[substr($i,2,1)]; 
} 
if($key > 0){ 
$rettxt .= " ".$hundreds[$key]." "; 
} 
} 
if($decnum < 0){ 
$rettxt .= " and "; 
if($decnum < 20){ 
$rettxt .= $ones[$decnum]; 
}elseif($decnum < 100){ 
$rettxt .= $tens[substr($decnum,0,1)]; 
$rettxt .= " ".$ones[substr($decnum,1,1)]; 
} 
} 
return $rettxt; 
} 

?>
<?php
include('connection.php');

date_default_timezone_set("Asia/Kolkata");
error_reporting(0);
$date = date('d/m/Y');

$id = $_GET['id'];
$sql = "SELECT a.*, b.vendor_name, b.address, b.mobile, b.gst FROM purchase_order as a INNER JOIN vendor as b ON a.id_vendor=b.id where a.id='$id' ";
$result = $con->query($sql) or die($con->error);
while ($row = mysqli_fetch_array($result))
{
	$vendor_name = $row['vendor_name'];
	$address = wordwrap($row['address'], 20, " ");
  $phone = $row['mobile'];
	$gstin = $row['gst'];
	$poNo = $row['po_no'];
  $poDate = $row['po_date'];
	$email = $row['email'];
	$time = date("H:i:sa");
	$ino = rand();
}

$viewquery = "SELECT a.*, b.name, b.code, b.description, b.hsn_code, b.rate FROM po_outwards_items as a INNER JOIN item as b ON a.id_item=b.id where a.id_poout='$id'";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['name'] = $row['name'];
  $career[$i]['code'] = $row['code'];
  $career[$i]['description'] = $row['description'];
  $career[$i]['unit_price']= $row['unit_price'];
  $career[$i]['quantity']= $row['quantity'];
  $career[$i]['total']= $row['total'];
  $career[$i]['hsn_code']= $row['hsn_code'];
  $career[$i]['discount']= $row['discount'];
  if($career[$i]['discount'] ==""){
        $career[$i]['discount'] = "0";
      }
  $career[$i]['rate']= $row['rate'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

$sql= "SELECT b.terms_condtion FROM purchase_order AS a INNER JOIN terms_conditions as b ON a.id_terms_conditions=b.id WHERE a.id='$id'";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $termsCondtion = $row['terms_condtion'];
}

$sql="SELECT sum(total) as totalAmount FROM po_outwards_items WHERE id_poout ='$id' ";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $totalAmount = $row['totalAmount'];
}

$sql="SELECT other_charges FROM po_outwards_items WHERE id_poout ='$id' AND other_charges >='0' ORDER BY id DESC LIMIT 0,1";
  $result = mysqli_query($con,$sql);
while ($row = mysqli_fetch_array($result)){
  $otherCharges = $row['other_charges'];
  if ($otherCharges == "") {
    $otherCharges = "0.00";
  }
}

$totalPO = $totalAmount + $otherCharges;
  $amountInWords = strtoupper(numberTowords($totalPO));

$currentDate = date('d-m-Y');
        $fromDate = $from_date;
    
        $currentTime = date('h:i:s a');
        

        $file_data = $file_data ."
		
		<table width='100%' border='1' style='border-collapse:collapse;'>
          <tr>
            <td style='text-align: left;' colspan='9'>
            	<b style='font-size: 25px; color: blue;'>SAI ENTERPRISES</b> <br>
              An ISO 9001-2015 CERTIFIED COMPANY <br>
              #107, 1<sup>ST</sup> Floor, MEI Colony, 
              Laggere Main Road,
              Peenya Industrial Area(WA),
              Bengaluru- 560058. <br> <br>
              <b>GSTIN: 29AHRPA5654N1ZN</b>
            </td>
          </tr>
          <tr><td style='text-align: center; font-size:18px; background-color: #F4F4F4;' colspan='9'><b>PURCHASE ORDER</b></td></tr>
          <tr><td style='font-size: 15px;' colspan='9'><font style='text-align: left;'>PO No. : $poNo </font> , 
          <font style='text-align: right;'>&emsp;&emsp;&emsp;&emsp;&emsp; 
            PO Date : $poDate </font></td></tr>
          <tr><td style='text-align: left; font-size:15px; background-color: #F4F4F4;' colspan='9'><b>Details of Vendor: </b></td></tr>
          <tr>
            <td style='text-align: left; font-size:16px;' colspan='9'><br> Name &emsp; : &emsp; $vendor_name
            <br>
            Address &emsp; : &emsp; $address
            <br>
            GSTIN &emsp; : &emsp; $gstin
            </td>
          </tr>
          <tr>
          <th style='background-color: #D5D3D3'>Sl. No</th>
          <th style='background-color: #D5D3D3'>Item Description</th>
          <th style='background-color: #D5D3D3'>HSN</th>
          <th style='background-color: #D5D3D3'>QTY</th>
          <th style='background-color: #D5D3D3'>UOM</th>
          <th style='background-color: #D5D3D3'>Unit Price</th>
          <th style='background-color: #D5D3D3'>Disc%</th>
          <th style='background-color: #D5D3D3'>Taxable Amount</th>
          <th style='background-color: #D5D3D3'>GST Rate</th>
          </tr>";

          for ($i=0; $i<count($career); $i++){

          $itemname = $career[$i]['name'];
          $itemcode = $career[$i]['code'];
          $itemdesc = $career[$i]['description'];
          $itemprice = $career[$i]['unit_price'];
          $itemqnty = $career[$i]['quantity'];
          $itemtotal = $career[$i]['total'];
          $hsncode = $career[$i]['hsn_code'];
          $discount = $career[$i]['discount'];
          $rate = $career[$i]['rate'];
          $no = $i+1;
       
         $file_data .="
         <tr>
         <td>$no</td>
         <td><b>$itemname</b> $itemdesc </td>
         <td>$hsncode</td>
         <td>$itemqnty</td>
         <td>Nos</td>
         <td>$itemprice</td>
         <td>$discount%</td>
         <td>$itemtotal</td>
         <td>$rate%</td>
         </tr>";
          }

        $file_data .="<tr><td colspan='7' style='text-align: right'> &emsp; Total : </td><td colspan='2'> $totalAmount </td></tr>
         <tr><td colspan='9'> &emsp; </td></tr>
         <tr><td colspan='6'>  </td><td colspan='2'>  Other Charges</td><td style='text-align: center'>  $otherCharges </td></tr>
         <tr><td colspan='6'> <b>Total PO Amount in Words:</b> <br> $amountInWords  </td><td colspan='2'>Total PO Value</td><td> $totalPO </td></tr>
         <tr><td style='text-align: left; font-size:15px; background-color: #F4F4F4;' colspan='9'><b>Terms & Conditions </b></td></tr>
         <tr><td colspan='9'>
         $termsCondtion
         </td></tr>
         <tr><td style='text-align: right' colspan='9'><b>For SAI ENTERPRISES</b> <br> <br>  Authorised Signatory</td></tr>

      </table>";

$currentDate = date('d_M_Y_H_i_s');

include("library/mpdf60/mpdf.php");
$mpdf=new mPDF();
$mpdf->WriteHTML($file_data);
$filename = "PO"."_" .$currentDate.".pdf";
$mpdf->Output($filename, 'D');

echo "<script>parent.location='generate_po_outwards.php?id=$id'</script>";
exit;