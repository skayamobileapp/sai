<?php

include 'connection.php';
error_reporting(0);

$sql = "select sum(balance) as total from purchase_bill";
    $result = $con->query($sql);
    $purTotal = $result->fetch_assoc();

$viewquery = "Select a.*, b.vendor_name, b.address, b.mobile, c.po_no from purchase_bill a INNER JOIN vendor b ON a.id_vendor=b.id INNER JOIN purchase_order c ON a.po_no_id=c.id ORDER BY a.id DESC LIMIT 0,1";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{

  $career[$i]['vendor_name'] = $row['vendor_name']."<br>".$row['address']."<br>".$row['mobile'];
  $career[$i]['grn_no'] = $row['grn_no'];
  $career[$i]['grn_date'] = $row['grn_date'];
  $career[$i]['purchase_bill_no'] = $row['po_no'];
  $career[$i]['remarks'] = $row['remarks'];
  $career[$i]['total_value'] = $row['total_value'];
  $career[$i]['balance'] = $row['balance'];
  $career[$i]['payment_status'] = $row['payment_status'];
  $career[$i]['date'] = $row['date'];
  $career[$i]['id'] = $row['id'];
  $i++;
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Purchase</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_purchase_bill.php?pbill_id='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Purchase Bill <b style="color: blue;">(Pending Balance : <?php echo $purTotal['total']; ?>)</b></h3>
                     <a href="purchase_bill_add.php" class="btn btn-primary">+ Create Purchase Bill</a>
                     <a href="purchase_bills.php" class="btn btn-success" > View All</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>GRN NO</th>
                                <th>GRN DATE</th>
                                <th>VENDOR NAME</th>
                                <th>PO NO</th>
                                <th>DATE</th>
                                <th>INVOICE VALUE</th>
                                <th>PAYMENT STATUS</th>
                              <th>REMARKS</th>
                              <th>GENERATE</th>
                            <!-- <th>ACTIONS</th> -->
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $payStatus = $career[$i]['payment_status'];
                            ?>
                        <tr>
                          <td><?php echo $career[$i]['grn_no']; ?></td>
                          <td><?php echo $career[$i]['grn_date']; ?></td>
                          <td><?php echo $career[$i]['vendor_name']; ?></td>
                          <td><?php echo $career[$i]['purchase_bill_no']; ?></td>
                          <td><?php echo $career[$i]['date']; ?></td>
                          <td><?php echo $career[$i]['total_value']; ?></td>
                          <td><?php echo $career[$i]['payment_status']; ?></td>
                          <td><?php echo $career[$i]['remarks']; ?></td>
                          <td><a href="generate_po_bill.php?id=<?php echo $id; ?>" class="btn btn-primary">Generate</a></td>
                          <!-- <td><a href="purchase_bill_add.php?id=<?php echo $id; ?>" title="EDIT"><i class="fa fa-edit fa-2x"></i></a> <a href="javascript:Ondelete(<?php echo $id; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td> -->

                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       <script type="text/javascript">
       function checkRow(){
      var id = $("#balance").val();
      console.log(id);
      alert(id);

      $.ajax({url: "add_purchase_remark.php?id="+id, success: function(result){
        $("#invoice_no").html(result);
        // location.reload();
      }
    });
    }

    $(function () {
            $("#balance").click(function () {
                if ($("#balance").is(':checked')) {
                      var id = $("#balance").val();
                      var remark = "PAID";
                  $.ajax({url: "add_purchase_remark.php?id="+id+"&remark="+remark, success: function(result){
                    $("#invoice_no").html(result);
                    location.reload();
                  }
                });
                } else {
                  var id = $("#balance").val();
                      var remark = "PENDING";
                  $.ajax({url: "add_purchase_remark.php?id="+id+"&remark="+remark, success: function(result){
                    $("#invoice_no").html(result);
                    location.reload();
                  }
                });
                }
            });
        });
       </script>
       
</body>

</html>