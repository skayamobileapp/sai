<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from purchase_order_inwards where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();

    $query = "select po.*, v.customer_name, v.email from purchase_order_inwards AS po INNER JOIN customer AS v ON v.id=po.id_customer where po.id = $id";

    $result1 = $con->query($query);
    $poCustomer = $result1->fetch_assoc();

      $sql = "select * from po_clarifications where id_inwards = $id";

    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $reviewList[$i]['id'] = $row['id'];
      $reviewList[$i]['clarification'] = $row['clarification'];
      $reviewList[$i]['date'] = $row['date'];
      $reviewList[$i]['attachment'] = $row['attachment'];
      $i++;
    }
    
    $sql = "select * from oa_clarifications where id_inwards = $id";

    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $clarfList[$i]['id'] = $row['id'];
      $clarfList[$i]['clarification'] = $row['clarification'];
      $i++;
    }
    
    $sql = "select * from po_drawings where id_inwards = $id";

    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $drawList[$i]['id'] = $row['id'];
      $drawList[$i]['attachment'] = $row['attachment'];
      $i++;
    }
}

$viewquery = "SELECT a.*, d.name as itemName, d.code FROM po_inwards_items as a INNER JOIN item as d ON a.id_item=d.id WHERE a.id_poin ='$id' ";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['item'] = $row['itemName']."-".$row['code'];
      $career[$i]['quantity'] = $row['quantity'];
    $career[$i]['unit_price'] = $row['unit_price'];
    $career[$i]['total'] = $row['total'];
    $career[$i]['gst_rate'] = $row['gst_rate'];
    $career[$i]['supplied'] = $row['supplied'];
    $career[$i]['pending'] = $row['pending'];
    $career[$i]['supplied_charge'] = $row['supplied_charge'];
    $career[$i]['discount'] = $row['discount'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }
    
    $sql = "select sum(total) as tot from po_inwards_items where id_poin='$id'";
    $result = $con->query($sql);
    $taxTotal = $result->fetch_assoc();
    if($taxTotal['tot'] == ""){
        $taxTotal['tot'] = '0.00';
    }

    $sql = "select sum(balance) as bal from po_inwards_items where id_poin='$id'";
    $result = $con->query($sql);
    $taxBal = $result->fetch_assoc();
    if($taxBal['bal'] == ""){
        $taxBal['bal'] = '0.00';
    }
    $totalAmount = round($taxTotal['tot'], 2);
    $balAmount = round($taxBal['bal'], 2);
    
    $sql="Update purchase_order_inwards set po_value='$totalAmount', po_pending='$balAmount' where id='$id'";
    $con->query($sql);

if(isset($_POST['send'])){

    $mailType = $_POST['mail_type'];

      $poNo = $poCustomer['customer_po_no'];
      $oaNo = $poCustomer['oa_no'];
      $vendorEmail = $poCustomer['email'];
    $vendorName = strtoupper($poCustomer['customer_name']);
    $date = date("d/m/Y");

$to = $vendorEmail;
if ($mailType == "1") {
    $subject = "SAI ENTERPRISES- Purchase Order Received";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR Sir/Madam, </th></tr>
<tr>
<th style='text-align: center'>Ref PO NO. $poNo Dated $date</th>
</tr>
</table>
<br>
<table>
<tr>
<td style='text-align: left'>
Thanks for the valuable PO. Will send you our OA shortly.
<br>
</td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
Bhagyalakshmi <br>
Mob: 9513159513 <br>
Application Engineer

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

if ($mailType == "2") {
    
    $ino = rand(0000, 9999);
    $month = date("m");
    
    $oano= "$month-$ino";
    if($oaNo == "NA"){

    $updatequery = "UPDATE purchase_order_inwards SET oa_no='$oano' where id = $id";
    $res=$con->query($updatequery);
    }
    
    $subject = "SAI ENTERPRISES- Purchase Order Acceptance";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR Sir/Madam, </th></tr>
<tr>
<th style='text-align: center'>Ref PO NO. $poNo Dated $date</th>
</tr>
</table>
<br>
<table>
<tr>
<td style='text-align: left'>
With Ref to above PO. Pls find Material delivery & OA for the same. <br>
Please find here with attachment of OA (If any Drawing). <br>
Please Clarify points mentioned in OA (give drawing approval) to <br> manufacture & dispatch goods on time.
<br><br>
Thanking you and awaiting for your early response.
</td>
<td style='text-align: right'><a href='https://www.saienpl.com/billing_system/purchase_order_acceptance_invoice.php?id=$id' target='_blank'>Click Here To Download OA</a></td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
Bhagyalakshmi <br>
Mob: 9513159513 <br>
Application Engineer

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

if ($mailType == "3") {

$subject = "SAI ENTERPRISES- Proforma Invoice No: $poNo";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR MR. </th></tr>
<tr>
<th style='text-align: left'>Please find attached Proforma Invoice.</th>
<th style='text-align: right; border: 1'><a href='https://www.saienpl.com/billing_system/purchase_order_inwards_invoice.php?id=$id' target='_blank'>Click Here To Download Proforma Invoice</a> </th>
</tr>
</table>
<br>
<table>
<tr>
<th style='text-align: left'>
Kindly deposit the Agreed Advance/Payment for Proforma Invoice to below<br>
mention bank details.
<br><br>
A/C NAME: M/S. SAI ENTERPRISES <br>
A/C NO: 1146201007427 <br>
A/C TYPE: CURRENT A/C <br>
RTGS/IFSC CODE: CNRB 0001146 <br>
MICR CODE: 560015062 <br>
BANK: CANARA BANK <br>
BRANCH: VIJAYANAGAR <br>
CITY: BANGALORE-560040 <br>
STATE: KARANATAKA<br>
</th>
</tr>
</table>
<br><br><br>
REGARDS
<br>
Bhagyalakshmi <br>
Mob: 9513159513 <br>
Application Engineer

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

if ($mailType == "4") {
    $subject = "SAI ENTERPRISES- Ask For Dispatch Instruction";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR Sir/Madam, </th></tr>
<tr>
<th style='text-align: center'>Ref PO NO. $poNo Dated $date</th>
</tr>
</table>
<br>
<table>
<tr>
<td style='text-align: left'>
ABOVE PO MATERIAL IS READY FULLY/PARTIALLY PLS SEND THE DISPACTH CLEARENCE FOR THE SAME.
<br><br>
Thanking you and awaiting for your early response.
</td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
Bhagyalakshmi <br>
Mob: 9513159513 <br>
Application Engineer

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

if ($mailType == "5") {
    $subject = "SAI ENTERPRISES- Material Dispatch";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR Sir/Madam, </th></tr>
<tr>
<th style='text-align: center'>Ref PO NO. $poNo Dated $date</th>
</tr>
</table>
<br>
<table>
<tr>
<td style='text-align: left'>
Pls find the attached dispatch details for your reference. <br>
Pls send your material receipt for the same.
<br><br>
Dispatch Details download : DD0001/20-21 <br>
Transporter or Courier copy : <a href='#'>download</a>
</td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
Bhagyalakshmi <br>
Mob: 9513159513 <br>
Application Engineer

<br><br>
M/S. SAI ENTERPRISES <br>
An ISO-9001:2015 Company <br>
NO-107, 1ST FLOOR, MEI COLONY. <br>
LAGGERE MAIN ROAD <br>
PEENYA INDUSTRIAL AREA<br>
PH NO:080-28398596<br>
E-MAIL-saientp_ajit@gmail.com<br>
www.saienpl.com <br>
www.indiamart.com/saient/";
}

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <mktg@saienpl.com>' . "\r\n";
$headers .= 'Cc: <yogeshdims@gmail.com>' . "\r\n";

mail($to,$subject,$message,$headers);

echo "<script>alert('Email Sent Succesfully')</script>";
echo "<script>parent.location='generate_po_inwards.php?id=$id'</script>";
}

if (isset($_POST['update']))
{
    $id = $_GET['id'];

    $sql      = "select * from purchase_order_inwards";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
      $currMonth = date("m");
      $currYear = date("y");
      $nxtYear = date("y", strtotime("+1 year"));
    $ref= sprintf("%'.04d", $resnum);
    $serialNo = $ref."/".$currYear."-".$nxtYear;

    $pono = "PI".$serialNo;
      $poNo = $poCustomer['proforma_no'];
    
    if ($poNo == "NA") {
    $updatequery = "UPDATE purchase_order_inwards SET proforma_no='$pono' where id = $id";

    $res=$con->query($updatequery);
    }
        echo "<script>parent.location='purchase_order_inwards_invoice.php?id=$id' </script>";
}


$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
  $sql = "SELECT id, terms_condtion, terms_name FROM terms_conditions";
$result = $con->query($sql);
$termsList = array();
while ($row = $result->fetch_assoc()) {
    array_push($termsList, $row);
  }

$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$customerList = array();
while ($row = $result->fetch_assoc()) {
    array_push($customerList, $row);
  }
  
  $sql = "SELECT id, vendor_name FROM vendor";
$result = $con->query($sql);
$vendorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorList, $row);
  }
 

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Generate PO Inwards</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    
</style>
<script type="text/javascript">
    function Ondel(id)
    {
        var sid = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_item_from_inwards.php?id="+id+"&sid="+sid;
      }
    }
    
    function OndelOa(cid)
    {
        var id = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_oa_clarification.php?cid='+cid+"&id="+id;
      }
        
    }
    
    function Ondelete(cid)
    {
        var id = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_clarification.php?cid='+cid+"&id="+id;
      }
    }
    
    function OndelAttach(cid){
        var id = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_po_drawing.php?cid='+cid+"&id="+id;
      }
    }
    
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Generate Purchase Order (Inwards)</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PO Received Date<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="po_date" id="po_date" maxlength="50" autocomplete="off" value="<?php echo $item['po_date']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Customer Name<span class="error">*</span></label>
                            <select name="id_customer" class="form-control selitemIcon" id="id_customer" style="width: 300px;" disabled>
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i<count($customerList); $i++) {  ?>
                                <option value="<?php echo $customerList[$i]['id']; ?>" <?php if($customerList[$i]['id'] == $item['id_customer']) { echo "selected=selected"; }
                                    ?>><?php echo $customerList[$i]['customer_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Customer PO NO<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="po_no" id="po_no" maxlength="50" autocomplete="off" value="<?php echo $item['customer_po_no']; ?>" readonly>
                            </div>
                        </div>
                    </div>
                
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Date<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="date" id="date" maxlength="50" autocomplete="off" value="<?php echo $item['date']; ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>PO Value<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="po_value" id="po_value" maxlength="50" autocomplete="off" value="<?php echo $item['po_value']; ?>" readonly>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Remarks<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remark']; ?>" readonly>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <br>
                            <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="width: 260px;">Add Items</button>
                        </div>
                    </div>
                    <div class="col-sm-4">
                            <div class="form-group">
                                <label>Mail Type <span class="error"> *</span></label>
                                <select name="mail_type" id="mail_type" class="form-control selitemIcon" onchange="getFileField()">
                                    <option value="">SELECT</option>
                                    <option value="1">1. PURCHASE ORDER RECEIVED THANKS MAIL</option>
                                    <option value="2">2. PURCHASE ORDER ACCEPTANCE MAIL</option>
                                    <option value="3">3. PROFORMA INVOICE MAIL</option>
                                    <option value="4">4. ASK FOR DISPATCH INSTRUCTION</option>
                                </select>
                            </div>
                        </div>
                </div>
                <div id="previous"></div>
                
                <div style="<?php if($id==''){echo "display:none";}?>">
                                <table id="example" class="table table-striped">
                                   <thead>
                                    <tr>
                                        <th>Item Name</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Discount</th>
                                        <th>GST </th>
                                        <th>Total</th>
                                        <th>Supplied</th>
                                        <th>Pending</th>
                                        <th>Supplied Charges</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0; $i<count($career); $i++){?>
                                        <tr>
                                            <td><?php echo $career[$i]['item']; ?></td>
                                            <td style="text-align: center;"><?php echo $career[$i]['quantity']; ?></td>
                                            <td><?php echo $career[$i]['unit_price']; ?></td>
                                            <td><?php echo $career[$i]['discount']; ?>%</td>
                                            <td><?php echo $career[$i]['gst_rate']; ?>%</td>
                                            <td><?php echo $career[$i]['total']; ?></td>
                                            <td style="text-align: center;"><?php echo $career[$i]['supplied']; ?></td>
                                            <td style="text-align: center;"><?php echo $career[$i]['pending']; ?></td>
                                            <td style="text-align: center;"><?php echo $career[$i]['supplied_charge']; ?></td>
                                            <td><a href="javascript:Ondel(<?php echo $career[$i]['id']; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                </div>
                    
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="purchase_order_inwards.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Generate";} else {echo "Save";}?></button>
                    <button type="submit" name="send" id="send" class="btn btn-success" style="width: 180px;">Send Mail To Customer</button>
                   </div>
                </div>

                <div class="page-container col-sm-6">
                    <div class="page-title clearfix">
                    <h3>OA Clarifications  </h3>
                </div>
                <div class="row addoa">
                        <div class="col-sm-8">
                            <label>Add New Clarification<span class="error">*</span></label>
                                <textarea  class="form-control" name="oaclarification" id="oaclarification" value=""></textarea>
                        </div>
                         <div class="col-sm-1">
                            <input type="button" name="addclarification" id="addclarification" class="btn btn-danger"  value="Add Clarification" onclick="saveOA()" style="margin-top: 30px;"/>
                        </div>
                   </div>
                   
                   <div id="edit_oa_clarification"></div>
                   
                   <hr/>
                       
                        <table class="table table-striped" id="example1">
                        <thead>
                            <tr>
                          <th>Clarification</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($clarfList); $i++)
                          {
                              $cid = $clarfList[$i]['id'];
                          ?>
                        <tr>
                          <td><?php echo $clarfList[$i]['clarification']; ?></td>
                        <td><a href="javascript:OneditOa(<?php echo $cid; ?>);"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:OndelOa(<?php echo $cid; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                    </div>
                    
                    <div class="page-container col-sm-6">
                <div class="page-title clearfix">
                    <h3>Drawing Attachments </h3>
                </div>
                <div class="row add">
                        <div class="col-sm-8">
                            <label>Add New Attachment<span class="error">*</span></label>
                                <input type="file" class="form-control" name="drwAttachment" id="drwAttachment" >
                        </div>
                         <div class="col-sm-1">
                            <input type="button" name="addclarification" id="addclarification" class="btn btn-danger"  value="Add Attachment" onclick="saveAttach()" style="margin-top: 30px;"/>
                        </div>
                   </div>
                   <div id="viewfiles"></div>
                   <hr/>
                        <table class="table table-striped" id="example3">
                        <thead>
                            <tr>
                          <th>Attachments</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($drawList); $i++)
                          {
                              $cid = $drawList[$i]['id'];
                          ?>
                        <tr>
                          <td><a href="uploads/<?php echo $drawList[$i]['attachment']; ?>" target="_blank"><?php echo $drawList[$i]['attachment']; ?></a></td>
                        <td> <a href="javascript:OndelAttach(<?php echo $cid; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                    </div>
                    
                <div class="page-title clearfix col-sm-12">
                    <h3>PO Clarifications</h3>
                </div>
                <div class="page-container">
                <div class="row add">
                    <div class="col-sm-3">
                        <label>Date<span class="error">*</span></label>
                            <input type="text" class="form-control" name="clarification_date" id="clarification_date" >
                    </div>
                        <div class="col-sm-4">
                            <label>Add New Clarification<span class="error">*</span></label>
                                <textarea  class="form-control" name="clarification" id="clarification" value=""></textarea>
                        </div>
                        <div class="col-sm-3">
                            <label>Attachment<span class="error"></span></label>
                                <input type="file" class="form-control" name="file" id="file">
                        </div>
                         <div class="col-sm-1">
                            <input type="button" name="Review" id="Review" class="btn btn-danger"  value="Add Clarification" onclick="saveReview()" style="margin-top: 30px;"/>
                        </div>
                   </div>
                   <div id="edit_clarification"></div>
                   
                   <hr/>
                       
                        <table class="table table-striped" id="example2">
                        <thead>
                            <tr>
                            <th>Date</th>
                          <th>Clarification</th>
                          <th>Attached File</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($reviewList); $i++)
                          {
                              $cid = $reviewList[$i]['id'];
                          ?>
                        <tr>
                            <td><?php echo $reviewList[$i]['date']; ?></td>
                          <td><?php echo $reviewList[$i]['clarification']; ?></td>
                          <td><a href="uploads/<?php echo $reviewList[$i]['attachment']; ?>" target="_blank"><?php if(!empty($reviewList[$i]['attachment'])){ echo "click to view"; } ?></a></td>
                        <td><a href="javascript:Onedit(<?php echo $cid; ?>);"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $cid; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                       <div id="addfiles"></div>
                   </div>
                       
                   </div>
                   
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
  <input type="hidden" name="poinId" id="poinId">  

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;" onchange="getPrice()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                    <div id="qtnfields"></div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>

  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Item Details</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> Supplied<span class="error"> *</span></label>
                        <input type="text" name="supplied" id="supplied" class="form-control">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> Pending<span class="error"> *</span></label>
                        <input type="text" name="pending" id="pending" class="form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label> Remarks<span class="error"> *</span></label>
                        <input type="text" name="remark" id="remark" class="form-control">
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="addDetail" id="addDetail" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
    $("#po_date").datepicker();
    $("#clarification_date").datepicker();
  });
  </script>
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            date : "required",
            mail_type: "required",
            id_customer_vendor: "required",
            id_category:"required",
            id_subcategory:"required",
            id_item:"required",
            qnty:"required",
            remarks:"required",
            material_status:"required"
        },
        messages:{
            
            date :"<span>Select DC Date</span>",
            mail_type:"<span>Select Mail Type</span>",
            id_customer_vendor:"<span>Select Value</span>",
            id_category:"<span>Select Category</span>",
            id_subcategory:"<span>Select Sub Category</span>",
            id_item:"<span>Select Item</span>",
            qnty:"<span>Enter Quantity</span>",
            remarks:"<span>Enter Remarks</span>",
            material_status:"<span>Select Material Status</span>"
    }
    });
});
</script>
<script type="text/javascript">

    function getFileField(){
        
    }

    function Onedit(cid)
    {
      $.ajax({url: "get_clarification.php?id="+cid, success: function(result){
        $("#edit_clarification").html(result);
        $(".addoa").hide();
      }
    });
    }
    
    function OneditOa(cid){
        $.ajax({url: "get_oa_clarification.php?id="+cid, success: function(result){
        $("#edit_oa_clarification").html(result);
        $(".add").hide();
      }
    });
    }
    
    function getSubcategory(){
      var id = $("#id_category").val();
      console.log(id);

      $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
        $("#id_subcategory").html(result);
      }
    });
    }
    
    function getItem(){
      var id = $("#id_subcategory").val();
      var poid = '<?php echo $id; ?>';
      console.log(id);

      $.ajax({url: "get_items_for_po_inwards.php?id="+id+"&poid="+poid, success: function(result){
        $("#id_item").html(result);
      }
    });
    }

    function getPrice(){
          var id = $("#id_item").val();
          console.log(id);

          $.ajax({url: "get_fields_for_purchase.php?id="+id, success: function(result){
            $("#qtnfields").html(result);
          }
        });

      }

    function getCalculate(){
        var unit= parseInt($('#unit').val());
            var totqnty = $("#totqnty").val();
            var purchase_cost = parseInt(unit * totqnty);
            // alert(purchase_cost);
            // console.log(purchase_cost);

        var per= $("#discount").val();
        var gst= $("#gst").val();
        
        var pernum = (purchase_cost * 0.01 * (100 - per));
        var discountnum = (purchase_cost - pernum);
        var totnum = purchase_cost - discountnum;
        
        var gstpernum = (purchase_cost * 0.01 * (100 - gst));
        var gstnum = (purchase_cost - gstpernum);
        var totamt = totnum + gstnum;

        $('#total').val(totamt.toFixed(2));
    }
</script>
<script type="text/javascript">
    function pass(id){
        var strid = $(id).attr('id');
        console.log(strid);
        $("#poinId").val(strid);
    }

    $("#addDetail").on('click',function(){
    var poinId = $("#poinId").val();
    var supplied = $("#supplied").val();
    if(supplied == ""){
        alert('Enter Supplied Quantity');
        return false;
    }
    var pending = $("#pending").val();
    if(pending == ""){
        alert('Enter Pending Quantity');
        return false;
    }

    var remark = $("#remark").val();
    if(remark == ""){
        alert('Enter Remark');
        return false;
    }
    
      $.ajax({

        url: 'add_items_to_po_inwards.php',
        data:{

          'poinId': poinId,
            'remark': remark,
            'supplied': supplied,
            'pending': pending,
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
</script>
<script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $id; ?>';
    var id_category= $("#id_category").val();
    if(id_category == ""){
        alert('Select Category Name');
        return false;
    }
    var id_subcategory= $("#id_subcategory").val();
    if(id_subcategory == ""){
        alert('Select Sub Category Name');
        return false;
    }
    var item = $("#id_item").val();
    if(item == ""){
        alert('Select Item Name');
        return false;
    }
    var unit = $("#unit").val();
    if(unit == ""){
        alert('Enter Unit Price');
        return false;
    }
    var totqnty = $("#totqnty").val();
    if(totqnty == ""){
        alert('Enter Quantity');
        return false;
    }
    var total = $("#total").val();
    if(total == ""){
        alert('Enter Total');
        return false;
    }
    var discount = $("#discount").val();
    if(discount == ""){
        alert('Enter Discount');
        return false;
    }
    
    var gst = $("#gst").val();
    if(gst == ""){
        alert('Enter GST Rate');
        return false;
    }
    
      $.ajax({

        url: 'add_items_to_po_inwards.php',
        data:{

          'sid': sid,
          'item': item,
           'id_category': id_category,
            'id_subcategory': id_subcategory,
            'unit': unit,
            'totqnty': totqnty,
            'discount': discount,
            'gst': gst,
            'total': total,
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
</script>
<script type="text/javascript">
    function selectField(){
        var value = $("#customer_vendor").val();
        if(value== "customer"){
            $(".customer").show();
            $(".vendor").hide();
        }
        if (value == "vendor") {
            $(".vendor").show();
            $(".customer").hide();
        }
    }
    var value = $("#customer_vendor").val();
        if(value== "customer"){
            $(".customer").show();
            $(".vendor").hide();
        }
        if (value == "vendor") {
            $(".vendor").show();
            $(".customer").hide();
        }
        
        function saveOA(){
        
            var clarf = $("#oaclarification").val();
            if(clarf== ""){
                alert('Enter Clarification');
                exit();
            }
            var id_inwards='<?php echo $id?>';
            
            $.ajax({

        url: 'add_clarifications_oa.php',
        data:{

          'clarf': clarf,
           'id_inwards': id_inwards
        },
        success: function(result){
            location.reload();
      }
        });
        }
        
        function editOaReview(id){
        
           var clarf = $("#editoaclarification").val();
            if(clarf== ""){
                alert('Enter Clarification');
                exit;
            }
            
         $.ajax({

        url: 'add_clarifications_oa.php',
        method:'POST',
        data:{

          'clarf': clarf,
           'id': id
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
        }
        
        function saveAttach() {
            
            var id_inwards='<?php echo $id?>';
            var file_data = $('#drwAttachment').prop('files')[0];

            if(file_data== undefined){
                alert('Select Attachment');
                exit();
            }
            
            var form_data = new FormData();
              form_data.append('file', file_data);
              form_data.append('id_inwards', id_inwards);
              
            $.ajax({
                  url: 'add_drawing_attachment.php', 
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function (response) {
                      $('#viewfiles').html(response); 
                      location.reload();
                  },
              });
            
        }
        
        function saveReview() {
        
            var clarf = $("#clarification").val();
            if(clarf== ""){
                alert('Enter Clarification');
                exit;
            }
            
            var clarfDate = $("#clarification_date").val();
            if(clarfDate== ""){
                alert('Select Clarification Date');
                exit;
            }
            var id_inwards='<?php echo $id?>';
            var file_data = $('#file').prop('files')[0];
            
            var form_data = new FormData();
              form_data.append('file', file_data);
              form_data.append('clarf', clarf);
              form_data.append('clarfDate', clarfDate);
              form_data.append('id_inwards', id_inwards);
              
            $.ajax({
                  url: 'add_clarifications_po.php', 
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function (response) {
                      $('#addfiles').html(response); 
                      location.reload();
                  },
              });
            
        }
    
    function editReview(id) {
        
            var clarf = $("#editclarification").val();
            if(clarf== ""){
                alert('Enter Clarification');
                exit;
            }
            
         $.ajax({

        url: 'add_clarifications_po.php',
        method:'POST',
        data:{

          'clarf': clarf,
           'id': id
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
  
      
        
    }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
<script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
    $('#example1').DataTable();
    $('#example2').DataTable();
    $('#example3').DataTable();
});
</script>
</html>