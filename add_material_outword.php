<?php
include 'connection.php';
session_start();
$sid = session_id();

if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from material_outword where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
}

$viewquery = "SELECT a.*, d.name as itemName, d.code FROM material_outword_items as a INNER JOIN item as d ON a.id_item=d.id WHERE a.id_outword ='$id' ";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['item'] = $row['itemName']."-".$row['code'];
      $career[$i]['qnty'] = $row['qnty'];
      $career[$i]['doc_no'] = $row['doc_no'];
      $career[$i]['item_date'] = $row['item_date'];
      $career[$i]['photo'] = $row['photo'];
      $career[$i]['remarks'] = $row['remarks'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }

if (isset($_POST['save']))
{
	$date = date("Y-m-d", strtotime($_POST['date']));
	$cus_ven = $_POST['customer_vendor'];
	$custvenId = $_POST['id_customer'];
    if ($custvenId == "") {
        $custvenId = $_POST['id_vendor'];
    }
	// $material = $_POST['material_status'];
	
	$sql      = "select * from material_outword";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
      $curdate = date("d-m"); 
      $currYear = date("y");
    $nxtYear = date("y", strtotime("+1 year"));
    $ref= sprintf("%'.05d", $resnum);
    $dcNo = "OUTWORD".$ref."/".$currYear."-".$nxtYear;

   
    $sql="INSERT INTO material_outword(outword_no, outword_date, customer_vendor, id_customer_vendor) VALUES('$dcNo', '$date', '$cus_ven', '$custvenId')";
    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

$tempsql = mysqli_query($con,"SELECT * FROM temp_material_inword_items WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['id_item'] = $row['id_item'];
    $fetch[$i]['qnty'] = $row['qnty'];
    $fetch[$i]['doc_no'] = $row['doc_no'];
    $fetch[$i]['item_date'] = $row['item_date'];
    $fetch[$i]['remarks'] = $row['remarks'];
    $fetch[$i]['photo'] = $row['photo'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $item= $fetch[$i]['id_item'];
    $qnty= $fetch[$i]['qnty'];
    $docNo = $fetch[$i]['doc_no'];
    $itemDate = $fetch[$i]['item_date'];
    $remarks= $fetch[$i]['remarks'];
    $photo= $fetch[$i]['photo'];

    $insertbill = "INSERT INTO material_outword_items(id_outword, id_item, qnty, doc_no, item_date, remarks, photo) VALUES ('$last_id', '$item', '$qnty', '$docNo', '$itemDate', '$remarks', '$photo')";
   
    $result = mysqli_query($con,$insertbill);
}


    header("location: material_outwords.php");
}

if (isset($_POST['update']))
{
    $date = date("Y-m-d", strtotime($_POST['date']));
    $cus_ven = $_POST['customer_vendor'];
    $custvenId = $_POST['id_customer'];
    if ($custvenId == "") {
        $custvenId = $_POST['id_vendor'];
    }
    
    $id  = $item['id'];

    $tempsql = mysqli_query($con,"SELECT * FROM temp_material_inword_items WHERE id_session='$sid' ");
  $i=0;
  while ($row = mysqli_fetch_assoc($tempsql)) {
    $fetch[$i]['id_item'] = $row['id_item'];
    $fetch[$i]['qnty'] = $row['qnty'];
    $fetch[$i]['doc_no'] = $row['doc_no'];
    $fetch[$i]['item_date'] = $row['item_date'];
    $fetch[$i]['remarks'] = $row['remarks'];
    $fetch[$i]['photo'] = $row['photo'];
    $i++;
  }
  for($i=0; $i<count($fetch); $i++){
    $item= $fetch[$i]['id_item'];
    $qnty= $fetch[$i]['qnty'];
    $docNo = $fetch[$i]['doc_no'];
    $itemDate = $fetch[$i]['item_date'];
    $remarks= $fetch[$i]['remarks'];
    $photo= $fetch[$i]['photo'];

    $insertbill = "INSERT INTO material_outword_items(id_outword, id_item, qnty, doc_no, item_date, remarks, photo) VALUES ('$last_id', '$item', '$qnty', '$docNo', '$itemDate', '$remarks', '$photo')";
   
    $result = mysqli_query($con,$insertbill);
}

    $updatequery = "UPDATE material_outword SET outword_date='$date', customer_vendor='$cus_ven', id_customer_vendor='$custvenId' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>alert("Updated successfully")</script>';
        echo '<script>parent.location="material_outwords.php"</script>';
}

  mysqli_query($con, "DELETE FROM temp_material_inword_items");


$sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, name, sub_cat_code FROM sub_category";
$result = $con->query($sql);
$subcategoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($subcategoryList, $row);
  }
  
$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$customerList = array();
while ($row = $result->fetch_assoc()) {
    array_push($customerList, $row);
  }
  $sql = "SELECT id, vendor_name FROM vendor";
$result = $con->query($sql);
$vendorList = array();
while ($row = $result->fetch_assoc()) {
    array_push($vendorList, $row);
  }
 

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Material Outword</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="library/ckeditor/ckeditor.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
    
</style>
<script type="text/javascript">
    function Ondelete(id)
    {
        var sid = '<?php echo $id ?>';
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location="delete_material_outword_item.php?id="+id+"&sid="+sid;
      }
    }
</script>
<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if (!empty($item['id'])) {echo "Edit";} else {echo "Add";}?> Material Outword</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Outword Date<span class="error"> *</span></label>
                                <input type="text" class="form-control" name="date" id="date" maxlength="50" autocomplete="off" value="<?php echo $item['outword_date']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                            <label>Customer/Vendor<span class="error">*</span></label>
                            <select name="customer_vendor" class="form-control selitemIcon" id="customer_vendor" style="width: 300px;" onchange="selectField()">
                                <option value="">SELECT</option>
                                <option value="customer" <?php if($item['customer_vendor']=="customer"){ echo "selected=seleted"; }?>>CUSTOMER</option>
                                <option value="vendor" <?php if($item['customer_vendor']=="vendor"){ echo "selected=seleted"; }?>>VENDOR</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4 customer" style="display: none;">
                            <div class="form-group">
                            <label>Customer Name<span class="error">*</span></label>
                            <select name="id_customer" class="form-control selitemIcon" id="id_customer" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i<count($customerList); $i++) {  ?>
                                <option value="<?php echo $customerList[$i]['id']; ?>" <?php if($customerList[$i]['id'] == $item['id_customer_vendor']) { echo "selected=selected"; }
                                    ?>><?php echo $customerList[$i]['customer_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-4 vendor" style="display: none;">
                            <div class="form-group">
                            <label>Vendor Name<span class="error">*</span></label>
                            <select name="id_vendor" class="form-control selitemIcon" id="id_vendor" style="width: 300px;">
                                <option value="">SELECT</option>
                                    <?php
                                    for ($i=0; $i<count($vendorList); $i++) {  ?>
                                <option value="<?php echo $vendorList[$i]['id']; ?>" <?php if($vendorList[$i]['id'] == $item['id_customer_vendor'])  { echo "selected=selected"; }
                                    ?>><?php echo $vendorList[$i]['vendor_name']; ?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <br>
                            <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="width: 260px;">Add Items</button>
                        </div>
                    </div>
                    <!-- <div class="col-sm-4">
                        <div class="form-group">
                            <p><label> Material <span class='error'> *</span></label></p>
                            <label class="radio-inline">
                            <input type="radio" name="material_status" id="material1" value="recieved" <?php if($item['material_status']=='recieved'){ echo "checked"; }?>><span class="check-radio"></span> Recieved
                            </label>
                            <label class="radio-inline">
                            <input type="radio" name="material_status" id="material2" value="not-recieved" <?php if($item['material_status']=='not-recieved'){ echo "checked"; }?>><span class="check-radio"></span> Not Recieved
                            </label>
                        </div>
                    </div> -->
                </div>
                <div id="previous"></div>
                <div style="<?php if($id==''){echo "display:none";}?>">
                                <table id="example" class="table table-striped">
                                   <thead>
                                    <tr>
                                        <th>ITEM NAME</th>
                                        <th>QUANTITY</th>
                                        <th>DOC NO</th>
                                        <th>DATE</th>
                                        <th>REMARKS</th>
                                        <th>MATERIAL PHOTO</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0; $i<count($career); $i++){?>
                                        <tr>
                                            <td><?php echo $career[$i]['item']; ?></td>
                                            <td><?php echo $career[$i]['qnty']; ?></td>
                                            <td><?php echo $career[$i]['doc_no']; ?></td>
                                            <td><?php echo $career[$i]['item_date']; ?></td>
                                            <td><?php echo $career[$i]['remarks']; ?></td>
                                            <td><a href="uploads/<?php echo $career[$i]['photo']; ?>" target='_blank'> <?php echo $career[$i]['photo']; ?> </a></td>
                                            <td style="text-align: center;"><a href="javascript:Ondelete(<?php echo $career[$i]['id']; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>

                </div>
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="material_outwords.php">Cancel</a></button>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($item['id'])) {echo "update";} else {echo "save";}?>"><?php if (!empty($item['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>"><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Quantity<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="qnty" id="qnty" maxlength="50" autocomplete="off" >
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Doc No<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="doc_no" id="doc_no" autocomplete="off" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Remarks<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" >
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Date<span class="error"> *</span></label>
                            <input type="text" class="form-control" name="item_date" id="item_date" autocomplete="off" >
                            </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label> Material Photo <span class="error"> *</span></label>
                            <input type="file" class="form-control" name="photo" id="photo" autocomplete="off" >
                        </div>
                    </div>
                </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#date" ).datepicker();
    $( "#item_date" ).datepicker();
  } );
  </script>
<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function(){
    $("#form").validate({
        rules:{
            date : "required",
            customer_vendor: "required",
            id_customer_vendor: "required",
            id_category:"required",
            id_subcategory:"required",
            id_item:"required",
            qnty:"required",
            remarks:"required",
            material_status:"required"
        },
        messages:{
            
            date :"<span>Select DC Date</span>",
            customer_vendor:"<span>Select Customer or Vendor</span>",
            id_customer_vendor:"<span>Select Value</span>",
            id_category:"<span>Select Category</span>",
            id_subcategory:"<span>Select Sub Category</span>",
            id_item:"<span>Select Item</span>",
            qnty:"<span>Enter Quantity</span>",
            remarks:"<span>Enter Remarks</span>",
            material_status:"<span>Select Material Status</span>"
    }
    });
});
</script>
<script type="text/javascript">
    function getSubcategory(){
      var id = $("#id_category").val();
      console.log(id);

      $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
        $("#id_subcategory").html(result);
      }
    });
    }
    
    function getItem(){
      var id = $("#id_subcategory").val();
      console.log(id);

      $.ajax({url: "get_items.php?id="+id, success: function(result){
        $("#id_item").html(result);
      }
    });
    }
</script>
<script type="text/javascript">
      $(document).ready(function (e) {
          $('#add').on('click', function () {

            var sid = '<?php echo $sid; ?>';
            var id_category= $("#id_category").val();
            if(id_category == ""){
                alert('SELECT CATEGORY NAME');
                return false;
            }
            var id_subcategory= $("#id_subcategory").val();
            if(id_subcategory == ""){
                alert('SELECT SUB CATEGORY NAME');
                return false;
            }
            var item = $("#id_item").val();
            if(item == ""){
                alert('SELECT ITEM NAME');
                return false;
            }
            var qnty = $("#qnty").val();
            if(qnty == ""){
                alert('ENTER QUANTITY');
                return false;
            }
            var doc_no = $("#doc_no").val();
            if(doc_no == ""){
                alert('ENTER DOC NUMBER');
                return false;
            }
            var remarks = $("#remarks").val();
            if(remarks == ""){
                alert('ENTER REMARKS');
                return false;
            }
            var item_date = $("#item_date").val();
            if(item_date == ""){
                alert('SELECT DATE');
                return false;
            }
            
              var file_data = $('#photo').prop('files')[0];
              if(file_data == undefined){
                alert('UPLOAD MATERIAL PHOTO');
                return false;
              }
              var form_data = new FormData();
              form_data.append('file', file_data);
              form_data.append('id_category', id_category);
              form_data.append('id_subcategory', id_subcategory);
              form_data.append('item', item);
              form_data.append('qnty', qnty);
              form_data.append('sid', sid);
              form_data.append('doc_no', doc_no);
              form_data.append('remarks', remarks);
              form_data.append('item_date', item_date);


              $.ajax({
                  url: 'add_items_to_material.php', // point to server-side PHP script 
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function (response) {
                      $('#previous').html(response); // display success response from the PHP script
                      $("#id_category").empty();
                    $("#id_category").append('<option value="">SELECT </option>' 
                      + '<?php for($i=0; $i<count($categoryList); $i++) { ?>' 
                      + '<option value="' 
                      + '<?php echo $categoryList[$i]['id']; ?>' +
                        '">' 
                      + '<?php echo $categoryList[$i]['name']?>' +
                        '</option>' +
                        '<?php } ?>');
                      $("#id_subcategory").empty();
                      $("#id_subcategory").append('<option value="">SELECT</option>');
                      $("#id_item").empty();
                      $("#id_item").append('<option value="">SELECT</option>');
                      $('#photo').val('');
                      $("#qnty").val('');
                      $("#doc_no").val('');
                      $("#remarks").val('');
                      $("#item_date").val('');
                  },
              });
          });
      });
    </script>
<script type="text/javascript">
    function selectField(){
        var value = $("#customer_vendor").val();
        if(value== "customer"){
            $(".customer").show();
            $(".vendor").hide();
        }
        if (value == "vendor") {
            $(".vendor").show();
            $(".customer").hide();
        }
    }
    var value = $("#customer_vendor").val();
        if(value== "customer"){
            $(".customer").show();
            $(".vendor").hide();
        }
        if (value == "vendor") {
            $(".vendor").show();
            $(".customer").hide();
        }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
<script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
</script>
</html>