<?php

include 'connection.php';
// $id = $_GET['id'];
$strId = $_GET['strId'];
$rno = $_GET['rno'];
$sno = $_GET['sno'];

$viewquery = "SELECT a.*, b.name, b.code, c.name as subStore FROM sale_history as a INNER JOIN item as b ON a.id_item=b.id INNER JOIN sub_store as c ON a.id_sub_store=c.id WHERE a.id_sub_store ='$strId' AND a.rack_no='$rno' AND a.self_no='$sno' ";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $career[$i]['id'] = $row['id'];
      $career[$i]['name'] = $row['name']."-".$row['code'];
      $career[$i]['rack_no'] = $row['rack_no'];
      $career[$i]['self_no'] = $row['self_no'];
      $career[$i]['subStore'] = $row['subStore'];
      $career[$i]['qty_sale'] = $row['qty_sale'];
      $i++;
    }

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Items in Store</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?item_id='+id;
      }
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Items in Store</h3>
                    <a href="sale_items_sub_store.php?id=<?php echo $strId; ?>" class="btn-primary pull-right">Back</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sub Store</th>
                           <th >Item Name</th>
                    		<th>Rack Number</th>
                    		<th>Self Number</th>
                    		<th>Sale Quantity</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                          for ($i=0; $i<count($career); $i++)
                          { ?>
                        <tr>
                            <td><?php echo $career[$i]['subStore']; ?></td>
                            <td><?php echo $career[$i]['name']; ?></td>
                          <td><?php echo $career[$i]['rack_no']; ?></td>
                          <td><?php echo $career[$i]['self_no']; ?></td>
                          <td><?php echo $career[$i]['qty_sale']; ?></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>