<?php
include 'connection.php';
if (isset($_GET['id'])) {

    $id       = $_GET['id'];
    $sql      = "select * from category where id = $id";
    $result   = $con->query($sql);
    $category = $result->fetch_assoc();
   
}else {
    $id= 0 ;
}
      
if (isset($_POST['save'])) {
    
    $sql      = "select * from category";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
    $ref= sprintf("%'.03d", $resnum);
    
    $name     = $_POST['name'];

    $sql = "insert into category(name, category_code, status) values('$name', '$ref', 1)";
    $con->query($sql);
    
    echo "<script>alert('category added successfully')</script>";
    echo "<script>parent.location='categories.php'</script>";
    // header("location: categories.php");
}
if (isset($_POST['update'])) {
    
    $name  = $_POST['name'];
    
    $sql = "update category set name = '$name' where id = $id";

    $con->query($sql);
    header("location: categories.php");
}


?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if(!empty($category['category_id'])){echo "Edit";}else{echo "Add";}?> Category</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
<link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : red;
    }
    input{
        text-transform: UPPERCASE;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">
                
                <?php include 'sidebar.php'; ?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3><?php if(!empty($category['id'])){echo "Edit";}else{echo "Add";}?> Category</h3>
                    </div>


                        <div class="form-group">
                            <div class="row">
                              <div class="col-sm-4">
                                <label>Name <span class="text-danger"> *</span></label>
                                <input type="text" class="form-control" id="name" name="name" value="<?php if (!empty($category['name'])) {echo $category['name'];}?>" onblur="checkduplicateName()">
                                </div>
                            </div>
                            
                        </div>
        <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Compitator</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <label>Compitator Item</label>
                    <input type="text" class="form-control" name="citem" id="citem" autocomplete="off">
                </div>
                <div class="col-sm-6">
                    <label>Unit Price</label>
                    <input type="text" class="form-control" name="unitprice" id="unitprice" autocomplete="off">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <label>Item Attachment</label>
                    <input type="file" class="form-control" name="file" id="file" autocomplete="off">
                </div>
                <div class="col-sm-6">
                    <label>Sub Category</label>
                    <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                      </select>
                </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                  <label>Item Name<span class="error">*</span></label>
                    <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;" onchange="getItemPrice()">
                      <option value="">SELECT</option>
                    </select>
              </div>
              <div id="itemprice"></div>
            </div>
          </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
                
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <a href="categories.php" class="btn btn-error">Cancel</a>
                    <button class="btn btn-success" type="submit" name="<?php if(!empty($category['id'])){echo "update";}else{echo "save";}?>"><?php if(!empty($category['id'])){echo "Update";}else{echo "Save";}?></button>
                   </div>
                </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
    </div>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
    /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
      });
    }
    </script>
    
     <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                name:"required"
            },
            messages:{
                name:"<span>Enter Category Name</span>"
            },
        })
    })
</script>
<script type="text/javascript">
        function checkduplicateName(){
          var name = $("#name").val();
          var id = "<?php echo $id;?>";
          console.log(id);

          $.ajax({url: "duplicate_categoryname.php?name="+name+"&id="+id, success: function(result){
            if(result==1) {
                alert("Category Name already there");
                $("#name").val(' ');
            }
          }
        });
        }

          var id = '<?php echo $id; ?>';
          console.log(id);

          $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
            $("#id_subcategory").html(result);
          }
        });
        
        function getItem(){
          var id = $("#id_subcategory").val();
          console.log(id);

          $.ajax({url: "get_items.php?id="+id, success: function(result){
            $("#id_item").html(result);
          }
        });
        }
</script>
<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
        <script type="text/javascript">
      $(document).ready(function (e) {
          $('#add').on('click', function () {
              var cid = '<?php echo $id; ?>';
              var file_data = $('#file').prop('files')[0];
              if(file_data == undefined){
                alert('UPLOAD FILE');
                return false;
              }
              var citem = $('#citem').val();
              if(citem == ""){
                alert('ENTER COMPETITOR ITEM NAME');
                return false;
              }

              var unitprice = $('#unitprice').val();
              if(unitprice == ""){
                alert('ENTER UNIT PRICE');
                return false;
              }

              var id_subcategory = $('#id_subcategory').val();
              if(id_subcategory == ""){
                alert('SELECT SUBCATEGORY NAME');
                return false;
              }

              var id_item = $('#id_item').val();
              if(id_item == ""){
                alert('SELECT ITEM NAME');
                return false;
              }

              var form_data = new FormData();
              form_data.append('file', file_data);
              form_data.append('cid', cid);
              form_data.append('citem', citem);
              form_data.append('unitprice', unitprice);
              form_data.append('id_subcategory', id_subcategory);
              form_data.append('id_item', id_item);

              $.ajax({
                  url: 'add_item_competitors.php', // point to server-side PHP script 
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function (response) {
                      $('#addedfiles').html(response); // display success response from the PHP script
                      $('#file').val('');
                  },
              });
          });
      });
</script>

</body>

</html>
