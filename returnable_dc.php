<?php
include 'connection.php';

    $query = "SELECT a.*, d.name as itemName FROM returnable_dc as a INNER JOIN item as d on a.id_item=d.id";
    $queryresult = mysqli_query($con,$query);
  $career = [];
  $i=0;
  while ($row = mysqli_fetch_array($queryresult))
  {
    $career[$i]['dc_no']= $row['dc_no'];
    $career[$i]['dc_date']= $row['dc_date'];
    $career[$i]['customer_vendor']= $row['customer_vendor'];
    $career[$i]['id_customer_vendor']= $row['id_customer_vendor'];
    $career[$i]['itemName']= $row['itemName'];
    $career[$i]['qnty']= $row['qnty'];
    $career[$i]['remark']= $row['remark'];
    $career[$i]['material_status']= ucwords($row['material_status']);
    $career[$i]['id']= $row['id'];
    $i++;
  }
?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Returnable DC</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete.php?item_id='+id;
      }
    }
  </script>

<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <div class="page-title clearfix">
                    <h3>Out Returnable DC List</h3>
                     <a href="add_returnable_dc.php" class="btn btn-primary">+ Create Out Returnable DC</a>
                </div>

  <table class="table table-striped" id="example">
                        <thead>
                            <tr>
                                <th>Sl. No</th>
                            <th>DC NO</th>
                            <th>DC Date</th>
                          <th>Customer/Vendor</th>
                          <th>Name</th>
                          <th>Material</th>
                          <th>Edit&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            if ($career[$i]['customer_vendor']=="customer") {
                              $query = "SELECT customer_name FROM customer WHERE id='".$career[$i]['id_customer_vendor']."'";
                              $result = $con->query($query);
                              $item = $result->fetch_assoc();

                            }else{
                              $query = "SELECT vendor_name FROM vendor WHERE id='".$career[$i]['id_customer_vendor']."'";
                              $result = $con->query($query);
                              $item = $result->fetch_assoc();
                            }

                            ?>
                        <tr>
                            <td><?php echo $i+1; ?></td>
                          <td><?php echo $career[$i]['dc_no']; ?></td>
                          <td><?php echo $career[$i]['dc_date']; ?></td>
                          <td><?php echo strtoupper($career[$i]['customer_vendor']); ?></td>
                          <td><?php if($career[$i]['customer_vendor']=="customer"){ echo $item['customer_name'];}else{ echo $item['vendor_name'];} ?></td>
                          <td><?php echo $career[$i]['material_status']; ?></td>
                          <td><a href="add_returnable_dc.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a><font size="5px">&nbsp;</td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>