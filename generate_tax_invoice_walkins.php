<?php
include 'connection.php';
error_reporting(0);
if (isset($_GET['id']))
{

    $id = $_GET['id'];
    $sql = "select * from tax_invoice_sales where id = $id";

    $result = $con->query($sql);
    $item = $result->fetch_assoc();
    
    $sql = "select * from tax_invoice_consignee_details where id_tax_sales = $id";
    $result = $con->query($sql);
    $ship = $result->fetch_assoc();

    $sql = "select * from tax_sales_invoice_notes where id_tax_sales = $id";

    $viewqueryresult = mysqli_query($con,$sql);
    $i=0;
      while ($row = mysqli_fetch_array($viewqueryresult))
    {
        $reviewList[$i]['id'] = $row['id'];
      $reviewList[$i]['note'] = $row['note'];
      $reviewList[$i]['date'] = date("d/m/Y", strtotime($row['date']));
      $i++;
    }

 $viewquery = " SELECT a.*, d.name as itemName, d.code FROM po_inward_items_sold as a INNER JOIN item as d ON a.id_item=d.id WHERE a.id_tax_sales ='$id'";
    $viewqueryresult = mysqli_query($con,$viewquery);
    $career = [];
    $i=0;
    while ($row = mysqli_fetch_array($viewqueryresult))
    {
      $career[$i]['itemName'] = $row['itemName']."-".$row['code'];
      $career[$i]['quantity'] = $row['quantity'];
    $career[$i]['unit_price'] = $row['unit_price'];
    $career[$i]['supplied_charge'] = $row['supplied_charge'];
    $career[$i]['total'] = $row['total'];
    $career[$i]['supplied'] = $row['supplied'];
    $career[$i]['gst_rate'] = $row['gst_rate'];
    $career[$i]['pending'] = $row['pending'];
    $career[$i]['discount'] = $row['discount'];
      $career[$i]['id'] = $row['id'];
      $i++;
    }
    
    $result = $con->query($sql);
    $ship = $result->fetch_assoc();
    
    $sql="SELECT sum(total) as totalAmount FROM po_inward_items_sold WHERE id_tax_sales ='$id' ";
      $result = mysqli_query($con,$sql);
    while ($row = mysqli_fetch_array($result)){
      $totalSum = $row['totalAmount'];
    }

    $sql="Update tax_invoice_sales SET total_amount='$totalSum', tax_include_amount='$totalSum', payment_status='PENDING($totalSum)' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));

$sql      = "select * from tax_invoice_sales";
    $result1   = $con->query($sql);
      $resnum = mysqli_num_rows($result1);
      $resnum = $resnum + 1;
      $currMonth = date("m");
      $currYear = date("y");
      $nxtYear = date("y", strtotime("+1 year"));
    $ref= sprintf("%'.04d", $resnum);
    $serialNo = $ref."/".$currYear."-".$nxtYear;

if (isset($_POST['save']))
{
    $invoiceNo = $_POST['invoice_no'];
    $customer_name =$_POST['id_customer'];
    $poNoId = $_POST['po_no'];
    $total = $_POST['total'];
    $gstRate = $_POST['rate'];
    $gstType = $_POST['gst_type'];
    $sgst = $_POST['sgst_rate'];
    $cgst = $_POST['cgst_rate'];
    $igst = $_POST['igst_rate'];
    $totalAmount = $_POST['totalinclude'];
    $payStatus = "PENDING"."($totalAmount)";
    $balance = $_POST['totalinclude'];
    $remarks = $_POST['remarks'];
    $file = $_FILES['attachment']['name'];
    $temp = $_FILES['attachment']['tmp_name'];
    move_uploaded_file($temp, "uploads/".$file);
    $date = date("Y-m-d", strtotime($_POST['date']));
    $days = $_POST['days'];

   $sql="INSERT INTO tax_invoice_sales(invoice_no, date, id_customer, po_num_id, total_amount, balance, gst_rate, gst_type, sgst_rate, cgst_rate, igst_rate, tax_include_amount, payment_status, remarks, days_no, attachment) VALUES('$invoiceNo', '$date', '$customer_name', '$poNoId ', '$total', '$balance', '$gstRate', '$gstType', '$sgst', '$cgst', '$igst', '$totalAmount', '$payStatus', '$remarks', '$days', '$file')";

    $con->query($sql) or die(mysqli_error($con));

    $result = $con->query("SELECT LAST_INSERT_ID() as id");
$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
$last_id = $result['id'];

    header("location: tax_invoice_walkins.php");
}

if (isset($_POST['add']))
{
    $cosigneeType = $_POST['cosigdetail'];
    $otherCharges = $_POST['other_charges'];
    $vehicleNo = $_POST['vehicle_no'];
    $lrNo = $_POST['lrno'];
    $lrDate = date("Y-m-d", strtotime($_POST['lr_date']));
    $esugamNo = $_POST['esugam'];
    
    if($cosigneeType=='1'){
        $sql="Update tax_invoice_sales SET cosignee='same as above', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    }
    if($cosigneeType=='2'){
        $customer = $_POST['customer'];
        $sql="Update tax_invoice_sales SET cosignee='$customer', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    }
    if($cosigneeType=='3'){
        $sql="Update tax_invoice_sales SET cosignee='new cosignee', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    
    $cosigneeName = $_POST['cosignee'];
    $address =$_POST['address'];
    $gstNo = $_POST['gstno'];
    
    $sql="INSERT INTO tax_invoice_consignee_details(id_tax_sales, name, address, gst_no) VALUES('$id', '$cosigneeName', '$address', '$gstNo')";

    $con->query($sql) or die(mysqli_error($con));
    }
    echo "<script>parent.location='generate_tax_invoice_walkins.php?id=$id'</script>";
}

if(isset($_POST['send'])){

    $courier = $_POST['courier'];
    $docNo = $_POST['doc_no'];
    $rdate = date("Y-m-d", strtotime($_POST['rdate']));
    $dattach = $_FILES['doc_attach']['name'];
    $dattachtmp = $_FILES['doc_attach']['tmp_name'];
    move_uploaded_file($dattachtmp, "uploads/".$dattach);
    $rname = $_POST['rname'];
    $rphone = $_POST['rphone'];
    $rdesig = $_POST['rdesig'];

   $updatequery = "update tax_invoice_sales set regard_name = '$rname', regard_phone='$rphone', regard_designation='$rdesig', courier='$courier', doc_no='$docNo', doc_date='$rdate', doc_attach='$dattach' where id = $id";
    $res=$con->query($updatequery);
      
   $query = "select id, invoice_no, regard_name, regard_phone, regard_designation, courier, doc_no, doc_date, doc_attach, po_num_id, customer_name, email from tax_invoice_sales where id = $id";

    $result1 = $con->query($query);
    $dispatchData = $result1->fetch_assoc();

      $poNo = $dispatchData['po_num_id'];
      $customerEmail = $dispatchData['email'];
      $customerName = $dispatchData['customer_name'];
      $invoiceNum = $dispatchData['invoice_no'];
      $courieror = $dispatchData['courier'];
      $docNo = $dispatchData['doc_no'];
      $docDate = $dispatchData['doc_date'];
      $docAttach = $dispatchData['doc_attach'];
      $regardPhone = $dispatchData['regard_phone'];
    $regardName = strtoupper($dispatchData['regard_name']);
    $regardDesig = ucwords($dispatchData['regard_designation']);
    $date = date("d/m/Y");

$to = $customerEmail;

$subject = "SAI ENTERPRISES- PO DISPATCH: $poNo";

$message = "
<table style='width: 50%'>
<tr><th style='text-align: left'>DEAR SIR/MADAM</th></tr>
<tr>
<th style='text-align: center; color: blue;'>DISPATCH DETAILS</th>
</tr>
</table>
<br>
<table  style='text-align: left'>
<tr>
<th style='text-align: left'>
<b style='color: blue;'>REFF:</b> PO: $poNo  Date: $date
<br><br>
<b style='color: blue;'>WE DISPATCHED ABOVE PO REFERENCE MATERIAL AS PER YOUR DI.
<br>
FIND BELOW DISPATCH DETAILS FOR YOUR REFERENCE.</b>
</th>
</tr>
<tr>
<th style='text-align: left'>DI :</th></tr>
<tr >
<td style='text-align: left'><b>INVOICE NO:</b> $invoiceNum</td>
</tr>
<tr>
<td style='text-align: left'><b>COURIEROR:</b> $courieror</td>
</tr>
<tr>
<td style='text-align: left'><b>DOC NO:</b> $docNo</td>
</tr>
<tr>
<td style='text-align: left'><b>DATE:</b> $docDate</td>
</tr>
<tr>
<td style='text-align: left'><b>DOC NO ATTACHED:</b> <a href='https://www.saienpl.com/billing_system/uploads/$docAttach' target='_blank'>$docAttach</a></td>
</tr>
</table>
<br><br><br>
REGARDS
<br>
$regardName <br>
$regardDesig <br>
Mob: $regardPhone";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <mktg@saienpl.com>' . "\r\n";
$headers .= 'Cc: <yogeshdims@gmail.com>' . "\r\n";

mail($to,$subject,$message,$headers);
echo "<script>alert('Email Sent Succesfully')</script>";
echo "<script>parent.location='generate_tax_invoice_walkins.php?id=$id'</script>";
}

if (isset($_POST['edit']))
{
    $cosigneeType = $_POST['cosigdetail'];
    $otherCharges = $_POST['other_charges'];
    $vehicleNo = $_POST['vehicle_no'];
    $lrNo = $_POST['lrno'];
    $lrDate = date("Y-m-d", strtotime($_POST['lr_date']));
    $esugamNo = $_POST['esugam'];
    if($cosigneeType=='1'){
        $sql="Update tax_invoice_sales SET cosignee='same as above', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo', lr_date='$lrDate' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    }
    if($cosigneeType=='2'){
        $customer = $_POST['customer'];
        $sql="Update tax_invoice_sales SET cosignee='$customer', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo', lr_date='$lrDate' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
    }
    if($cosigneeType=='3'){
        $sql="Update tax_invoice_sales SET cosignee='new cosignee', other_charges='$otherCharges', esugam='$esugamNo', vehicle_no='$vehicleNo', lr_number='$lrNo', lr_date='$lrDate' WHERE id='$id'";
        $con->query($sql) or die(mysqli_error($con));
        
    $cosigneeName = $_POST['cosignee'];
    $address =$_POST['address'];
    $gstNo = $_POST['gstno'];
    
    $sql="Update tax_invoice_consignee_details SET name='$cosigneeName', address='$address', gst_no='$gstNo' WHERE id_tax_sales='$id'";

    $con->query($sql) or die(mysqli_error($con));
    }
    echo "<script>parent.location='generate_tax_invoice_walkins.php?id=$id'</script>";
}

if (isset($_POST['update']))
{

    $invoiceNo = $_POST['invoice_no'];
    $customer_name =$_POST['id_customer'];
    $invValue = $_POST['invoice_value'];
    $remarks = $_POST['remarks'];
    $file = $_FILES['attachment']['name'];
    if($file == ""){
        $file = $item['attachment'];
    }
    else{
        $file = $_FILES['attachment']['name'];
        $temp = $_FILES['attachment']['tmp_name'];
        move_uploaded_file($temp, "uploads/".$file);
    }
    $date = date("Y-m-d", strtotime($_POST['date']));
    $days = $_POST['days'];
    
    $id  = $item['id'];
    $updatequery = "update tax_invoice_sales set id_customer = '$customer_name', invoice_no='$invoiceNo', invoice_value='$invValue', attachment='$file', remarks='$remarks', date='$date', days_no='$days' where id = $id";

    $res=$con->query($updatequery);

        echo '<script>parent.location="tax_invoice_sales.php"</script>';
}

$sql = "SELECT id, name, category_code FROM category";
$result = $con->query($sql);
$categoryList = array();
while ($row = $result->fetch_assoc()) {
    array_push($categoryList, $row);
  }

$sql = "SELECT id, customer_name FROM customer";
$result = $con->query($sql);
$companyList = array();
while ($row = $result->fetch_assoc()) {
    array_push($companyList, $row);
  }
  
  $sql = "SELECT id, name, code FROM item";
$result = $con->query($sql);
$itemList = array();
while ($row = $result->fetch_assoc()) {
    array_push($itemList, $row);
  }
  
  $sql = "SELECT id, customer_po_no FROM purchase_order_inwards";
$result = $con->query($sql);
$poInList = array();
while ($row = $result->fetch_assoc()) {
    array_push($poInList, $row);
  }
}

?>
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Generate Tax Invoice Sales </title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link href="library/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="library/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<style>
    .error{
        text-transform: UPPERCASE;
        color : #a94442;
    }
</style>

<body>
    <div class="fluid-container container-wrapper clearfix">
            <div class="col-sm-3 side-bar">

                <?php include 'sidebar.php';?>
            </div>

            <div class="col-sm-9 main-container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                    </div><!-- /.container-fluid -->
                </nav>
                
                <form method="POST" action="" enctype="multipart/form-data" id="form">
                <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Generate Tax Invoice Sale(WalkIns)</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Invoice Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="invoice_no" id="invoice_no" autocomplete="off" value="<?php if($item['invoice_no']) { echo $item['invoice_no']; } else { echo $serialNo; } ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Date <span class="error">*</span></label>
                                <input type="text" class="form-control" name="date" id="date" autocomplete="off" value="<?php echo $item['date']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Customer Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="customer_name" id="customer_name" value="<?php echo $item['customer_name'] ?>" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>PO NO<span class="error">*</span></label>
                                <input type="text" class="form-control" name="po_no" id="po_no" value="<?php echo $item['po_num_id'] ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>GST NO<span class="error">*</span></label>
                                <input type="text" class="form-control" name="gst_no" id="gst_no" value="<?php echo $item['gst_no'] ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>No of Days<span class="error"></span></label>
                                <input type="text" class="form-control" name="days" id="days" autocomplete="off" value="<?php echo $item['days_no']; ?>" maxlength="3" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Remarks<span class="error"></span></label>
                                <input type="text" class="form-control" name="remarks" id="remarks" autocomplete="off" value="<?php echo $item['remarks']; ?>" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Email<span class="error"></span></label>
                                <input type="text" class="form-control" name="email" id="email" autocomplete="off" value="<?php echo $item['email']; ?>" disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Other Charges<span class="error"></span></label>
                                <input type="text" class="form-control" name="other_charges" id="other_charges" autocomplete="off" value="<?php echo $item['other_charges']; ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>E Sugam Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="esugam" id="esugam" autocomplete="off" value="<?php echo $item['esugam']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Vehicle Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="vehicle_no" id="vehicle_no" autocomplete="off" value="<?php echo $item['vehicle_no']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> LR Number <span class="error">*</span></label>
                                <input type="text" class="form-control" name="lrno" id="lrno" autocomplete="off" value="<?php echo $item['lr_number']; ?>" >
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> LR Date <span class="error">*</span></label>
                                <input type="text" class="form-control" name="lr_date" id="lr_date" autocomplete="off" value="<?php echo $item['lr_date']; ?>" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                        <div class="form-group">
                            <br>
                            <button type="button" name="modal" id="modal" class="btn btn-danger" data-toggle="modal" data-target="#myModal" style="width: 260px;">Add Items</button>
                        </div>
                    </div>
                    </div>
                    <div>
                    </div>
                </div>
                <br>
                <div style="<?php if($id==''){echo "display:none";}?>">
                                <table id="example" class="table table-striped">
                                   <thead>
                                    <tr>
                                        <th>Item Name</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Discount</th>
                                        <th>GST </th>
                                        <th>Total</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php for($i=0; $i<count($career); $i++){?>
                                        <tr>
                                            <td><?php echo $career[$i]['itemName']; ?></td>
                                            <td style="text-align: center;"><?php echo $career[$i]['quantity']; ?></td>
                                            <td><?php echo $career[$i]['unit_price']; ?></td>
                                            <td><?php echo $career[$i]['discount']; ?>%</td>
                                            <td><?php echo $career[$i]['gst_rate']; ?>%</td>
                                            <td><?php echo $career[$i]['total']; ?></td>
                                            <td><a href="javascript:Ondel(<?php echo $career[$i]['id']; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <br>
                    <div class="page-container">

                    <div class="page-title clearfix">
                        <h3>Add Cosignee Details</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <select name="cosigdetail" id="cosigdetail" class="form-control selitemIcon" onchange="getCosigneeFields()">
                                    <option value="">SELECT</option>
                                    <option value="1" <?php if($item['cosignee']=="same as above"){ echo "selected"; } ?>>Customer has Cosignee</option>
                                    <option value="2" <?php if($item['cosignee'] !='same as above' && $item['cosignee'] !='new cosignee'){ echo "selected=selected"; } ?>>Existing Customer</option>
                                    <option value="3" <?php if($item['cosignee']=='new cosignee'){ echo "selected=selected"; } ?>>Add New Cosignee</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4 existing_customer" style="display: none;">
                            <div class="form-group">
                                <select class="form-control selitemIcon" name="customer" id="customer" style="width: 280px;">
                                    <option value="">SELECT</option>
                                    <?php for($i=0; $i<count($companyList); $i++){?>
                                    <option value="<?php echo $companyList[$i]['id']; ?>" <?php if($companyList[$i]['id']==$item['cosignee']){ echo "selected=selected"; } ?>><?php echo $companyList[$i]['customer_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row cosignee_fields" style="display: none">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Cosignee Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="cosignee" id="cosignee" autocomplete="off" value="<?php echo $ship['name']; ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Address <span class="error">*</span></label>
                                <input type="text" class="form-control" name="address" id="address" autocomplete="off" value="<?php echo $ship['address']; ?>" >
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label> Gst No <span class="error">*</span></label>
                                <input type="text" class="form-control" name="gstno" id="gstno" autocomplete="off" value="<?php echo $ship['gst_no']; ?>" >
                            </div>
                        </div>
                    </div>
                    </div>
                        
                <div class="button-block clearfix">
                   <div class="pull-right">
                    <button class="btn btn-error"><a href="tax_invoice_walkins.php">Cancel</a></button>
                    <a href="tax_sales_walkin_invoice.php?poid=<?php echo $item['po_num_id']; ?>&id=<?php echo $id ?>" class="btn btn-success" name="generate" target="_blank">Generate</a>
                    <button class="btn btn-success" type="submit" name="<?php if (!empty($ship['id'])) {echo "edit";} else {echo "add";}?>"><?php if (!empty($ship['id'])) {echo "Update";} else {echo "Save";}?></button>
                   </div>
                </div>
                <div class="page-title clearfix col-sm-12">
                    <h3>Send Dispatch Mail</h3>
                    <div class="pull-right">
                        <button class="btn btn-success" name="send" id="send">Send Mail</button>
                    </div>
                </div>
                <div class="page-container">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Courieror<span class="error">*</span></label>
                                <input type="text" class="form-control" name="courier" id="courier" value="<?php  echo $item['courier']; ?>">
                        </div>
                        <div class="col-sm-4">
                            <label>DOC No<span class="error">*</span></label>
                                <input type="text" class="form-control" name="doc_no" id="doc_no" value="<?php  echo $item['doc_no']; ?>">
                        </div>
                        <div class="col-sm-4">
                            <label>Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="rdate" id="rdate" autocomplete="off" value="<?php  echo $item['doc_date']; ?>">
                        </div>
                   </div>
                   <div class="row">
                       <div class="col-sm-4">
                        <label>DOC Attachment<span class="error">*</span></label>
                            <input type="file" name="doc_attach" id="doc_attach" class="form-control">
                       </div>
                   </div>
                    <div class="row">
                        <h4 class="col-sm-12">Regards:</h4> <br>
                        <div class="col-sm-4">
                            <label>Name<span class="error">*</span></label>
                                <input type="text" class="form-control" name="rname" id="rname" value="<?php  echo $item['regard_name']; ?>">
                        </div>
                        <div class="col-sm-4">
                            <label>Phone Number<span class="error">*</span></label>
                                <input type="text" class="form-control" name="rphone" id="rphone" value="<?php  echo $item['regard_phone']; ?>">
                        </div>
                        <div class="col-sm-4">
                            <label>Designation<span class="error">*</span></label>
                                <input type="text" class="form-control" name="rdesig" id="rdesig" value="<?php  echo $item['regard_designation']; ?>">
                        </div>
                   </div>
               </div>

                <div class="page-title clearfix col-sm-12">
                    <h3>Notes</h3>
                </div>
                <div class="page-container">
                <div class="row add">
                    <h4 class="col-sm-12">Add New Note</h4> <br>
                        <div class="col-sm-4">
                            <label>Date<span class="error">*</span></label>
                                <input type="text" class="form-control" name="note_date" id="note_date" value="">
                        </div>
                        <div class="col-sm-4">
                            <label>Note<span class="error">*</span></label>
                                <textarea  class="form-control" name="note" id="note" value=""></textarea>
                        </div>
                         <div class="col-sm-1">
                            <input type="button" name="Review" id="Review" class="btn btn-danger"  value="Add Note" onclick="saveReview()" style="margin-top: 30px;"/>
                        </div>
                   </div>
                   <div id="edit_note"></div>
                   
                   <hr/>
                       
                        <table class="table table-striped" id="example2">
                        <thead>
                            <tr>
                          <th>Date</th>
                          <th>Note</th>
                          <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($reviewList); $i++)
                          {
                              $cid = $reviewList[$i]['id'];
                          ?>
                        <tr>
                          <td><?php echo $reviewList[$i]['date']; ?></td>
                          <td><?php echo $reviewList[$i]['note']; ?></td>
                        <td><a href="javascript:Onedit(<?php echo $cid; ?>);"><i class="fa fa-edit fa-2x" title="EDIT"></i></a> <a href="javascript:Ondelete(<?php echo $cid; ?>);" title="DELETE"><i class="fa fa-trash fa-2x"></i></a></td>
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
                       <div id="addfiles"></div>
                   </div>
                </form>
                    <div class="footer">
                        <p>&copy; 2018, Allrights reserved</p>
                    </div>
                </div>

            </div>
<input type="hidden" name="poItemId" id="poItemId">
<input type="hidden" name="itemQty" id="itemQty">
<input type="hidden" name="itemSup" id="itemSup">
<input type="hidden" name="itemPend" id="itemPend">
    <input type="hidden" name="poinId" id="poinId">  

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Items</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Category Name<span class="error">*</span></label>
                            <select name="id_category" class="form-control selitemIcon" onchange="getSubcategory()" id="id_category" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    // $id_category=$item['id_category'];
                                    for ($i=0; $i <count($categoryList); $i++)
                                    { 
                                        $value=$categoryList[$i]['id'];
                                        $label=$categoryList[$i]['name']."-".$categoryList[$i]['category_code'];
                                    ?>
                                    <option value="<?php echo $value;?>"
                                            <?php if($value==$item['id_category'])
                                            {
                                                echo "selected=selected";
                                            }
                                         ?>
                                            ><?php echo $label; ?>
                                        </option>;
                                    <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Sub Category Name<span class="error">*</span></label>
                            <select name="id_subcategory" class="form-control selitemIcon" id="id_subcategory" onchange="getItem()" style="width: 260px;">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_sub_category'])){
                                    for ($i=0; $i<count($subcategoryList); $i++) {  ?>
                                <option value="<?php echo $subcategoryList[$i]['id']; ?>" <?php if($subcategoryList[$i]['id'] == $item['id_sub_category'])  { echo "selected=selected"; }
                                    ?>><?php echo $subcategoryList[$i]['name']."-".$subcategoryList[$i]['sub_cat_code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Item Name<span class="error">*</span></label>
                            <select name="id_item" class="form-control selitemIcon" id="id_item" style="width: 260px;" onchange="getPrice()">
                                <option value="">SELECT</option>
                                    <?php
                                    if(!empty($item['id_item'])){
                                    for ($i=0; $i<count($itemList); $i++) {  ?>
                                    <?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?>
                                <option value="<?php echo $itemList[$i]['id']; ?>" <?php if($itemList[$i]['id'] == $item['id_item'])  { echo "selected=selected"; }
                                    ?>><?php echo $itemList[$i]['name']."-".$itemList[$i]['code']; ?></option>
                                    <?php
                                    }
                                  }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                    <div id="qtnfields"></div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" name="add" id="add" class="btn btn-success">Add</button>
        </div>
      </div>
      
    </div>
  </div>
    
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#lr_date" ).datepicker();
    $( "#note_date" ).datepicker();
    $( "#editdate" ).datepicker();
    $( "#rdate" ).datepicker();
  } );
  </script>
  <script type="text/javascript">

    function getFileField(){
        
    }

    function Onedit(cid)
    {
      $.ajax({url: "get_clarification.php?id="+cid, success: function(result){
        $("#edit_clarification").html(result);
        $(".addoa").hide();
      }
    });
    }
    
    function OneditOa(cid){
        $.ajax({url: "get_oa_clarification.php?id="+cid, success: function(result){
        $("#edit_oa_clarification").html(result);
        $(".add").hide();
      }
    });
    }
    
    function getSubcategory(){
      var id = $("#id_category").val();
      console.log(id);

      $.ajax({url: "get_subcategory.php?id="+id, success: function(result){
        $("#id_subcategory").html(result);
      }
    });
    }
    
    function getItem(){
      var id = $("#id_subcategory").val();
      console.log(id);

      $.ajax({url: "get_items.php?id="+id, success: function(result){
        $("#id_item").html(result);
      }
    });
    }

    function getPrice(){
          var id = $("#id_item").val();
          console.log(id);

          $.ajax({url: "get_fields_for_purchase.php?id="+id, success: function(result){
            $("#qtnfields").html(result);
          }
        });

      }

    function getCalculate(){
        var unit= parseInt($('#unit').val());
            var totqnty = $("#totqnty").val();
            var purchase_cost = parseInt(unit * totqnty);
            // alert(purchase_cost);
            // console.log(purchase_cost);

        var per= $("#discount").val();
        var gst= $("#gst").val();
        
        var pernum = (purchase_cost * 0.01 * (100 - per));
        var discountnum = (purchase_cost - pernum);
        var totnum = purchase_cost - discountnum;
        
        var gstpernum = (totnum * 0.01 * (100 - gst));
        var gstnum = (totnum - gstpernum);
        var totamt = totnum + gstnum;

        $('#total').val(totamt.toFixed(2));
    }

        function getCosigneeFields(){
            var cosig = $("#cosigdetail").val();
            if(cosig == "1"){
                $(".existing_customer").hide();
                $(".cosignee_fields").hide();
            }
            if(cosig == "2"){
                $(".existing_customer").show();
                $(".cosignee_fields").hide();
            }
            if(cosig == "3"){
                $(".cosignee_fields").show();
                $(".existing_customer").hide();
            }
        }
        
        var cosig = $("#cosigdetail").val();
            if(cosig == "1"){
                $(".existing_customer").hide();
                $(".cosignee_fields").hide();
            }
            if(cosig == "2"){
                $(".existing_customer").show();
                $(".cosignee_fields").hide();
            }
            if(cosig == "3"){
                $(".cosignee_fields").show();
                $(".existing_customer").hide();
            }
    
    </script>

  <script type="text/javascript" src="js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function()
    {
        $('#form').validate({
            rules:{
                id_customer:"required",
                remarks:"required",
                date : "required",
                rate : "required",
                po_no : "required"
            },
            messages:{
                id_customer:"<span>Select customer Name</span>",
                remarks:"<span>Enter Remarks</span>",
                date:"<span>select Date</span>",
                rate:"<span>Select GST Rate</span>",
                po_no:"<span>select PO NO</span>"
            },
        });
    });
</script>

<script type="text/javascript">
   $.validator.addMethod("accept", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    });
</script>
<script type="text/javascript">

    function pass(id){
        
        $("#poItemId").val(id);
    }
    
    function passQty(qid){
        
        $("#itemQty").val(qid);
    }
    
    function passSup(sid){
        
        $("#itemSup").val(sid);
    }
    
    function passPend(pid){
        
        $("#itemPend").val(pid);
    }
    
    function checkQty(){
        var id = $("#poItemId").val();
        var pend = parseInt($("#itemPend").val());
       var qty = parseInt($("#itemQty").val());
        var sup = parseInt($("#itemSup").val());
        var quantity = parseInt($("#quantity").val());
        var totqty = parseInt(quantity + sup);
        if(totqty > qty){
            alert("Item pending quantity is "+pend);
            return false;
        }
    }
    
</script>
  <script type="text/javascript">
    $("#add").on('click',function(){
    var sid = '<?php echo $id; ?>';
    var id_category= $("#id_category").val();
    if(id_category == ""){
        alert('Select Category Name');
        return false;
    }
    var id_subcategory= $("#id_subcategory").val();
    if(id_subcategory == ""){
        alert('Select Sub Category Name');
        return false;
    }
    var item = $("#id_item").val();
    if(item == ""){
        alert('Select Item Name');
        return false;
    }
    var unit = $("#unit").val();
    if(unit == ""){
        alert('Enter Unit Price');
        return false;
    }
    var totqnty = $("#totqnty").val();
    if(totqnty == ""){
        alert('Enter Quantity');
        return false;
    }
    var total = $("#total").val();
    if(total == ""){
        alert('Enter Total');
        return false;
    }
    var discount = $("#discount").val();
    if(discount == ""){
        alert('Enter Discount');
        return false;
    }
    
    var gst = $("#gst").val();
    if(gst == ""){
        alert('Enter GST Rate');
        return false;
    }
    
      $.ajax({

        url: 'add_items_to_po_walkins.php',
        data:{

          'sid': sid,
          'item': item,
           'id_category': id_category,
            'id_subcategory': id_subcategory,
            'unit': unit,
            'totqnty': totqnty,
            'discount': discount,
            'gst': gst,
            'total': total,
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
      });
</script>
<script type="text/javascript">
    function Onedit(cid)
    {
      $.ajax({url: "get_tax_sales_note.php?id="+cid, success: function(result){
        $("#edit_note").html(result);
        $(".add").hide();
      }
    });
    }

        function saveReview() {
        
            var note = $("#note").val();
            if(note== ""){
                alert('Enter Note');
                exit;
            }
            var date = $("#note_date").val();
            if(date== ""){
                alert('Select Date');
                exit;
            }
            
            var tid = '<?php echo $id; ?>';         
            $.ajax({

        url: 'add_tax_sales_invoice_notes.php',
        method:'POST',
        data:{

          'note': note,
          'date': date,
           'tid': tid
        },
        success: function(result){
            location.reload();
      }
        });
            
        }

        function editReview(id) {
        
            var note = $("#editnote").val();
            if(note== ""){
                alert('Enter Note');
                exit;
            }

            var date = $("#editdate").val();
            if(date== ""){
                alert('Select Date');
                exit;
            }
            
         $.ajax({

        url: 'add_tax_sales_invoice_notes.php',
        method:'POST',
        data:{

          'note': note,
          'date': date,
           'id': id
        },
        success: function(result){
            $("#previous").html(result);
            location.reload();
      }
        });
  
      
        
    }
</script>
</body>

<script src="library/select2/js/select2.js" ></script>
<script src="library/select2/js/select2-init.js" ></script>
<script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
    $('#example2').DataTable();
});
</script>
</html>