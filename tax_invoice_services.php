<?php

include 'connection.php';

     $sql = "select sum(balance) as total from tax_invoice_services";
    $result = $con->query($sql);
    $taxBalance = $result->fetch_assoc();
    if($taxBalance['total'] == ""){
        $taxBalance['total'] = '0.00';
    }
    $balanceAmountSales = $taxBalance['total'];
    
  $sql = "select sum(tax_include_amount) as total from tax_invoice_services";
    $result = $con->query($sql);
    $taxTotal = $result->fetch_assoc();
    if($taxTotal['total'] == ""){
        $taxTotal['total'] = '0.00';
    }

$viewquery = "Select a.*, b.customer_name, b.address, b.mobile, b.email from tax_invoice_services a INNER JOIN customer b ON a.id_customer=b.id ORDER BY a.invoice_no ASC";
$viewqueryresult = mysqli_query($con,$viewquery);
$career = [];
$i=0;
while ($row = mysqli_fetch_array($viewqueryresult))
{
  $career[$i]['customer_name'] = $row['customer_name']."<br>".$row['address']."<br>".$row['mobile'];
  $career[$i]['item'] = $row['name']."<br>".$row['code'];
  $career[$i]['invoice_value'] = $row['tax_include_amount'];
  $career[$i]['invoice_no'] = $row['invoice_no'];
  $career[$i]['remarks'] = substr($row['remarks'], 10)."...";
  $career[$i]['payment_status'] = $row['payment_status'];
  $career[$i]['balance'] = $row['balance'];
  $career[$i]['date'] = $row['date'];
  $career[$i]['id'] = $row['id'];
  $i++;
}
$currDate = date("Y-m-d");
$query = "Select a.*, b.customer_name, b.email from tax_invoice_services a INNER JOIN customer b ON a.id_customer=b.id ORDER BY a.invoice_no ASC";

$queryresult = mysqli_query($con,$query);
$taxData = [];
$i=0;
while ($row = mysqli_fetch_array($queryresult))
{

  $taxData[$i]['customer_name'] = $row['customer_name'];
  $taxData[$i]['email'] = $row['email'];
  $taxData[$i]['invoice_value'] = $row['invoice_value'];
  $taxData[$i]['invoice_no'] = $row['invoice_no'];
  $taxData[$i]['remarks'] = $row['remarks'];
  $taxData[$i]['payment_status'] = $row['payment_status'];
  $taxData[$i]['balance'] = $row['balance'];
  $taxData[$i]['date'] = $row['date'];
  $taxData[$i]['days_no'] = $row['days_no'];
  $taxData[$i]['finaldate'] = date("Y-m-d", strtotime(+$row['days_no']." days", strtotime($row['date'])));
  // echo date("d M Y", $thirtyDaysUnix);
  $taxData[$i]['id'] = $row['id'];
  $i++;
}

$emailquery = "Select b.email from tax_invoice_services a INNER JOIN customer b ON a.id_customer=b.id ORDER BY a.invoice_no ASC";

$queryresult = mysqli_query($con,$emailquery);
$emailData = [];
$i=0;
while ($row = mysqli_fetch_assoc($queryresult))
{
  $emailData[$i]['email'] = $row['email'];
  $i++;
}

if($_POST){

$subject = "SENDING BILLS PAYABLE -REG.";

$message = "
<table>
<tr><th style='text-align: left'>Dear Sir. </th></tr>
<tr><th style='text-align: left'>This is to inform you that we are yet to receive the amount towards the following bills.</th></tr>
<br><br>
<tr><th style='text-align: left'><br><u>INVOICE DETAILS</u></th></tr>
</table>

<table border='1' style='border-collapse: collapse;'>
<tr>
<th>CUSTOMER</th>
<th>INVOICE NO</th>
<th>INVOICE DATE</th>
<th>INVOICE VALUE</th>
<th>BALANCE</th>
<th>DUE DATE</th>
</tr>";

for ($i=0; $i<count($taxData); $i++)
{
 $id = $i+1;
 $name = $taxData[$i]['customer_name'];
 $invoiceNo = $taxData[$i]['invoice_no'];
 $date = $taxData[$i]['date'];
 $invoiceValue = $taxData[$i]['invoice_value'];
 $balance = $taxData[$i]['balance'];
 $dueDays = $taxData[$i]['days_no'];
 $dueDate = $taxData[$i]['finaldate'];
 $dueDays = $taxData[$i]['days_no'];

$message .="
<tr>
<td>$name</td>
<td>$invoiceNo</td>
<td>$date</td>
<td>$invoiceValue</td>
<td>$balance</td>
<td>$dueDate</td>
</tr>
<tr>";
}

$message .="</table>
<br><br>
<i>Total Outstanding Amount as per above list : Rs. $balanceAmount /-</i>.
<br><br><br>
REGARDS
<br>
AJIT";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <info@atninstruments.com>' . "\r\n";
$headers .= 'Cc: <yogeshdims@gmail.com>' . "\r\n";
for ($i=0; $i <count($emailData); $i++) { 
    $to = $emailData[$i]['email'];

mail($to,$subject,$message,$headers);
  }

echo "<script>alert('Email Sent succesfully')</script>";
echo "<script>parent.location='tax_invoice_sales.php'</script>";
}

?>

<!DOCTYPE html>

<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Tax Invoice Services</title>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
<link href="css/main.css" rel="stylesheet">
    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/dataTables.jqueryui.min.css" rel="stylesheet">

</head>
<style type="text/css">
  button{
    left: 810px;
    top: 90px;
    position: absolute;
  }
</style>
<script type="text/javascript">
    function Ondelete(id)
    {
      var conf = confirm('Do you want to delete?');
      if (conf==true)
      {
        parent.location='delete_tax_order.php?porder='+id;
      }
    }
  </script>


<body>
    <div class="fluid-container container-wrapper clearfix">
        <div class="col-sm-3 side-bar">
            
                        <?php include('sidebar.php');?>
        </div>

        <div class="col-sm-9 main-container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <?php include('navigation.php');?>
                </div><!-- /.container-fluid -->
            </nav>

            <div class="page-container">
                <form method="POST" id="form">
                <div class="page-title clearfix">
                    <h3>Tax Invoice Services <br><b style="color: blue;">(Total Value : <?php echo round($taxTotal['total'], 2); ?>)</b> &emsp; <b style="color: blue;">(Pending Balance : <?php echo round($taxBalance['total'], 2); ?>)</b>
                    <button class="btn btn-primary" type="submit" name="send">SEND MAIL FOR PAYMENT</button></h3> <br><br>
                     <a href="add_tax_invoice_services.php" class="btn btn-primary">+ Create Tax Invoice Services</a> 
                </div>
                  
                </form>

                    <table class="table table-striped" id="example" >
                        <thead>
                            <tr>
                                <th>Invoice No.</th>
                                <th>Date</th>
                                <th>Customer Name</th>
                                <th>Invoice Value</th>
                                <th>Payment Status</th>
                          <th>Remarks</th>
                          <th>Generate</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                          for ($i=0; $i<count($career); $i++)
                          {
                            $id = $career[$i]['id'];
                            $status = $career[$i]['payment_status'];
                            ?>
                        <tr>
                            <td><?php echo $career[$i]['invoice_no']; ?></td>
                            <td><?php echo $career[$i]['date']; ?></td>
                          <td><?php echo $career[$i]['customer_name']; ?></td>
                          <td><?php echo $career[$i]['invoice_value']; ?></td>
                          <td><?php echo $career[$i]['payment_status']; ?></td>
                          <td><?php echo $career[$i]['remarks']; ?></td>
                          <td><a href="generate_tax_invoice_services.php?id=<?php echo $career[$i]['id']; ?>" class="btn btn-primary">Generate</a></td>
                          <!--<td><a href="add_tax_invoice_sales.php?id=<?php echo $id; ?>"><i class="fa fa-edit fa-2x"></i></a></td>-->
                        </tr>
                          <?php
                          }
                          ?>

                        </tbody>
                    </table>
              
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
         <script src="js/jquery-3.3.1.js"></script>
       <script src="js/jquery.dataTables.min.js"></script>
       <script src="js/dataTables.jqueryui.min.js"></script>

       <script type="text/javascript">
       $(document).ready(function() {
    $('#example').DataTable();
});
       </script>
       
</body>

</html>